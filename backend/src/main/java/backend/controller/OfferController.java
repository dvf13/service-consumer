package backend.controller;

import backend.dtos.OfferDto;
import backend.dtos.ResponseDto;

import java.util.Date;

public interface OfferController {

    ResponseDto findOffersByBusinessIdOrderByValidUntil(Long business_id);

    ResponseDto findOffersByBusinessIdFromTo(Long business_id, Date from, Date to);

    ResponseDto getAllOffers();

    ResponseDto getSomeOffers();

    ResponseDto getOfferById(Long id);

    ResponseDto getOffersByTimeSlotIds(Long[] id);

    ResponseDto saveOffer(OfferDto offerDto);

    void deleteOfferById(Long id);

    ResponseDto updateOffer(Long id, OfferDto offerDto);
}
