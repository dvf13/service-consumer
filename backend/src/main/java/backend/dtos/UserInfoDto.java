package backend.dtos;

import com.fasterxml.jackson.annotation.JsonRootName;
import lombok.Data;

@Data
@JsonRootName("user_info")
public class UserInfoDto {

    private Long id;

    private String username;

    private UserType userType;


    private UserInfoDto() { }

    public UserInfoDto(Long id, String username, UserType userType) {
        this.id = id;
        this.username = username;
        this.userType = userType;
    }

    public enum UserType {
        BUSINESS, CUSTOMER, ADMIN
    }
}
