import Vue from 'vue';
import router from '../router';
import config from '../config';
import HttpService from './HttpService';

export default {

  /* Authentication */

  async login(username, password) {
    if (!this.isAuthenticated()) {
      return HttpService.post(`${config.apiUrl}auth/login/`, { username, password })
        .then((response) => {
          const responseDto = response.data;

          if (responseDto.error) {
            return Promise.reject(responseDto.data.message);
          }
          this.setAuthToken(responseDto.data);
          return Promise.resolve();
        }, error => Promise.reject(error.statusText))
        .catch(e => Promise.reject(e));
    }
    return Promise.resolve();
  },

  async logout() {
    if (!this.isAuthenticated()) {
      router.push('/');
      return Promise.resolve();
    }
    return HttpService.post(`${config.apiUrl}auth/logout/`)
      .then((response) => {
        const responseDto = response.data;

        if (responseDto.error) {
          return Promise.reject(responseDto.data.message);
        }
        this.removeAuthToken();
        router.push('/');
        return Promise.resolve(responseDto.data);
      }, error => Promise.reject(error.statusText))
      .catch(e => Promise.reject(e));
  },

  async getUserInfo() {
    if (!this.isAuthenticated()) {
      return Promise.reject('Not logged in');
    }
    return HttpService.get(`${config.apiUrl}auth/userinfo/`)
      .then((response) => {
        const responseDto = response.data;

        if (responseDto.error) {
          return Promise.reject(responseDto.data.message);
        }
        return Promise.resolve(responseDto.data);
      }, error => Promise.reject(error.statusText))
      .catch(e => Promise.reject(e));
  },

  isAuthenticated() {
    if (!Vue.localStorage.get('LMD_auth_token')) {
      return false;
    }
    return Vue.localStorage.get('LMD_auth_token') !== '';
  },

  getAuthToken() {
    return JSON.parse(Vue.localStorage.get('LMD_auth_token'));
  },

  setAuthToken(token) {
    Vue.localStorage.set('LMD_auth_token', JSON.stringify(token));
  },

  removeAuthToken() {
    Vue.localStorage.set('LMD_auth_token', '');
  },


  /* Registration */

  async register(userType, data) {
    return HttpService.post(`${config.apiUrl}registration/${userType}/`, data)
      .then((response) => {
        const responseDto = response.data;

        if (responseDto.error) {
          return Promise.reject(responseDto.data.message);
        }
        return Promise.resolve(responseDto.data);
      }, error => Promise.reject(error.statusText))
      .catch(e => Promise.reject(e));
  },
};
