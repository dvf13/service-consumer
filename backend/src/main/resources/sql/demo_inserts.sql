INSERT INTO `business` (`id`, `username`, `password_hash`, `email`, `name`, `street`, `street_number`, `zip_code`, `city`) VALUES
(1,	'schweizerhaus',	'$2a$10$55Iw6ZZQpeWlDoCq.zsrJu4iHYEX2j0ulB9M.apmvpBovGc8liuWC',	'info@schweizerhaus.at',	'Schweizerhaus',	'Prater',	'116',	1020,	'Vienna'),
(2,	'figlmüller',	'$2a$10$xaBUJUw4/q.HQjpzZ5c0henf7hrSTAl9mg5hs.4nlPHTMndfgTmGe',	'info@figlmueller.at',	'Figlmüller',	'Wollzeile',	'5',	1010,	'Vienna'),
(3,	'klippOper',	'$2a$10$7RCkvpK8HByh/c13sVyxs.cHu41p1rRTi6wLy.A9YwEUm4QUBrHy2',	'info@klipp.at',	'Klipp',	'Opernring',	'5',	1010,	'Vienna'),
(4,	'gruberreisen',	'$2a$10$raDkEAbFhVRqmmfzZRLi.eIAgRCDSN3UNE4KY8CoOFTIGlYhFjADK',	'walfisch@grubereisen.at',	'Gruper Reisen',	'Walfischgasse',	'10',	1010,	'Vienna'),
(5,	'soccerdome',	'$2a$10$SC6tKLk.y1GOq.KK/FMrw.2tiGh4by1oiPPA5Zc1nbvtrlkxz.Q6.',	'office@soccerdome.at',	'Soccer Dome',	'Hopsagasse',	'5',	1200,	'Vienna');

INSERT INTO `category` (`id`, `name`) VALUES
(1,	'Food'),
(2,	'Beauty'),
(3, 'Travel'),
(4, 'Sport');

INSERT INTO `customer` (`id`, `username`, `password_hash`, `email`, `first_name`, `last_name`, `birthdate`, `gender`) VALUES
(1,	'user1',	'$2a$10$pbxwO7wYK931KNLUq6qcxO4OQPevkg/F28cWJgwKN8rMrO7dLJ73.',	'user1@email.com',	'Max',	'Mustermann',	'2001-01-20 00:00:00',	'm'),
(2,	'user2',	'$2a$10$OUQzv7XDlbhzgGBFqOPkZeG8xjNd1DWOZngF0UZue7GvLft3Vq1TW',	'user2@email.com',	'Maria',	'Musterfrau',	'2001-01-20 01:00:00',	'f'),
(3,	'user3',	'$2a$10$OUQzv7XDlbhzgGBFqOPkZeG8xjNd1DWOZngF0UZue7GvLft3Vq1TW',	'user3@email.com',	'Maria',	'Musterfrau',	'2001-01-20 01:00:00',	'f');

INSERT INTO `offer` (`id`, `business_id`, `category_id`, `title`, `description`, `picture_url`, `picture`, `latitude`, `longitude`, `price`, `discount`, `amount`, `valid_until`, `created_at`, `updated_at`) VALUES
(1,	1, 1, 'Mittag im Schweizerhaus',	'Österreichisches Mittagessen', 'http://www.gastronews.wien/wp-content/uploads/2017/03/Schweizerhaus_2__c__Kolarik.jpg', LOAD_FILE(''), 48.210033, 16.363449, 10,	15.7,	2,	'2018-08-07 16:45:00',	'2018-01-01 00:00:00',	'2018-01-01 00:00:00'),
(2,	2, 1,	'Bestes Schnitzel',	'Das beste Schnitzel Wiens', 'https://figlmueller.at/wp-content/uploads/figlmueller_home_bg.jpg', LOAD_FILE(''), 48.210033, 16.363449,	20,	50.0,	5,	'2018-08-21 12:00:00',	'2018-03-05 00:00:00',	'2018-03-05 00:00:00'),
(3,	2, 1,	'Abendessen',	'Dinner im Figlmüller', 'https://figlmueller.at/wp-content/uploads/figlmueller_home_bg.jpg', LOAD_FILE(''), 48.210033, 16.363449,	40,	10.0,	2,	'2018-08-21 12:00:00',	'2018-03-05 00:00:00',	'2018-03-05 00:00:00'),
(4,	1, 1,	'Buffet',	'Buffet im Schweizerhaus', 'http://www.gastronews.wien/wp-content/uploads/2017/03/Schweizerhaus_2__c__Kolarik.jpg',	LOAD_FILE(''), 48.210033, 16.363449, 20,	25.0,	5,	'2018-08-05 12:00:00',	'2018-03-05 00:00:00',	'2018-03-05 00:00:00'),
(5,	3, 2,	'Haarschnitt',	'Haarschnitt vom Profi', 'http://images05.oe24.at/Unbenannt-1.jpg/620x388/204.925.117',	LOAD_FILE(''), 48.210033, 16.363449, 15,	25.0,	5,	'2018-08-05 12:00:00',	'2018-03-05 00:00:00',	'2018-03-05 00:00:00'),
(6,	3, 1,	'Waschen+Schneiden',	'Waschen und Schneiden', 'http://images05.oe24.at/Unbenannt-1.jpg/620x388/204.925.117', LOAD_FILE(''), 48.210033, 16.363449, 20,	15.0,	3,	'2018-08-05 12:00:00',	'2018-03-05 00:00:00',	'2018-03-05 00:00:00'),
(7,	4, 3,	'Italien',	'Italien im Juni', 'https://www.master-and-more.at/fileadmin/user_upload/Laender-Regionen/Italien.jpg', LOAD_FILE(''), 48.210033, 16.363449, 400,	5.0,	2,	'2018-08-10 12:00:00',	'2018-03-05 00:00:00',	'2018-03-05 00:00:00'),
(8,	4, 3,	'Kroatien',	'Kroatien im Juni', 'https://cdn.kroati.de/images/uploads/info/bilder/kroatien_01.jpg',	LOAD_FILE(''), 48.210033, 16.363449, 300,	5.0,	2,	'2018-08-10 12:00:00',	'2018-03-05 00:00:00',	'2018-03-05 00:00:00'),
(9,	5, 4,	'Afterkick',	'Kicken im Dome', 'http://collegeblog.kn-online.de/files/2017/07/Spielszene1-624x416.jpg',LOAD_FILE(''), 48.210033, 16.363449, 50,	5.0,	2,	'2018-08-10 12:00:00',	'2018-03-05 00:00:00',	'2018-03-05 00:00:00'),
(10,	5, 4,	'Afterkick',	'Kicken im Dome', 'http://collegeblog.kn-online.de/files/2017/07/Spielszene1-624x416.jpg', LOAD_FILE(''), 48.210033, 16.363449,	50,	5.0,	2,	'2018-08-10 12:00:00',	'2018-03-05 00:00:00',	'2018-03-05 00:00:00');

INSERT INTO `timeslot` (`id`, `offer_id`, `beg_date_time`, `end_date_time`, `quantity_total`, `quantity_booked`) VALUES
(1,	1, '2018-06-05 15:00:00',	'2018-08-07 17:00:00',	2, 1),
(2,	2, '2018-06-15 17:00:00',	'2018-08-21 18:00:00',	5, 1),
(3,	3, '2018-05-15 17:00:00',	'2018-08-21 18:00:00',	2, 0),
(4,	4, '2018-05-01 17:00:00',	'2018-08-05 18:00:00',	5, 0),
(5,	5, '2018-07-03 17:00:00',	'2018-08-05 18:00:00',	5, 0),
(6,	7, '2018-06-05 08:00:00',	'2018-08-10 18:00:00',	2, 0),
(7,	9, '2018-06-10 18:00:00',	'2018-08-10 21:00:00',	2, 0),
(8,	9, '2018-06-10 20:00:00',	'2018-08-10 22:00:00',	2, 1),
(9,	1, '2018-06-05 12:00:00',	'2018-08-07 14:00:00',	3, 0),
(10,	6, '2018-06-05 12:00:00',	'2018-08-07 14:00:00',	3, 0),
(11,	8, '2018-06-05 12:00:00',	'2018-08-07 14:00:00',	3, 0),
(12,	10, '2018-06-05 12:00:00',	'2018-08-07 14:00:00',	3, 0);

INSERT INTO `reservation` (`id`, `customer_id`, `timeslot_id`, `state`, `reserved_at`) VALUES
(1 ,1,	1,	0,	'2018-05-20 18:00:00'),
(2, 2,	2,	0,	'2018-05-20 18:00:00'),
(3, 2,	8,	0,	'2018-05-20 18:00:00'),
(4, 3,	8,	0,	'2018-05-20 18:00:00');

INSERT INTO `authority` (`id`, `name`) VALUES
(1,	'ROLE_BUSINESS'),
(2,	'ROLE_CUSTOMER'),
(3,	'ROLE_ADMIN');

INSERT INTO `user_authority` (`id`, `authority_id`, `business_id`, `customer_id`) VALUES
(1, 1, 1, NULL),
(2, 1, 2, NULL),
(3, 1, 3, NULL),
(4, 1, 4, NULL),
(5, 1, 5, NULL),
(6, 2, NULL, 1),
(7, 2, NULL, 2),
(8, 3, NULL, 3);
