package backend.services.impl;

import backend.dtos.TimeSlotDto;
import backend.dtos.BusinessDto;
import backend.exceptions.ServiceException;
import backend.models.Business;
import backend.models.Offer;
import backend.dtos.OfferDto;
import backend.models.TimeSlot;
import backend.repositories.OfferRepository;
import backend.services.AuthenticationService;
import backend.services.BusinessService;
import backend.services.CategoryService;
import backend.services.OfferService;
import backend.services.TimeSlotService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;

import java.security.Principal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class OfferServiceImpl implements OfferService {

    private OfferRepository offerRepository;

    private BusinessService businessService;

    private CategoryService categoryService;

    private AuthenticationService authenticationService;

    private TimeSlotService timeSlotService;


    @Autowired
    public OfferServiceImpl(OfferRepository offerRepository, BusinessService businessService, CategoryService categoryService, TimeSlotService timeSlotService) {
        this.offerRepository = offerRepository;
        this.businessService = businessService;
        this.categoryService = categoryService;
        this.timeSlotService = timeSlotService;
    }

    @Override
    public List<OfferDto> findOffersByBusinessIdOrderByValidUntil(Long id) throws ServiceException {

        List<Offer> offerList = this.offerRepository.findOffersByBusinessIdOrderByValidUntil(id);

        if (offerList == null || offerList.isEmpty()) {
            throw new ServiceException(1043, "offer doesn’t exist");
        }

        return this.convertOfferDaoListToOfferDtoList(offerList);
    }

    @Override
    public List<OfferDto> findOffersByBusinessIdFromTo(Long id, Date from, Date to) throws ServiceException {
        List<TimeSlot> tsList;
        List<Offer> offerList = this.offerRepository.findOffersByBusinessIdOrderByValidUntil(id);
        for(Offer o : offerList) {
            tsList = o.getTimeSlotList();
            for(TimeSlot t : tsList){
                t.setQuantity_booked(this.offerRepository.findReservationCount(t.getId(), from, to));
            }
        }
        if (offerList == null || offerList.isEmpty()) {
            throw new ServiceException(1043, "offer doesn’t exist");
        }

        return this.convertOfferDaoListToOfferDtoList(offerList);
    }

    @Override
    @PreAuthorize("hasAnyRole('ADMIN', 'BUSINESS', 'CUSTOMER')")
    public List<OfferDto> getAllOffers() throws ServiceException {
        List<Offer> offerList = this.offerRepository.findAll();

        if(offerList == null || offerList.isEmpty()) {
            throw new ServiceException(1043, "offer doesn’t exist");
        }
        return this.convertOfferDaoListToOfferDtoList(offerList);
    }

    @Override
    public List<OfferDto> getSomeOffers() throws ServiceException {
        List<Offer> offerList = this.offerRepository.findOffersOrderByDiscountDescAndAmountAsc();

        if(offerList == null || offerList.isEmpty()) {
            throw new ServiceException(1043, "offer doesn’t exist");
        }

        int offerNumber = 6;
        List<Offer> offerListResult = new ArrayList<>();

        for(int i = 0; i < offerNumber && i < offerList.size(); i++) {
            offerListResult.add(offerList.get(i));
        }
        return this.convertOfferDaoListToOfferDtoList(offerListResult);
    }

    @Override
    @PreAuthorize("hasAnyRole('ADMIN', 'BUSINESS', 'CUSTOMER')")
    public OfferDto getOfferById(Long id) throws ServiceException {
        if(id == null || id <= 0) {
            throw new ServiceException(1040, "invalid offer id");
        }
        Offer offer = this.offerRepository.findOne(id);

        if(offer == null) {
            throw new ServiceException(1043, "offer doesn’t exist");
        }
        return this.convertOfferToOfferDto(offer);
    }

    @Override
    @PreAuthorize("hasAnyRole('ADMIN', 'BUSINESS', 'CUSTOMER')")
    public List<OfferDto> getOffersByTimeSlotIds(Long[] ids) throws ServiceException {
        if(ids == null) {
            throw new ServiceException(1040, "invalid offer id");
        }
        List<Offer> offerList = new ArrayList<>();
        for (int i = 0; i < ids.length; i++) {
            offerList.add(this.offerRepository.findOfferByTimeSlotId(ids[i]));
        }
        // List<Offer> offerList = this.offerRepository.findOfferByTimeSlotId(id);

        if(offerList == null || offerList.isEmpty()) {
            throw new ServiceException(1043, "offer doesn’t exist");
        }

        return this.convertOfferDaoListToOfferDtoList(offerList);
    }

    // TODO: make production ready (checks for null etc.)
    @Override
    @PreAuthorize("hasAnyRole('ADMIN', 'BUSINESS')")
    public OfferDto saveOffer(OfferDto offerDto) throws ServiceException {
        System.out.println("SaveOfferServiceImpl" + offerDto + "\n\n" + offerDto.getTimeSlotDtoList());
        if (offerDto == null || !this.isValidOfferDtoToSave(offerDto)) {
            throw new ServiceException(1042, "invalid offer object");
        }
        Offer newOffer = this.convertOfferDtoToOffer(offerDto);
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String currentPrincipalName = authentication.getName();
        List<BusinessDto> businessDtos = businessService.getBusinessesByUsername(currentPrincipalName); // TODO: check if null
        Business business = businessService.convertBusinessDtoToBusiness(businessDtos.get(0));
        newOffer.setBusiness(business);
        newOffer.setCategory(categoryService.convertCategoryDtoToCategory(categoryService.getCategoryById(offerDto.getCategoryDto().getId()))); // TODO: this is for testing only, category implementation is missing, delete after implementation
        Offer savedOffer = this.offerRepository.save(newOffer);
        return this.convertOfferToOfferDto(savedOffer);
    }

    @Override
    @PreAuthorize("hasAnyRole('ADMIN', 'BUSINESS')")
    public void deleteOfferById(Long id) throws ServiceException {
        if(id == null || id <= 0) {
            throw new ServiceException(1040, "invalid offer id");
        }
        this.offerRepository.delete(id);
    }

    @Override
    @PreAuthorize("hasAnyRole('ADMIN', 'BUSINESS')")
    public OfferDto updateOffer(Long id, OfferDto offerDto) throws ServiceException {
        if(id == null || id <= 0) {
            throw new ServiceException(1040, "invalid offer id");
        }
        if (offerDto == null || !this.isValidOfferDtoToUpdate(offerDto)) {
            throw new ServiceException(1042, "invalid offer object");
        }
        if(id.equals(offerDto.getId())) {
            throw new ServiceException(1041, "offer ids doesn’t match");
        }
        if(this.offerRepository.findOne(id) == null) {
            throw new ServiceException(1043, "offer doesn’t exist");
        }
        Offer updatedOffer = this.offerRepository.save(this.convertOfferDtoToOffer(offerDto));
        return this.convertOfferToOfferDto(updatedOffer);
    }

    @Override
    public OfferDto convertOfferToOfferDto(Offer offer) throws ServiceException {
        if(offer == null) {
            throw new ServiceException(1042, "invalid offer object");
        }
        OfferDto offerDto = new OfferDto();

        offerDto.setId(offer.getId());
        offerDto.setDescription(offer.getDescription());
        offerDto.setPrice(offer.getPrice());
        offerDto.setDiscount(offer.getDiscount());
        offerDto.setTitle(offer.getTitle());
        offerDto.setValidUntil(offer.getValidUntil());
        offerDto.setCreatedAt(offer.getCreatedAt());
        offerDto.setUpdatedAt(offer.getUpdatedAt());
        offerDto.setAmount(offer.getAmount());
        offerDto.setPictureUrl(offer.getPictureUrl());
        offerDto.setPicture(offer.getPicture());
        offerDto.setLatitude(offer.getLatitude());
        offerDto.setLongitude(offer.getLongitude());
        offerDto.setBusinessDto(this.businessService.convertBusinessToBusinessDto(offer.getBusiness())); // TODO: enable again
        offerDto.setTimeSlotDtoList(this.timeSlotService.convertTimeSlotDaoListToTimeSlotDtoList(offerDto, offer.getTimeSlotList()));
        offerDto.setCategoryDto(this.categoryService.convertCategoryToCategoryDto(offer.getCategory())); // TODO: enable again

        return offerDto;
    }

    @Override
    public Offer convertOfferDtoToOffer(OfferDto offerDto) throws ServiceException {
        if(offerDto == null) {
            throw new ServiceException(1042, "invalid offer object");
        }
        Offer offer = new Offer();

        offer.setId(offerDto.getId());
        offer.setDescription(offerDto.getDescription());
        offer.setPrice(offerDto.getPrice());
        offer.setDiscount(offerDto.getDiscount());
        offer.setTitle(offerDto.getTitle());
        offer.setValidUntil(offerDto.getValidUntil());
        offer.setAmount(offerDto.getAmount());
        offer.setPictureUrl(offerDto.getPictureUrl());
        offer.setPicture(offerDto.getPicture());
        offer.setLatitude(offerDto.getLatitude());
        offer.setLongitude(offerDto.getLongitude());
        offer.setBusiness(this.businessService.convertBusinessDtoToBusiness(this.businessService.getBusinessById(offerDto.getBusinessDto().getId())));
        offer.setCategory(this.categoryService.convertCategoryDtoToCategory(this.categoryService.getCategoryById(offerDto.getCategoryDto().getId())));
        offer.setTimeSlotList(this.timeSlotService.convertTimeSlotDtoListToTimeSlotDaoList(offer, offerDto.getTimeSlotDtoList()));

        //zwei convert methoden für save und existing object timeslot
        //   offer.getTimeSlotList().get(0).setOffer(offer);

        //offer.setBusiness(this.businessService.convertBusinessDtoToBusiness(offerDto.getBusinessDto())); // TODO: enable again
        // offer.setCategory(this.categoryService.convertCategoryDtoToCategory(offerDto.getCategoryDto())); // TODO: enable again

        return offer;
    }

    @Override
    public boolean isValidOfferDtoToSave(OfferDto offerDto) {
        return offerDto.getAmount() > 0 &&
                //offerDto.getBusinessDto() != null && this.businessService.isValidBusinessDtoToSave(offerDto.getBusinessDto()) && // TODO: this should be enabled again
                //offerDto.getCategoryDto() != null && this.categoryService.isValidCategoryDtoToSave(offerDto.getCategoryDto()) && // TODO: this should be enabled again
                offerDto.getValidUntil() != null &&
                offerDto.getDescription() != null && !offerDto.getDescription().equals("") &&
                offerDto.getDiscount() != null && !(offerDto.getDiscount() < 0.0) &&
                //offerDto.getPictureUrl() != null && !offerDto.getPictureUrl().equals("") &&
                offerDto.getTitle() != null && !offerDto.getTitle().equals("") &&
                offerDto.getPrice() > 0;
    }

    @Override
    public boolean isValidOfferDtoToUpdate(OfferDto offerDto) {
        return this.isValidOfferDtoToSave(offerDto) && offerDto.getId() != null && offerDto.getId() > 0;
    }

    private List<OfferDto> convertOfferDaoListToOfferDtoList(List<Offer> offerList) throws ServiceException {
        List<OfferDto> offerDtoList = new ArrayList<>();

        for (Offer offer : offerList) {
            offerDtoList.add(this.convertOfferToOfferDto(offer));
        }

        return offerDtoList;
    }
}
