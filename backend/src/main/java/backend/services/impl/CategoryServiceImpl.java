package backend.services.impl;

import backend.exceptions.ServiceException;
import backend.models.Category;
import backend.dtos.CategoryDto;
import backend.repositories.CategoryRepository;
import backend.services.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class CategoryServiceImpl implements CategoryService {

    private CategoryRepository categoryRepository;


    @Autowired
    public CategoryServiceImpl(CategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }


    @Override
    public List<CategoryDto> getAllCategories() throws ServiceException {
        List<Category> categoryList = this.categoryRepository.findAll();

        if(categoryList == null || categoryList.isEmpty()) {
            throw new ServiceException(1023, "category doesn’t exist");
        }
        return this.convertCategoryDaoListToCategoryDtoList(categoryList);
    }

    @Override
    @PreAuthorize("hasAnyRole('ADMIN', 'BUSINESS', 'CUSTOMER')")
    public CategoryDto getCategoryById(Long id) throws ServiceException {
        if(id == null || id <= 0) {
            throw new ServiceException(1020, "invalid category id");
        }
        Category category = this.categoryRepository.findOne(id);

        if(category == null) {
            throw new ServiceException(1023, "category doesn’t exist");
        }
        return this.convertCategoryToCategoryDto(category);
    }

    @Override
    @PreAuthorize("hasRole('ADMIN')")
    public CategoryDto saveCategory(CategoryDto categoryDto) throws ServiceException {
        if (categoryDto == null || !this.isValidCategoryDtoToSave(categoryDto)) {
            throw new ServiceException(1022, "invalid category object");
        }
        Category savedCategory = this.categoryRepository.save(this.convertCategoryDtoToCategory(categoryDto));
        return this.convertCategoryToCategoryDto(savedCategory);
    }

    @Override
    @PreAuthorize("hasRole('ADMIN')")
    public void deleteCategoryById(Long id) throws ServiceException {
        if(id == null || id <= 0) {
            throw new ServiceException(1020, "invalid category id");
        }
        this.categoryRepository.delete(id);
    }

    @Override
    @PreAuthorize("hasRole('ADMIN')")
    public CategoryDto updateCategory(Long id, CategoryDto categoryDto) throws ServiceException {
        if(id == null || id <= 0) {
            throw new ServiceException(1020, "invalid category id");
        }
        if (categoryDto == null || !this.isValidCategoryDtoToUpdate(categoryDto)) {
            throw new ServiceException(1022, "invalid category object");
        }
        if(id.equals(categoryDto.getId())) {
            throw new ServiceException(1021, "category ids doesn’t match");
        }
        if(this.categoryRepository.findOne(id) == null) {
            throw new ServiceException(1023, "category doesn’t exist");
        }
        Category updatedCategory = this.categoryRepository.save(this.convertCategoryDtoToCategory(categoryDto));
        return this.convertCategoryToCategoryDto(updatedCategory);
    }

    @Override
    public CategoryDto convertCategoryToCategoryDto(Category category) throws ServiceException {
        if(category == null) {
            throw new ServiceException(1022, "invalid category object");
        }
        CategoryDto categoryDto = new CategoryDto();

        categoryDto.setId(category.getId());
        categoryDto.setName(category.getName());

        return categoryDto;
    }

    @Override
    public Category convertCategoryDtoToCategory(CategoryDto categoryDto) throws ServiceException {
        if(categoryDto == null) {
            throw new ServiceException(1022, "invalid category object");
        }
        Category category = new Category();

        category.setId(categoryDto.getId());
        category.setName(categoryDto.getName());

        return category;
    }

    @Override
    public boolean isValidCategoryDtoToSave(CategoryDto categoryDto) {
        return categoryDto.getName() != null && !categoryDto.getName().equals("");
    }

    @Override
    public boolean isValidCategoryDtoToUpdate(CategoryDto categoryDto) {
        return this.isValidCategoryDtoToSave(categoryDto) && categoryDto.getId() != null && categoryDto.getId() > 0;
    }

    private List<CategoryDto> convertCategoryDaoListToCategoryDtoList(List<Category> categoryList) throws ServiceException {
        List<CategoryDto> categoryDtoList = new ArrayList<>();

        for (Category category : categoryList) {
            categoryDtoList.add(this.convertCategoryToCategoryDto(category));
        }

        return categoryDtoList;
    }
}
