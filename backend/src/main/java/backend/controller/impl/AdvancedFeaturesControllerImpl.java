package backend.controller.impl;

import backend.controller.AdvancedFeaturesController;
import backend.dtos.ErrorDto;
import backend.dtos.MessageDto;
import backend.dtos.ResponseDto;
import backend.exceptions.ServiceException;
import backend.services.AdvancedFeaturesService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Slf4j
@RestController
@RequestMapping("/api/advanced/")
public class AdvancedFeaturesControllerImpl implements AdvancedFeaturesController {

    private AdvancedFeaturesService advancedFeaturesService;

    @Autowired
    public AdvancedFeaturesControllerImpl(AdvancedFeaturesService advancedFeaturesService) {
        this.advancedFeaturesService = advancedFeaturesService;
    }


    @Override
    @ResponseBody
    @PreAuthorize("hasAnyRole('ADMIN', 'BUSINESS', 'CUSTOMER')")
    @RequestMapping(value = "correct/{query}", method = RequestMethod.GET)
    public ResponseDto correct(@PathVariable String query) {
        try {
            List<MessageDto> correctionResults = this.advancedFeaturesService.correct(query);
            return new ResponseDto<>(false, correctionResults);
        } catch (ServiceException e) {
            log.error(e.getMessage());
            return new ResponseDto<>(true, new ErrorDto(e.getCode(), e.getMessage()));
        }
    }

    @Override
    @ResponseBody
    @PreAuthorize("hasAnyRole('ADMIN', 'BUSINESS', 'CUSTOMER')")
    @RequestMapping(value = "autocomplete/{query}", method = RequestMethod.GET)
    public ResponseDto autocomplete(@PathVariable String query) {
        try {
            List<MessageDto> autocompletionResults = this.advancedFeaturesService.autocomplete(query);
            return new ResponseDto<>(false, autocompletionResults);
        } catch (ServiceException e) {
            log.error(e.getMessage());
            return new ResponseDto<>(true, new ErrorDto(e.getCode(), e.getMessage()));
        }
    }

    @Override
    @ResponseBody
    @PreAuthorize("hasAnyRole('ADMIN', 'BUSINESS', 'CUSTOMER')")
    @RequestMapping(value = "search/{query}", method = RequestMethod.GET)
    public ResponseDto search(@PathVariable String query) {
        try {
            List<MessageDto> searchResults = this.advancedFeaturesService.search(query);
            return new ResponseDto<>(false, searchResults);
        } catch (ServiceException e) {
            log.error(e.getMessage());
            return new ResponseDto<>(true, new ErrorDto(e.getCode(), e.getMessage()));
        }
    }

    @Override
    @ResponseBody
    @PreAuthorize("hasAnyRole('ADMIN', 'BUSINESS', 'CUSTOMER')")
    @RequestMapping(value = "recommendhistory/{uid}", method = RequestMethod.GET)
    public ResponseDto recommendHistory(@PathVariable String uid) {
        try {
            List<MessageDto> recommendHistoryResults = this.advancedFeaturesService.recommendHistory(uid);
            return new ResponseDto<>(false, recommendHistoryResults);
        } catch (ServiceException e) {
            log.error(e.getMessage());
            return new ResponseDto<>(true, new ErrorDto(e.getCode(), e.getMessage()));
        }
    }

    @Override
    @ResponseBody
    @PreAuthorize("hasAnyRole('ADMIN', 'BUSINESS', 'CUSTOMER')")
    @RequestMapping(value = "alsobooked/{pid}", method = RequestMethod.GET)
    public ResponseDto alsoBooked(@PathVariable String pid) {
        try {
            List<MessageDto> alsoBookedResults = this.advancedFeaturesService.alsoBooked(pid);
            return new ResponseDto<>(false, alsoBookedResults);
        } catch (ServiceException e) {
            log.error(e.getMessage());
            return new ResponseDto<>(true, new ErrorDto(e.getCode(), e.getMessage()));
        }
    }
}