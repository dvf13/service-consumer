package backend.dtos;

import com.fasterxml.jackson.annotation.JsonRootName;
import lombok.Data;

@Data
@JsonRootName("category")
public class CategoryDto {

    private Long id;

    private String name;
}
