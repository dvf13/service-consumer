package backend.controller;

import backend.controller.impl.CustomerControllerImpl;
import backend.dtos.CustomerDto;
import backend.dtos.ErrorDto;
import backend.dtos.ResponseDto;
import backend.exceptions.ServiceException;
import backend.services.CustomerService;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.hamcrest.Matchers.instanceOf;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

public class CustomerControllerImplTest {

    private CustomerService customerService;
    private CustomerController customerController;

    public CustomerControllerImplTest() {
        this.customerService = mock(CustomerService.class);
        this.customerController = new CustomerControllerImpl(customerService);
    }

    @Test
    public void getAllCustomersTest_NOError() throws ServiceException {
        when(this.customerService.getAllCustomers()).thenReturn(this.getAllCustomerDto());

        ResponseDto responseDto = this.customerController.getAllCustomers();

        assertFalse(responseDto.isError());
        assertNotNull(responseDto);
        assertNotNull(responseDto.getData());
        assertThat(responseDto.getData(), instanceOf(ArrayList.class));
        assertEquals(getAllCustomerDto(), responseDto.getData());
    }

    @Test
    public void getAllCustomersTest_Error() throws ServiceException {
        when(this.customerService.getAllCustomers()).thenThrow(ServiceException.class);

        ResponseDto responseDto = this.customerController.getAllCustomers();

        assertTrue(responseDto.isError());
        assertNotNull(responseDto);
        assertNotNull(responseDto.getData());
        assertThat(responseDto.getData(), instanceOf(ErrorDto.class));
    }

    @Test
    public void getCustomerByIdTest_NOError() throws ServiceException {
        when(this.customerService.getCustomerById(Mockito.anyLong())).thenReturn(getCustomerDto());

        ResponseDto responseDto = this.customerController.getCustomerById(1L);

        assertFalse(responseDto.isError());
        assertNotNull(responseDto);
        assertNotNull(responseDto.getData());
        assertThat(responseDto.getData(), instanceOf(CustomerDto.class));
        assertEquals(getCustomerDto(), responseDto.getData());
    }

    @Test
    public void getCustomerByIdTest_Error() throws ServiceException {
        when(this.customerService.getCustomerById(Mockito.anyLong())).thenThrow(ServiceException.class);

        ResponseDto responseDto = this.customerController.getCustomerById(-1L);

        assertTrue(responseDto.isError());
        assertNotNull(responseDto);
        assertNotNull(responseDto.getData());
        assertThat(responseDto.getData(), instanceOf(ErrorDto.class));
    }

    @Test
    public void saveCustomerTest_NOError() throws ServiceException {
        when(this.customerService.saveCustomer(any(CustomerDto.class))).thenReturn(getCustomerDto());

        ResponseDto responseDto = this.customerController.saveCustomer(getCustomerDto());

        assertFalse(responseDto.isError());
        assertNotNull(responseDto);
        assertNotNull(responseDto.getData());
        assertThat(responseDto.getData(), instanceOf(CustomerDto.class));
        assertEquals(getCustomerDto(), responseDto.getData());
    }

    @Test
    public void saveCustomerTest_Error() throws ServiceException {
        when(this.customerService.saveCustomer(any(CustomerDto.class))).thenThrow(ServiceException.class);

        ResponseDto responseDto = this.customerController.saveCustomer(null);

        assertTrue(responseDto.isError());
        assertNotNull(responseDto);
        assertNotNull(responseDto.getData());
        assertThat(responseDto.getData(), instanceOf(ErrorDto.class));
    }

    @Test
    public void updateCustomerTest_NOError() throws ServiceException {
        when(this.customerService.updateCustomer(Mockito.anyLong(), any(CustomerDto.class)))
                .thenReturn(getCustomerDto());

        ResponseDto responseDto = this.customerController.updateCustomer(1L, getCustomerDto());

        assertFalse(responseDto.isError());
        assertNotNull(responseDto);
        assertNotNull(responseDto.getData());
        assertThat(responseDto.getData(), instanceOf(CustomerDto.class));
        assertEquals(getCustomerDto(), responseDto.getData());
    }

    @Test
    public void updateCustomerTest_Error() throws ServiceException {
        when(this.customerService.updateCustomer(Mockito.anyLong(), any(CustomerDto.class)))
                .thenThrow(ServiceException.class);

        ResponseDto responseDto = this.customerController.updateCustomer(1L, null);

        assertTrue(responseDto.isError());
        assertNotNull(responseDto);
        assertNotNull(responseDto.getData());
        assertThat(responseDto.getData(), instanceOf(ErrorDto.class));
    }

    @Test
    public void deleteCustomerByIdTest_Error() throws ServiceException {
        doThrow(ServiceException.class).when(this.customerService).deleteCustomerById(Mockito.anyLong());

        this.customerController.deleteCustomerById(-1L);
    }

    private CustomerDto getCustomerDto() {
        CustomerDto customerDto = new CustomerDto();
        customerDto.setId(1L);
        customerDto.setUsername("Test_Username");
        customerDto.setPassword("Test_PasswordHash");
        customerDto.setEmail("mail@test.com");
        customerDto.setFirstName("Test_First_Name");
        customerDto.setLastName("Test_Last_Name");
        customerDto.setBirthdate(new Date(1991, 1, 1));
        customerDto.setGender('M');
        return customerDto;
    }

    private List<CustomerDto> getAllCustomerDto() {
        List<CustomerDto> customerArrayList = new ArrayList<>();
        customerArrayList.add(getCustomerDto());
        return customerArrayList;
    }
}
