package backend.dtos;

import backend.models.TimeSlot;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;
import lombok.Data;

import java.io.File;
import java.util.Date;
import java.util.List;

@Data
@JsonRootName("offer")
public class OfferDto {

    private Long id;

    @JsonProperty("business")
    private BusinessDto businessDto;

    @JsonProperty("category")
    private CategoryDto categoryDto;

    private String title;

    private String description;

    private String pictureUrl;

    private String picture;

    private double latitude;

    private double longitude;

    private int price;

    private Double discount;

    private int amount;

    private Date validUntil;

    private Date createdAt;

    @JsonIgnore
    private Date updatedAt;

    @JsonProperty("timeSlotList")
    @JsonManagedReference
    private List<TimeSlotDto> timeSlotDtoList;

}
