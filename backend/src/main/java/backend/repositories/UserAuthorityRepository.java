package backend.repositories;

import backend.models.UserAuthority;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * UserAuthority repository for jpa
 */
@Repository
public interface UserAuthorityRepository extends JpaRepository<UserAuthority, Long> {
}
