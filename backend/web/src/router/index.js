import Vue from 'vue';
import Router from 'vue-router';
import BootstrapVue from 'bootstrap-vue';
import VueResource from 'vue-resource';
import Home from '@/components/Home';
import Offer from '@/components/Offer';
import OfferDetail from '@/components/OfferDetail';
import OfferEdit from '@/components/OfferEdit';
import OfferNew from '@/components/OfferNew';
import CustomerRegistration from '@/components/CustomerRegistration';
import BusinessRegistration from '@/components/BusinessRegistration';
import CustomerProfile from '@/components/CustomerProfile';
import BusinessProfile from '@/components/BusinessProfile';
import BusinessUserActualOffers from '@/components/BusinessUserActualOffers';
import BusinessUserExpiredOffers from '@/components/BusinessUserExpiredOffers';
import BusinessUserProvisionDetails from '@/components/BusinessUserProvisionDetails';
import BusinessUserAdvancedFeatures from '@/components/BusinessUserAdvancedFeatures';
import CustomerUserReservedOffers from '@/components/CustomerUserReservedOffers';
import CustomerUserBookedOffers from '@/components/CustomerUserBookedOffers';
import CustomerUserCancelledOffers from '@/components/CustomerUserCancelledOffers';
import AdminLogin from '@/components/AdminLogin';
import AdminOverview from '@/components/AdminOverview';
import AdminDetails from '@/components/AdminDetails';
import BusinessRecommendations from '@/components/BusinessRecommendations';
import Business from '@/components/Business';
import AlsoBookedComponent from '@/components/AlsoBookedComponent';

Vue.use(VueResource);
Vue.use(BootstrapVue);
Vue.use(Router);

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home,
    },
    {
      path: '/offer',
      name: 'Offer',
      component: Offer,
    },
    {
      path: '/offer/edit/:id',
      name: 'OfferEdit',
      component: OfferEdit,
    },
    {
      path: '/offer/new',
      name: 'OfferNew',
      component: OfferNew,
    },
    {
      path: '/offer/:id',
      name: 'OfferDetail',
      component: OfferDetail,
    },
    {
      path: '/customer_registration',
      name: 'CustomerRegistration',
      component: CustomerRegistration,
    },
    {
      path: '/business_registration',
      name: 'BusinessRegistration',
      component: BusinessRegistration,
    },
    {
      path: '/customer_profile',
      name: 'CustomerProfile',
      component: CustomerProfile,
    },
    {
      path: '/business_profile',
      name: 'BusinessProfile',
      component: BusinessProfile,
    },
    {
      path: '/business_user_actual_offers',
      name: 'BusinessUserActualOffes',
      component: BusinessUserActualOffers,
    },
    {
      path: '/business_user_expired_offers',
      name: 'BusinessUserExpiredOffes',
      component: BusinessUserExpiredOffers,
    },
    {
      path: '/business_user_provision_details',
      name: 'BusinessUserProvisionDetails',
      component: BusinessUserProvisionDetails,
    },
    {
      path: '/business_user_advanced_features',
      name: 'BusinessUserAdvancedFeatures',
      component: BusinessUserAdvancedFeatures,
    },
    {
      path: '/business_recommendations',
      name: 'BusinessRecommendations',
      component: BusinessRecommendations,
    },
    {
      path: '/business/:id',
      name: 'Business',
      component: Business,
    },
    {
      path: '/customer_user_reserved_offers',
      name: 'CustomerUserReservedOffes',
      component: CustomerUserReservedOffers,
    },
    {
      path: '/customer_user_booked_offers',
      name: 'CustomerUserBookedOffes',
      component: CustomerUserBookedOffers,
    },
    {
      path: '/customer_user_cancelled_offers',
      name: 'CustomerUserCancelledOffes',
      component: CustomerUserCancelledOffers,
    },
    {
      path: '/admin',
      name: 'AdminLogin',
      component: AdminLogin,
    },
    {
      path: '/admin_overview',
      name: 'AdminOverview',
      component: AdminOverview,
    },
    {
      path: '/admin_details',
      name: 'AdminDetails',
      component: AdminDetails,
    },
    {
      path: '/also_booked/:id',
      name: 'AlsoBookedComponent',
      component: AlsoBookedComponent,
    },

  ],
});
