package backend.repositories;

import backend.models.Offer;
import backend.models.TimeSlot;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Offer repository for jpa
 */
@Repository
public interface TimeSlotRepository extends JpaRepository<TimeSlot, Long> {

    @Query("SELECT t from timeslot t inner join t.offer o where o.id = :offer_id")
    List<TimeSlot> findByOfferID(@Param("offer_id") Long offer_id);
}