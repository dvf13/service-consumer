package backend.services;

import backend.exceptions.ServiceException;
import backend.models.Business;
import backend.dtos.BusinessDto;

import java.util.List;

public interface BusinessService {

    List<BusinessDto> getAllBusinesses() throws ServiceException;

    List<BusinessDto> getBusinessesByUsername(String username) throws ServiceException;

    BusinessDto getBusinessById(Long id) throws ServiceException;

    BusinessDto saveBusiness(BusinessDto offerDto) throws ServiceException;

    void deleteBusinessById(Long id) throws ServiceException;

    BusinessDto updateBusiness(Long id, BusinessDto offerDto) throws ServiceException;

    BusinessDto convertBusinessToBusinessDto(Business business) throws ServiceException;

    Business convertBusinessDtoToBusiness(BusinessDto businessDto) throws ServiceException;

    boolean usernameAlreadyExists(Long businessId, String username);

    boolean emailAlreadyExists(Long businessId, String email);

    boolean isValidBusinessDtoToSave(BusinessDto businessDto);

    boolean isValidBusinessDtoToUpdate(BusinessDto businessDto);
}
