package backend.services.impl;

import backend.dtos.CustomerDto;
import backend.exceptions.ServiceException;
import backend.models.Customer;
import backend.models.UserAuthority;
import backend.repositories.AuthorityRepository;
import backend.repositories.CustomerRepository;
import backend.services.CustomerService;
import backend.services.ValidationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Service
public class CustomerServiceImpl implements CustomerService {

    private CustomerRepository customerRepository;

    private AuthorityRepository authorityRepository;

    private BCryptPasswordEncoder bCryptPasswordEncoder;

    private ValidationService validationService;


    @Autowired
    public CustomerServiceImpl(CustomerRepository customerRepository, AuthorityRepository authorityRepository, BCryptPasswordEncoder bCryptPasswordEncoder, ValidationService validationService) {
        this.customerRepository = customerRepository;
        this.authorityRepository = authorityRepository;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
        this.validationService = validationService;
    }


    @Override
    @PreAuthorize("hasRole('ADMIN')")
    public List<CustomerDto> getAllCustomers() throws ServiceException {
        List<Customer> customerList = this.customerRepository.findAll();

        if(customerList == null || customerList.isEmpty()) {
            throw new ServiceException(1033, "customer doesn’t exist");
        }
        return this.convertCustomerDaoListToCustomerDtoList(customerList);
    }

    @Override
    @PreAuthorize("hasAnyRole('ADMIN', 'BUSINESS', 'CUSTOMER')")
    public List<CustomerDto> getCustomersByUsername(String username) throws ServiceException {
        List<Customer> customerList = this.customerRepository.findCustomersByUsername(username);

        if(customerList == null || customerList.isEmpty()) {
            throw new ServiceException(1033, "customer doesn’t exist");
        }
        return this.convertCustomerDaoListToCustomerDtoList(customerList);
    }

    @Override
    @PreAuthorize("hasAnyRole('ADMIN', 'CUSTOMER')")
    public CustomerDto getCustomerById(Long id) throws ServiceException {
        if(id == null || id <= 0) {
            throw new ServiceException(1030, "invalid customer id");
        }
        Customer customer = this.customerRepository.findOne(id);

        if(customer == null) {
            throw new ServiceException(1033, "customer doesn’t exist");
        }
        return this.convertCustomerToCustomerDto(customer);
    }

    @Override
    public CustomerDto saveCustomer(CustomerDto customerDto) throws ServiceException {
        if (customerDto == null || !this.isValidCustomerDtoToSave(customerDto)) {
            throw new ServiceException(1032, "invalid customer object");
        }
        Customer customer = this.convertCustomerDtoToCustomer(customerDto);

        UserAuthority userAuthority = new UserAuthority();
        userAuthority.setAuthority(this.authorityRepository.findOne(2L));
        userAuthority.setCustomer(customer);

        customer.setUserAuthorities(new ArrayList<>(Collections.singletonList(userAuthority)));

        Customer savedCustomer = this.customerRepository.save(customer);
        return this.convertCustomerToCustomerDto(savedCustomer);
    }

    @Override
    @PreAuthorize("hasAnyRole('ADMIN', 'CUSTOMER')")
    public void deleteCustomerById(Long id) throws ServiceException {
        if(id == null || id <= 0) {
            throw new ServiceException(1030, "invalid customer id");
        }
        this.customerRepository.delete(id);
    }

    @Override
    @PreAuthorize("hasAnyRole('ADMIN', 'CUSTOMER')")
    public CustomerDto updateCustomer(Long id, CustomerDto customerDto) throws ServiceException {
        if(id == null || id <= 0) {
            throw new ServiceException(1030, "invalid customer id");
        }
        if (customerDto == null || !this.isValidCustomerDtoToUpdate(customerDto)) {
            throw new ServiceException(1032, "invalid customer object");
        }
        if(!id.equals(customerDto.getId())) {
            throw new ServiceException(1031, "customer ids doesn’t match");
        }
        Customer storedCustomer = this.customerRepository.findOne(id);

        if(storedCustomer == null) {
            throw new ServiceException(1033, "customer doesn’t exist");
        }
        Customer customer = this.convertCustomerDtoToCustomer(customerDto);

        customer.setUserAuthorities(storedCustomer.getUserAuthorities());

        if(customerDto.getPassword().equals(storedCustomer.getPasswordHash())) {
            customer.setPasswordHash(storedCustomer.getPasswordHash());
        }

        Customer updatedCustomer = this.customerRepository.save(customer);
        return this.convertCustomerToCustomerDto(updatedCustomer);
    }

    @Override
    public CustomerDto convertCustomerToCustomerDto(Customer customer) throws ServiceException {
        if(customer == null) {
            throw new ServiceException(1032, "invalid customer object");
        }
        CustomerDto customerDto = new CustomerDto();

        customerDto.setBirthdate(customer.getBirthdate());
        customerDto.setEmail(customer.getEmail());
        customerDto.setFirstName(customer.getFirstName());
        customerDto.setLastName(customer.getLastName());
        customerDto.setGender(customer.getGender());
        customerDto.setId(customer.getId());
        customerDto.setPassword(customer.getPasswordHash());
        customerDto.setUsername(customer.getUsername());

        return customerDto;
    }

    @Override
    public Customer convertCustomerDtoToCustomer(CustomerDto customerDto) throws ServiceException {
        if(customerDto == null) {
            throw new ServiceException(1032, "invalid customer object");
        }
        Customer customer = new Customer();

        if(customerDto.getPassword() != null && customerDto.getPassword().length() > 0) {
            customer.setPasswordHash(this.bCryptPasswordEncoder.encode(customerDto.getPassword()));
        }

        customer.setBirthdate(customerDto.getBirthdate());
        customer.setEmail(customerDto.getEmail());
        customer.setFirstName(customerDto.getFirstName());
        customer.setLastName(customerDto.getLastName());
        customer.setGender(customerDto.getGender());
        customer.setId(customerDto.getId());
        customer.setUsername(customerDto.getUsername());

        return customer;
    }

    @Override
    public boolean usernameAlreadyExists(Long customerId, String username) {
        List<Customer> customerList = this.customerRepository.findCustomersByUsername(username);
        return (customerList != null && customerList.size() != 0 && !customerList.get(0).getId().equals(customerId));
    }

    @Override
    public boolean emailAlreadyExists(Long customerId, String email) {
        List<Customer> customerList = this.customerRepository.findCustomersByEmail(email);
        return (customerList != null && customerList.size() != 0 && !customerList.get(0).getId().equals(customerId));
    }

    @Override
    public boolean isValidCustomerDtoToSave(CustomerDto customerDto) {
        return customerDto.getBirthdate() != null &&
                this.validationService.isValidEmail(customerDto.getEmail()) && this.validationService.isUniqueEmail(customerDto.getId(), customerDto.getEmail()) &&
                customerDto.getFirstName() != null && !customerDto.getFirstName().equals("") &&
                customerDto.getLastName() != null && !customerDto.getLastName().equals("") &&
                (customerDto.getGender() == 'f' || customerDto.getGender() == 'm') &&
                customerDto.getUsername() != null && !customerDto.getUsername().equals("");
    }

    @Override
    public boolean isValidCustomerDtoToUpdate(CustomerDto customerDto) {
        return this.isValidCustomerDtoToSave(customerDto) && customerDto.getId() != null && customerDto.getId() > 0;
    }

    private List<CustomerDto> convertCustomerDaoListToCustomerDtoList(List<Customer> customerList) throws ServiceException {
        List<CustomerDto> customerDtoList = new ArrayList<>();

        for (Customer customer : customerList) {
            customerDtoList.add(this.convertCustomerToCustomerDto(customer));
        }

        return customerDtoList;
    }
}
