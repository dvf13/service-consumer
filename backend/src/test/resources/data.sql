delete from user_authority;
delete from authority;
delete from reservation;
delete from timeslot;
delete from offer;
delete from customer;
delete from category;
delete from business;

INSERT INTO business (id, username, password_hash, email, name, street, street_number, zip_code, city) VALUES
(1,	'business1',	'$2a$10$pbxwO7wYK931KNLUq6qcxO4OQPevkg/F28cWJgwKN8rMrO7dLJ73.',	'business1@email.com',	'Company1',	'Street',	'1',	1010,	'Vienna'),
(2,	'business2',	'$2a$10$OUQzv7XDlbhzgGBFqOPkZeG8xjNd1DWOZngF0UZue7GvLft3Vq1TW',	'business2@email.com',	'Company2',	'Street',	'2',	1010,	'Vienna');

INSERT INTO `category` (id, name) VALUES
(1,	'Food'),
(2,	'Beauty');

INSERT INTO customer (id, username, password_hash, email, first_name, last_name, birthdate, gender) VALUES
(1,	'user1',	'$2a$10$pbxwO7wYK931KNLUq6qcxO4OQPevkg/F28cWJgwKN8rMrO7dLJ73.',	'user1@email.com',	'Max',	'Mustermann',	'2001-01-20 00:00:00',	'm'),
(2,	'user2',	'$2a$10$OUQzv7XDlbhzgGBFqOPkZeG8xjNd1DWOZngF0UZue7GvLft3Vq1TW',	'user2@email.com',	'Maria',	'Musterfrau',	'2001-01-20 01:00:00',	'f'),
(3,	'user3',	'$2a$10$OUQzv7XDlbhzgGBFqOPkZeG8xjNd1DWOZngF0UZue7GvLft3Vq1TW',	'user3@email.com',	'Maria',	'Musterfrau',	'2001-01-20 01:00:00',	'f');

INSERT INTO offer (id, business_id, category_id, title, description, picture_url, picture, latitude, longitude, price, discount, amount, valid_until, created_at, updated_at) VALUES
(1,	1, 1, 'Test offer',	'Test offer description', 'http://google',	LOAD_FILE(''), 48.210033, 16.363449, 500,	15.7,	1,	'2018-05-20 16:45:00',	'2018-01-01 00:00:00',	'2018-01-01 00:00:00'),
(2,	2, 2,	'Another test offer',	'Another test offer description', 'http://google',	LOAD_FILE(''), 48.210033, 16.363449, 150,	50.0,	10,	'2018-05-21 12:00:00',	'2018-03-05 00:00:00',	'2018-03-05 00:00:00');

INSERT INTO timeslot (id, offer_id, beg_date_time, end_date_time, quantity_total, quantity_booked) VALUES
(1,	1, '2018-05-05 16:30:00',	'2018-05-05 17:00:00',	5, 0),
(2,	1, '2018-05-05 17:00:00',	'2018-05-05 17:30:00',	5, 2);

INSERT INTO reservation (id, customer_id, timeslot_id, state, reserved_at) VALUES
(1, 1,	1,	0,	'2008-04-20 18:00:00'),
(2, 2,	2,	0,	'2008-04-20 18:00:00');

INSERT INTO authority (id, name) VALUES
(1,	'ROLE_BUSINESS'),
(2,	'ROLE_CUSTOMER'),
(3,	'ROLE_ADMIN');

INSERT INTO user_authority (id, authority_id, business_id, customer_id) VALUES
(1, 1, 1, NULL),
(2, 2, NULL, 1),
(3, 3, 2, NULL),
(4, 3, NULL, 2),
(5, 2, NULL, 3),
(6, 3, NULL, 3);