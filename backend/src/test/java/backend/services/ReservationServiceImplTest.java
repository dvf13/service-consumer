package backend.services;

import backend.dtos.CustomerDto;
import backend.dtos.OfferDto;
import backend.dtos.ReservationDto;
import backend.dtos.TimeSlotDto;
import backend.exceptions.ServiceException;
import backend.models.*;
import backend.repositories.CustomerRepository;
import backend.repositories.OfferRepository;
import backend.repositories.ReservationRepository;
import backend.repositories.TimeSlotRepository;
import backend.services.impl.ReservationServiceImpl;
import org.junit.Test;
import org.mockito.Mockito;


import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static org.mockito.Mockito.*;
import static org.junit.Assert.*;

public class ReservationServiceImplTest {

    private ReservationRepository reservationRepository;

    private CustomerRepository customerRepository;

    private OfferRepository offerRepository;

    private TimeSlotRepository timeSlotRepository;

    private CustomerService customerService;

    private OfferService offerService;

    private ReservationService reservationService;

    private TimeSlotService timeSlotService;

    private LoggingService loggingService;

    public ReservationServiceImplTest() {
        reservationRepository = mock(ReservationRepository.class);
        customerRepository = mock(CustomerRepository.class);
        offerRepository = mock(OfferRepository.class);
        timeSlotRepository = mock(TimeSlotRepository.class);
        customerService = mock(CustomerService.class);
        offerService = mock(OfferService.class);
        timeSlotService = mock(TimeSlotService.class);
        loggingService = mock(LoggingService.class);
        reservationService = new ReservationServiceImpl(reservationRepository, customerRepository, timeSlotRepository, offerRepository, customerService, offerService, timeSlotService, loggingService);
    }

    @Test
    public void getAllReservationsTest_positive() throws ServiceException {
        when(this.reservationRepository.findAll()).thenReturn(getAllReservations());

        List<ReservationDto> allReservations = reservationService.getAllReservations();
        assertNotNull(allReservations);
        assertEquals(1, allReservations.size());
        assertEquals(1, getAllReservations().get(0).getState());
    }

    @Test(expected = ServiceException.class)
    public void getAllReservationsTest_invalidReservation() throws ServiceException {
        when(this.reservationRepository.findAll()).thenReturn(null);

        reservationService.getAllReservations();
    }

    @Test
    public void getReservationByIdTest_positive() throws ServiceException {
        when(this.reservationRepository.findOne(Mockito.anyLong())).thenReturn(getReservation());

        ReservationDto reservationById = reservationService.getReservationById(1L);
        assertNotNull(reservationById);
    }

    @Test(expected = ServiceException.class)
    public void getReservationByIdTest_invalidReservationId() throws ServiceException {
        when(this.customerRepository.findOne(Mockito.anyLong())).thenReturn(null);
        when(this.offerRepository.findOne(Mockito.anyLong())).thenReturn(this.getOffer());
        when(this.reservationRepository.findOne(Mockito.anyLong())).thenReturn(null);

        reservationService.getReservationById(getReservation().getId());
    }

    @Test(expected = ServiceException.class)
    public void getReservationByIdTest_noCustomer() throws ServiceException {
        when(this.customerRepository.findOne(Mockito.anyLong())).thenReturn(null);
        when(this.offerRepository.findOne(Mockito.anyLong())).thenReturn(this.getOffer());
        when(this.reservationRepository.findOne(Mockito.anyLong())).thenReturn(null);

        reservationService.getReservationById(getReservation().getId());
    }

    @Test(expected = ServiceException.class)
    public void getReservationByIdTest_reservationDoesNotExist() throws ServiceException {
        when(this.customerRepository.findOne(Mockito.anyLong())).thenReturn(this.getCustomer());
        when(this.offerRepository.findOne(Mockito.anyLong())).thenReturn(this.getOffer());
        when(this.reservationRepository.findOne(Mockito.anyLong())).thenReturn(null);

        reservationService.getReservationById(getReservation().getId());
    }

    /**TODO: Maybe enable again after javascript frontend-backend object adaption
    @Test
    public void saveReservationTest_positive() throws ServiceException {
        when(this.reservationRepository.save(Mockito.any(Reservation.class))).thenReturn(getReservation());

        ReservationDto reservationDto = reservationService.saveReservation(reservationService.convertReservationToReservationDto(getReservation()), getReservation().getReservationIdentity().getTimeSlot().getId());
        assertNotNull(reservationDto);
    }
     */

    @Test(expected = ServiceException.class)
    public void saveReservationTest_invalidReservation() throws ServiceException {
        when(this.reservationRepository.save(Mockito.any(Reservation.class))).thenReturn(null);

        reservationService.saveReservation(reservationService.convertReservationToReservationDto(null));
    }

    @Test
    public void deleteReservationByIdTest_positive() throws ServiceException {
        when(this.customerRepository.findOne(Mockito.anyLong())).thenReturn(this.getCustomer());
        when(this.timeSlotRepository.findOne(Mockito.anyLong())).thenReturn(this.getTimeSlot());
        when(this.reservationRepository.findOne(Mockito.anyLong())).thenReturn(getReservation());

        reservationService.deleteReservationById(getReservation().getId());
    }

    @Test(expected = ServiceException.class)
    public void deleteReservationByIdTest_noCustomerId() throws ServiceException {
        when(this.customerRepository.findOne(Mockito.anyLong())).thenReturn(this.getCustomer());
        when(this.offerRepository.findOne(Mockito.anyLong())).thenReturn(this.getOffer());
        when(this.reservationRepository.findOne(Mockito.anyLong())).thenReturn(getReservation());

        reservationService.deleteReservationById(null);
    }

    @Test(expected = ServiceException.class)
    public void deleteReservationByIdTest_noCustomer() throws ServiceException {
        when(this.customerRepository.findOne(Mockito.anyLong())).thenReturn(null);
        when(this.offerRepository.findOne(Mockito.anyLong())).thenReturn(this.getOffer());
        when(this.reservationRepository.findOne(Mockito.anyLong())).thenReturn(getReservation());

        reservationService.deleteReservationById(null);
    }

    @Test (expected = ServiceException.class)
    public void updateReservationTest_invalidCustomerId() throws ServiceException {
        reservationService.updateReservation(null);
    }

    @Test (expected = ServiceException.class)
    public void updateReservationTest_invalidReservation() throws ServiceException {
        reservationService.updateReservation(null);
    }

    @Test (expected = ServiceException.class)
    public void updateReservationTest_idsNotMatching() throws ServiceException {
        reservationService.updateReservation(null);
    }

    @Test (expected = ServiceException.class)
    public void updateReservationTest_noRegistration() throws ServiceException {
        when(this.reservationRepository.findOne(Mockito.anyLong())).thenReturn(null);

        reservationService.updateReservation(null);
    }

    @Test
    public void isValidReservationDtoToSaveTest_positive() {

        assertTrue(this.reservationService.isValidReservationDtoToSave(getReservationDto()));
    }

    @Test
    public void isValidReservationDtoToSaveTest_notValidObject() {

        ReservationDto reservationDto = new ReservationDto();
        assertFalse(this.reservationService.isValidReservationDtoToSave(reservationDto));
    }

    @Test
    public void isValidReservationToUpdateTest_notValidObject() {
        ReservationDto reservationDto = new ReservationDto();
        // TODO: AssertFalse when the method in service is corrected
        assertTrue(this.reservationService.isValidReservationDtoToUpdate(reservationDto));
    }

    private Reservation getReservation() {
        Reservation reservation = new Reservation();
        reservation.setId(1l);
        reservation.setState(1);
        reservation.setCustomer(getCustomer());
        reservation.setTimeSlot(getTimeSlot());
        reservation.setReservedAt(Calendar.getInstance().getTime());
        return reservation;
    }

    private ReservationDto getReservationDto() {
        ReservationDto reservationDto = new ReservationDto();
        reservationDto.setReservedAt(Calendar.getInstance().getTime());
        reservationDto.setState(1);
        reservationDto.setId(1l);
        reservationDto.setCustomerDto(getCustomerDto());
        reservationDto.setTimeSlotDto(getTimeSlotDto());
        return reservationDto;
    }

    private List<Reservation> getAllReservations() {
        List<Reservation> reservationsList = new ArrayList<>();
        reservationsList.add(getReservation());
        return reservationsList;
    }

    private Customer getCustomer() {

        Customer customer = new Customer();

        customer.setId(1L);
        customer.setBirthdate(Calendar.getInstance().getTime());
        customer.setGender('M');
        customer.setFirstName("Test");
        customer.setLastName("Last");
        customer.setPasswordHash("123");
        customer.setUsername("Test_Username2");
        customer.setEmail("test@mail.com");

        return customer;

    }

    private Offer getOffer() {
        Offer offer = new Offer();
        offer.setId(1L);
        offer.setAmount(10);
        offer.setBusiness(getBusiness());
        offer.setCategory(getCategory());
        offer.setTimeSlotList(null);
        return offer;
    }

    private TimeSlot getTimeSlot() {
        TimeSlot timeSlot = new TimeSlot();
        timeSlot.setId(1L);
        timeSlot.setOffer(getOffer());
        timeSlot.setBeg_date_time(Calendar.getInstance().getTime());
        timeSlot.setEnd_date_time(Calendar.getInstance().getTime());
        timeSlot.setQuantity_total(5);
        timeSlot.setQuantity_booked(2);

        return timeSlot;
    }

    private Business getBusiness() {
        Business business = new Business();
        business.setId(1L);
        business.setName("Test_Business_User");
        return business;
    }

    private CustomerDto getCustomerDto(){

        CustomerDto customerDto = new CustomerDto();
        customerDto.setId(1L);
        customerDto.setUsername("Test_Username");
        customerDto.setEmail("mail@test.com");
        customerDto.setFirstName("Test_First_Name");
        customerDto.setLastName("Test_Last_Name");
        customerDto.setBirthdate(new Date(1991, 1,1));
        customerDto.setGender('M');

        return customerDto;
    }

    private TimeSlotDto getTimeSlotDto() {
        TimeSlotDto timeSlotDto = new TimeSlotDto();
        timeSlotDto.setId(1L);
        timeSlotDto.setOfferDto(getOfferDto());
        timeSlotDto.setBeg_date_time(Calendar.getInstance().getTime());
        timeSlotDto.setEnd_date_time(Calendar.getInstance().getTime());
        timeSlotDto.setQuantity_total(5);
        timeSlotDto.setQuantity_booked(2);

        return timeSlotDto;
    }

    private OfferDto getOfferDto() {
        OfferDto offerDto = new OfferDto();
        offerDto.setId(1L);
        offerDto.setAmount(40);
        return offerDto;
    }

    private Category getCategory() {
        Category category = new Category();
        category.setId(1L);
        category.setName("Test_Category");
        return category;
    }
}
