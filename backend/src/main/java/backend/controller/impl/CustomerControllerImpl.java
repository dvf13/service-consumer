package backend.controller.impl;

import backend.controller.CustomerController;
import backend.dtos.CustomerDto;
import backend.dtos.ErrorDto;
import backend.dtos.ResponseDto;
import backend.exceptions.ServiceException;
import backend.services.CustomerService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Slf4j
@RestController
@RequestMapping("/api/customer/")
public class CustomerControllerImpl implements CustomerController {

    private CustomerService customerService;


    @Autowired
    public CustomerControllerImpl(CustomerService customerService) {
        this.customerService = customerService;
    }


    @Override
    @ResponseBody
    @PreAuthorize("hasRole('ADMIN')")
    @RequestMapping(value = "", method = RequestMethod.GET)
    public ResponseDto getAllCustomers() {
        try {
            List<CustomerDto> customerDtoListResponse = this.customerService.getAllCustomers();
            return new ResponseDto<>(false, customerDtoListResponse);
        } catch (ServiceException e) {
            log.error(e.getMessage());
            return new ResponseDto<>(true, new ErrorDto(e.getCode(), e.getMessage()));
        }
    }

    @Override
    @ResponseBody
    @PreAuthorize("hasAnyRole('ADMIN', 'CUSTOMER')")
    @RequestMapping(value = "{id}", method = RequestMethod.GET)
    public ResponseDto getCustomerById(@PathVariable Long id) {
        try {
            CustomerDto customerDtoResponse = this.customerService.getCustomerById(id);
            return new ResponseDto<>(false, customerDtoResponse);
        } catch (ServiceException e) {
            log.error(e.getMessage());
            return new ResponseDto<>(true, new ErrorDto(e.getCode(), e.getMessage()));
        }
    }

    @Override
    @ResponseBody
    @RequestMapping(value = "", method = RequestMethod.POST)
    public ResponseDto saveCustomer(@RequestBody CustomerDto customerDto) {
        try {
            CustomerDto customerDtoResponse = this.customerService.saveCustomer(customerDto);
            return new ResponseDto<>(false, customerDtoResponse);
        } catch (ServiceException e) {
            log.error(e.getMessage());
            return new ResponseDto<>(true, new ErrorDto(e.getCode(), e.getMessage()));
        }
    }

    @Override
    @ResponseBody
    @PreAuthorize("hasAnyRole('ADMIN', 'CUSTOMER')")
    @RequestMapping(value = "{id}", method = RequestMethod.DELETE)
    public void deleteCustomerById(@PathVariable Long id) {
        try {
            this.customerService.deleteCustomerById(id);
        } catch (ServiceException e) {
            log.error(e.getMessage());
        }
    }

    @Override
    @ResponseBody
    @PreAuthorize("hasAnyRole('ADMIN', 'CUSTOMER')")
    @RequestMapping(value = "{id}", method = RequestMethod.PUT)
    public ResponseDto updateCustomer(@PathVariable Long id, @RequestBody CustomerDto customerDto) {
        try {
            CustomerDto customerDtoResponse = this.customerService.updateCustomer(id, customerDto);
            return new ResponseDto<>(false, customerDtoResponse);
        } catch(ServiceException e) {
            log.error(e.getMessage());
            return new ResponseDto<>(true, new ErrorDto(e.getCode(), e.getMessage()));
        }
    }
}
