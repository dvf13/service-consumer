let fs = require("fs"),
    config,
    started = false,
    running = false,
    users = [],
    businessUsers = [],
    CONFIG = JSON.parse(fs.readFileSync(process.env['CONFIG_FILE']));

module.exports = {

  setDefault(){
    config = undefined;
    started = false;
    running = false;
    users = [];
    businessUsers = [];
    return true;
  },

  getConfig(){
    if(config) return config;
    return this.getDefaultConfig();
  },

  setConfig(newConfig){
    config = newConfig;
    return true;
  },

  getDefaultConfig(){
    config = {apiEndpoint: `https://localhost:${CONFIG.backend_port}${CONFIG.api_endpoint}`, concurrentUsers: CONFIG.concurrentUsers, concurrentBusinesses: CONFIG.concurrentBusinesses, registerEndpointUser: CONFIG.registerEndpointUser, registerEndpointBusiness: CONFIG.registerEndpointBusiness, requestProcessing: 'concurrently'};
    return config;
  },

  getUsers(){
    return users;
  },

  setUsers(newUsers){
    users = newUsers;
    return true;
  },

  getBusinessUsers(){
    return businessUsers;
  },

  setBusinessUsers(newBusinessUsers){
    businessUsers = newBusinessUsers;
    return true;
  },

  isStarted(){
    return started;
  },

  setStarted(newStartedStatus){
    started = newStartedStatus;
    return true;
  },


  isRunning(){
    return running;
  },

  setRunning(newRunningStatus){
    running = newRunningStatus;
    return true;
  }
};