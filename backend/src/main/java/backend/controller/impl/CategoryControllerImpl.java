package backend.controller.impl;

import backend.controller.CategoryController;
import backend.dtos.CategoryDto;
import backend.dtos.ErrorDto;
import backend.dtos.ResponseDto;
import backend.exceptions.ServiceException;
import backend.services.CategoryService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Slf4j
@RestController
@RequestMapping("/api/category/")
public class CategoryControllerImpl implements CategoryController {

    private CategoryService categoryService;


    @Autowired
    public CategoryControllerImpl(CategoryService categoryService) {
        this.categoryService = categoryService;
    }


    @Override
    @ResponseBody
    @RequestMapping(value = "", method = RequestMethod.GET)
    public ResponseDto getAllCategories() {
        try {
            List<CategoryDto> categoryDtoListResponse = this.categoryService.getAllCategories();
            return new ResponseDto<>(false, categoryDtoListResponse);
        } catch (ServiceException e) {
            log.error(e.getMessage());
            return new ResponseDto<>(true, new ErrorDto(e.getCode(), e.getMessage()));
        }
    }

    @Override
    @ResponseBody
    @PreAuthorize("hasAnyRole('ADMIN', 'BUSINESS', 'CUSTOMER')")
    @RequestMapping(value = "{id}", method = RequestMethod.GET)
    public ResponseDto getCategoryById(@PathVariable Long id) {
        try {
            CategoryDto categoryDtoResponse = this.categoryService.getCategoryById(id);
            return new ResponseDto<>(false, categoryDtoResponse);
        } catch (ServiceException e) {
            log.error(e.getMessage());
            return new ResponseDto<>(true, new ErrorDto(e.getCode(), e.getMessage()));
        }
    }

    @Override
    @ResponseBody
    @PreAuthorize("hasRole('ADMIN')")
    @RequestMapping(value = "", method = RequestMethod.POST)
    public ResponseDto saveCategory(@RequestBody CategoryDto categoryDto) {
        try {
            CategoryDto categoryDtoResponse = this.categoryService.saveCategory(categoryDto);
            return new ResponseDto<>(false, categoryDtoResponse);
        } catch (ServiceException e) {
            log.error(e.getMessage());
            return new ResponseDto<>(true, new ErrorDto(e.getCode(), e.getMessage()));
        }
    }

    @Override
    @ResponseBody
    @PreAuthorize("hasRole('ADMIN')")
    @RequestMapping(value = "{id}", method = RequestMethod.DELETE)
    public void deleteCategoryById(@PathVariable Long id) {
        try {
            this.categoryService.deleteCategoryById(id);
        } catch (ServiceException e) {
            log.error(e.getMessage());
        }
    }

    @Override
    @ResponseBody
    @PreAuthorize("hasRole('ADMIN')")
    @RequestMapping(value = "{id}", method = RequestMethod.PUT)
    public ResponseDto updateCategory(@PathVariable Long id, @RequestBody CategoryDto categoryDto) {
        try {
            CategoryDto categoryDtoResponse = this.categoryService.updateCategory(id, categoryDto);
            return new ResponseDto<>(false, categoryDtoResponse);
        } catch(ServiceException e) {
            log.error(e.getMessage());
            return new ResponseDto<>(true, new ErrorDto(e.getCode(), e.getMessage()));
        }
    }
}
