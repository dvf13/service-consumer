package backend.controller;

import backend.dtos.CategoryDto;
import backend.dtos.ResponseDto;

import java.util.List;

public interface CategoryController {

    ResponseDto getAllCategories();

    ResponseDto getCategoryById(Long id);

    ResponseDto saveCategory(CategoryDto categoryDto);

    void deleteCategoryById(Long id);

    ResponseDto updateCategory(Long id, CategoryDto categoryDto);
}
