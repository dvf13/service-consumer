package backend.controller;

import backend.dtos.MessageDto;
import backend.dtos.ResponseDto;

import java.util.List;

public interface AdvancedFeaturesController {

    ResponseDto correct(String query);

    ResponseDto autocomplete(String query);

    ResponseDto search(String query);

    ResponseDto recommendHistory(String query);

    ResponseDto alsoBooked(String query);
}
