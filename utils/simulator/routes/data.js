'use strict';

let _ = require('lodash'),
  striptags = require('striptags'),
  sanitizer = require('sanitizer'),
  fs = require("fs"),
  NodeRequestParser = require('node-request-parser'),
  StateService = require('../services/StateService'),
  MetricsService = require('../services/MetricsService');

const express = require('express'),
  router = express.Router();

//initialize request parser
let requestParserOptions = {disableRegex: true, sanitizeFunction: (property) => sanitizer.sanitize(striptags(property))};
let requestParser = new NodeRequestParser(requestParserOptions);

router.get('/', (req, res, next) => {
  let data = processData();
  res.json({success: true, data: data});
});

router.post('/reset/', (req, res, next) => {
  MetricsService.reset();
  let data = processData();
  res.json({success: true, data: data});
});

let processData = () => {
  let metrics = MetricsService.getData();
  let users = StateService.getUsers();
  let businessUsers = StateService.getBusinessUsers();
  let loggedInUsers = _.filter(users, function(u) { return !!u.authToken; });
  let loggedInBusinessUsers = _.filter(businessUsers, function(u) { return !!u.authToken; });
  let data = {};

  let userData = {};
  userData.userCount = users.length;
  userData.businessUserCount = businessUsers.length;
  userData.loggedInUsersCount = loggedInUsers.length;
  userData.loggedInBusinessesCount = loggedInBusinessUsers.length;

  let sumUserCreationTime = 0;
  let usersCreated = 0;
  if(_.has(metrics, ['/registration/customer/', 'loggedTimes', 'POST'])){
    _.forEach(metrics['/registration/customer/'].loggedTimes['POST'], (time) => {
      sumUserCreationTime += time;
      usersCreated ++;
    });
  }

  let sumUserLoginTime = 0;
  let usersLoggedIn = 0;
  if(_.has(metrics, ['/auth/login/', 'loggedTimes', 'POST'])){
    _.forEach(metrics['/auth/login/'].loggedTimes['POST'], (time) => {
      sumUserLoginTime += time;
      usersLoggedIn ++;
    });
  }

  let sumBusinessCreationTime = 0;
  let businessesCreated = 0;
  if(_.has(metrics, ['/registration/business/', 'loggedTimes', 'POST'])){
    _.forEach(metrics['/registration/business/'].loggedTimes['POST'], (time) => {
      sumBusinessCreationTime += time;
      businessesCreated ++;
    });
  }

  userData.avgUserCreationTime = sumUserCreationTime / usersCreated;
  userData.avgBusinessCreationTime = sumBusinessCreationTime / businessesCreated;
  userData.avgLoginTime = sumUserLoginTime / usersLoggedIn;

  data.user = userData;

  if(metrics.errors){
    data.errors = metrics.errors;
  } else{
    data.errors = {};
  }



  let getOfferData = {};
  let sumGetOfferRequestProcessingTime = 0;
  let getOfferRequestsMade = 0;
  if(_.has(metrics, ['/offer/', 'loggedTimes', 'GET'])){
    _.forEach(metrics['/offer/'].loggedTimes['GET'], (time) => {
      sumGetOfferRequestProcessingTime += time;
      getOfferRequestsMade ++;
    });
  }

  getOfferData.requestCount = getOfferRequestsMade;
  getOfferData.avgRequestProcessingTime = sumGetOfferRequestProcessingTime / getOfferRequestsMade;

  data.getOffer = getOfferData;



  let createOfferData = {};
  let sumCreateOfferRequestProcessingTime = 0;
  let createOfferRequestsMade = 0;
  if(_.has(metrics, ['/offer/', 'loggedTimes', 'POST'])){
    _.forEach(metrics['/offer/'].loggedTimes['POST'], (time) => {
      sumCreateOfferRequestProcessingTime += time;
      createOfferRequestsMade ++;
    });
  }

  createOfferData.requestCount = createOfferRequestsMade;
  createOfferData.avgRequestProcessingTime = sumCreateOfferRequestProcessingTime / createOfferRequestsMade;

  data.createOffer = createOfferData;



  // TODO: additional data implemented here
  return data;
};

module.exports = router;
