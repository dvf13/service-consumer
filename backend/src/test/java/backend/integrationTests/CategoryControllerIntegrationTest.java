package backend.integrationTests;

import backend.dtos.BusinessDto;
import backend.dtos.CategoryDto;
import backend.dtos.OfferDto;
import backend.dtos.ResponseDto;
import backend.integrationTests.util.SSLUtil;
import backend.integrationTests.util.IntegTestsUtil;
import org.hibernate.integrator.spi.ServiceContributingIntegrator;
import org.json.JSONObject;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
@ActiveProfiles("test")
@Sql(executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD, scripts="/../resources/data.sql")
public class CategoryControllerIntegrationTest {

    @Autowired
    private TestRestTemplate testRestTemplate;

    static {
        javax.net.ssl.HttpsURLConnection.setDefaultHostnameVerifier(
                (hostname, sslSession) -> hostname.equals("localhost"));
    }

    @Before
    public void turnOffSslChecking() throws Exception {
        SSLUtil.deactivateSslChecking();
    }

    @After
    public void turnOnSslChecking() throws Exception {
        SSLUtil.activateSslChecking();
    }

    @Test
    public void getAllCategoriesAsLoggedInUser() throws Exception{
        assertTrue(IntegTestsUtil.login("user1", "12345", this.testRestTemplate));

        HttpHeaders authHeader = IntegTestsUtil.getAuthHeader();

        HttpEntity<CategoryDto> entity = new HttpEntity<>(null, authHeader);

        ResponseEntity<ResponseDto> responseEntity = this.testRestTemplate.exchange("/api/category/", HttpMethod.GET, entity, ResponseDto.class);
        ResponseDto responseDto = responseEntity.getBody();

        ArrayList<JSONObject> result = IntegTestsUtil.convertArrayListOfLinkedHashMapsToArrayListOfJsonObjects((ArrayList<LinkedHashMap>) responseDto.getData());

        assertEquals(2, result.size());

        assertEquals(1, result.get(0).getInt("id"));
        assertEquals("Food", result.get(0).getString("name"));

        assertEquals(2, result.get(1).getInt("id"));
        assertEquals("Beauty", result.get(1).getString("name"));

        assertTrue(IntegTestsUtil.logout(this.testRestTemplate));
    }

    @Test
    public void getCategoryrById_loggedInUser() throws Exception{
        assertTrue(IntegTestsUtil.login("user1", "12345", this.testRestTemplate));

        HttpHeaders authHeader = IntegTestsUtil.getAuthHeader();

        HttpEntity<OfferDto> entity = new HttpEntity<>(null, authHeader);

        ResponseEntity<ResponseDto> responseEntity = this.testRestTemplate.exchange("/api/category/1", HttpMethod.GET, entity, ResponseDto.class);
        ResponseDto responseDto = responseEntity.getBody();

        JSONObject receivedOffer = IntegTestsUtil.convertLinkedHashMapToJsonObject((LinkedHashMap) responseDto.getData());

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertFalse(responseDto.isError());
        assertEquals("1", String.valueOf(receivedOffer.getLong("id")));
        assertEquals("Food", receivedOffer.getString("name"));

        assertTrue(IntegTestsUtil.logout(this.testRestTemplate));
    }

    //Should be redirected
//    @Test
//    public void getCategoryrById_NotLoggedIn() throws Exception{
//        ResponseEntity<String> responseEntity = this.testRestTemplate.getForEntity("/api/category/1", String.class);
//        System.out.println(responseEntity.getBody());
//    }

    //Should be redirected
//    @Test
//    public void getAllCategoriesNotLoggedIn() throws Exception{
//        ResponseEntity<String> responseEntity = this.testRestTemplate.getForEntity("/api/category/", String.class);
//        System.out.println(responseEntity.getStatusCode());
//    }

    @Test
    public void saveCategoryAsAdmin() throws Exception{
        assertTrue(IntegTestsUtil.login("user3", "123", this.testRestTemplate));

        HttpHeaders authHeader = IntegTestsUtil.getAuthHeader();

        CategoryDto categoryDtoToSave = this.createNewCategoryDto();

        HttpEntity<CategoryDto> entity = new HttpEntity<>(categoryDtoToSave, authHeader);

        ResponseEntity<ResponseDto> responseEntity = this.testRestTemplate.exchange("/api/category/", HttpMethod.POST, entity, ResponseDto.class);
        ResponseDto responseDto = responseEntity.getBody();

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertFalse(responseDto.isError());

        JSONObject savedOffer = IntegTestsUtil.convertLinkedHashMapToJsonObject((LinkedHashMap) responseDto.getData());

        assertEquals(categoryDtoToSave.getName(), savedOffer.getString("name"));

        HttpEntity entity1 = new HttpEntity(authHeader);

        responseEntity = this.testRestTemplate.exchange("/api/category/", HttpMethod.GET, entity1, ResponseDto.class);
        responseDto = responseEntity.getBody();

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertFalse(responseDto.isError());

        ArrayList<JSONObject> result = IntegTestsUtil.convertArrayListOfLinkedHashMapsToArrayListOfJsonObjects((ArrayList<LinkedHashMap>) responseDto.getData());

        assertEquals(3, result.size());

        assertTrue(IntegTestsUtil.logout(this.testRestTemplate));
    }

    @Test
    public void saveCategoryAsBusinessUser_shouldBeForbidden() throws Exception{
        assertTrue(IntegTestsUtil.login("business1", "12345", this.testRestTemplate));

        HttpHeaders authHeader = IntegTestsUtil.getAuthHeader();

        CategoryDto categoryDtoToSave = this.createNewCategoryDto();

        HttpEntity<CategoryDto> entity = new HttpEntity<>(categoryDtoToSave, authHeader);

        ResponseEntity<String> responseEntity = this.testRestTemplate.exchange("/api/category/", HttpMethod.POST, entity, String.class);
        String responseDto = responseEntity.getBody();

        assertEquals(HttpStatus.FORBIDDEN, responseEntity.getStatusCode());

        assertTrue(IntegTestsUtil.logout(this.testRestTemplate));
    }

    @Test
    public void saveNewCategoryAsAnonymUser_shouldGetRedirectTroughToNotLoggedIn() {
        CategoryDto categoryDto = this.createNewCategoryDto();

        ResponseEntity<ResponseDto> responseEntity = this.testRestTemplate.postForEntity("/api/category/", categoryDto, ResponseDto.class);

        assertEquals(HttpStatus.FOUND, responseEntity.getStatusCode());
    }

    // Does not work as foreign keys in table
//    @Test
//    public void deleteCategoryAsAdmin() throws Exception{
//        assertTrue(IntegTestsUtil.login("user3", "123", this.testRestTemplate));
//
//        HttpHeaders authHeader = IntegTestsUtil.getAuthHeader();
//
//        HttpEntity<CategoryDto> entity = new HttpEntity<>(null, authHeader);
//
//        ResponseEntity<String> responseEntity = this.testRestTemplate.exchange("/api/category/1", HttpMethod.DELETE, entity, String.class);
//
//        ResponseEntity<ResponseDto> responseEntity1 = this.testRestTemplate.exchange("/api/category/", HttpMethod.GET, entity, ResponseDto.class);
//        ResponseDto responseDto = responseEntity1.getBody();
//
//        assertEquals(HttpStatus.OK, responseEntity1.getStatusCode());
//        assertFalse(responseDto.isError());
//
//        ArrayList<JSONObject> result = IntegTestsUtil.convertArrayListOfLinkedHashMapsToArrayListOfJsonObjects((ArrayList<LinkedHashMap>) responseDto.getData());
//
//        assertEquals(1, result.size());
//
//        assertTrue(IntegTestsUtil.logout(this.testRestTemplate));
//    }

    private CategoryDto createNewCategoryDto(){
        CategoryDto categoryDto = new CategoryDto();
        categoryDto.setName("Test_Category");
        return categoryDto;
    }
}
