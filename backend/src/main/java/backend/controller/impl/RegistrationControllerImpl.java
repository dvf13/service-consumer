package backend.controller.impl;

import backend.controller.RegistrationController;
import backend.dtos.BusinessDto;
import backend.dtos.CustomerDto;
import backend.dtos.ErrorDto;
import backend.dtos.MessageDto;
import backend.dtos.ResponseDto;
import backend.exceptions.ServiceException;
import backend.services.RegistrationService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequestMapping("/api/registration/")
public class RegistrationControllerImpl implements RegistrationController {

    private RegistrationService registrationService;


    @Autowired
    public RegistrationControllerImpl(RegistrationService registrationService) {
        this.registrationService = registrationService;
    }


    @Override
    @ResponseBody
    @RequestMapping(value = "customer/", method = RequestMethod.POST)
    public ResponseDto registerCustomer(@RequestBody CustomerDto customerDto) {
        try {
            MessageDto messageDto = this.registrationService.registerCustomer(customerDto);
            return new ResponseDto<>(false, messageDto);
        } catch (ServiceException e) {
            log.error(e.getMessage());
            return new ResponseDto<>(true, new ErrorDto(e.getCode(), e.getMessage()));
        }
    }

    @Override
    @ResponseBody
    @RequestMapping(value = "business/", method = RequestMethod.POST)
    public ResponseDto registerBusiness(@RequestBody BusinessDto businessDto) {
        try {
            MessageDto messageDto = this.registrationService.registerBusiness(businessDto);
            return new ResponseDto<>(false, messageDto);
        } catch (ServiceException e) {
            log.error(e.getMessage());
            return new ResponseDto<>(true, new ErrorDto(e.getCode(), e.getMessage()));
        }
    }

}
