package backend.controller;

import backend.dtos.BusinessDto;
import backend.dtos.CustomerDto;
import backend.dtos.ResponseDto;
import backend.exceptions.ServiceException;

public interface RegistrationController {

    ResponseDto registerCustomer(CustomerDto customerDto);

    ResponseDto registerBusiness(BusinessDto businessDto);
}
