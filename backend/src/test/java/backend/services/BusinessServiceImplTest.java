package backend.services;

import backend.dtos.BusinessDto;
import backend.exceptions.ServiceException;
import backend.models.Business;
import backend.repositories.AuthorityRepository;
import backend.repositories.BusinessRepository;
import backend.services.impl.BusinessServiceImpl;
import org.junit.*;
import org.mockito.Mockito;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.*;
import static org.junit.Assert.*;

public class BusinessServiceImplTest {

    private BusinessRepository businessRepository;

    private AuthorityRepository authorityRepository;

    private BCryptPasswordEncoder bCryptPasswordEncoder;

    private ValidationService validationService;

    private BusinessService businessService;


    public BusinessServiceImplTest() {
        this.businessRepository = mock(BusinessRepository.class);
        this.authorityRepository = mock(AuthorityRepository.class);
        this.bCryptPasswordEncoder = mock(BCryptPasswordEncoder.class);
        this.validationService = mock(ValidationService.class);
        this.businessService = new BusinessServiceImpl(this.businessRepository, authorityRepository, bCryptPasswordEncoder, validationService);
    }


    @Test
    public void getAllBusinessesTest_ShouldFindAll() throws ServiceException {
        when(this.businessRepository.findAll()).thenReturn(this.getBusinessList());

        List<BusinessDto> businessDtoList = this.businessService.getAllBusinesses();

        assertNotNull(businessDtoList);
        assertEquals(1, businessDtoList.size());
        assertEquals((Long) 1L, businessDtoList.get(0).getId());
        assertEquals("Test_Business_User", businessDtoList.get(0).getName());
    }

    @Test
    public void getBusinessByIdTest_ValidId_ShouldFindOne() throws ServiceException {
        when(this.businessRepository.findOne(Mockito.anyLong())).thenReturn(this.getBusiness());

        BusinessDto businessDto = this.businessService.getBusinessById(1L);

        assertNotNull(businessDto);
        assertEquals((Long) 1L, businessDto.getId());
        assertEquals("Test_Business_User", businessDto.getName());
    }

    @Test(expected = ServiceException.class)
    public void getAllBusinessesTest_NoBusinessesInRepository_shouldThrowServiceException() throws ServiceException{
        when(this.businessRepository.findAll()).thenReturn(null);

        List<BusinessDto> businessDtoList = this.businessService.getAllBusinesses();

        assertNull(businessDtoList);
    }

    @Test(expected = ServiceException.class)
    public void getBusinessByIdTest_ValidIdButNotExisting_ShouldThrowServiceException() throws ServiceException {
        when(this.businessRepository.findOne(Mockito.anyLong())).thenReturn(null);

        BusinessDto businessDto = this.businessService.getBusinessById(1L);

        assertNull(businessDto);
    }

    @Test(expected = ServiceException.class)
    public void getBusinessByIdTest_InvalidId_ShouldThrowServiceException() throws ServiceException {
        when(this.businessRepository.findOne(Mockito.anyLong())).thenReturn(null);

        BusinessDto businessDto = this.businessService.getBusinessById(-1L);

        assertNull(businessDto);
    }

    @Test(expected = ServiceException.class)
    public void saveBusinessTest_BusinessNotValid_shouldThrowServiceException() throws ServiceException{
        when(this.businessRepository.save(any(Business.class))).thenReturn(null);

        BusinessDto businessDto = this.businessService.saveBusiness(null);

        assertNull(businessDto);
    }

    @Test(expected = ServiceException.class)
    public void updateBusinessTest_IdIsNull_shouldThrowServiceException() throws ServiceException{
        BusinessDto businessDto = this.businessService.updateBusiness(null, getBusinessDto());
        assertNull(businessDto);
    }

    @Test(expected = ServiceException.class)
    public void updateBusinessTest_IdBelowZero_shouldThrowServiceException() throws ServiceException{
        BusinessDto businessDto = this.businessService.updateBusiness(-1L, getBusinessDto());
        assertNull(businessDto);
    }

    @Test(expected = ServiceException.class)
    public void updateBusinessTest_BusinessDtoIsNull_shouldThrowServiceException() throws ServiceException{
        BusinessDto businessDto = this.businessService.updateBusiness(1L, null);
        assertNull(businessDto);
    }

    @Test(expected = ServiceException.class)
    public void updateBusinessTest_IdsDoNotMatch_shouldThrowServiceException() throws ServiceException{
        BusinessDto businessDto = this.businessService.updateBusiness(2L, getBusinessDto());
        assertNull(businessDto);
    }

    @Test(expected = ServiceException.class)
    public void updateBusinessTest_IdNotInRepository_shouldThrowServiceException() throws ServiceException{
        when(this.businessRepository.findOne(Mockito.anyLong())).thenReturn(null);

        BusinessDto businessDto = this.businessService.updateBusiness(1L, getBusinessDto());

        assertNull(businessDto);
    }

    @Test(expected = ServiceException.class)
    public void deleteBusinessByIdTest_IdIsNull_shouldThrowException() throws ServiceException{
        this.businessService.deleteBusinessById(null);
    }

    @Test(expected = ServiceException.class)
    public void deleteBusinessByIdTest_IdIsBelowZero_shouldThrowException() throws ServiceException{
        this.businessService.deleteBusinessById(-1L);
    }

    @Test
    public void isValidBusinessDtoToSaveTest_InvalidBusiness_ShouldBeFalse() throws ServiceException{
        BusinessDto businessDto = new BusinessDto();
        assertFalse(this.businessService.isValidBusinessDtoToSave(businessDto));
    }

    @Test
    public void isValidBusinessDtoToUpdateTest_CommitedBusinessNull() throws ServiceException{
        BusinessDto businessDto = new BusinessDto();
        assertFalse(this.businessService.isValidBusinessDtoToUpdate(businessDto));
    }

    @Test
    public void isValidBusinessDtoToUpdateTest_IdIsZero() throws ServiceException{
        BusinessDto businessDto = getBusinessDto();
        businessDto.setId(0L);
        assertFalse(this.businessService.isValidBusinessDtoToUpdate(businessDto));
    }

    private Business getBusiness() {
        Business business = new Business();
        business.setId(1L);
        business.setName("Test_Business_User");
        return business;
    }

    private List<Business> getBusinessList() {
        List<Business> businessArrayList = new ArrayList<>();
        businessArrayList.add(getBusiness());
        return businessArrayList;
    }

    private BusinessDto getBusinessDto() {
        BusinessDto businessDto = new BusinessDto();
        businessDto.setId(1L);
        businessDto.setName("Test_Business_User");
        return businessDto;
    }

    private List<BusinessDto> getBusinessDtoList() {
        List<BusinessDto> businessDtoArrayList = new ArrayList<>();
        businessDtoArrayList.add(getBusinessDto());
        return businessDtoArrayList;
    }
}




