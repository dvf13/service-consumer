package backend.services;

import backend.exceptions.ServiceException;
import backend.models.Reservation;
import backend.dtos.ReservationDto;

import java.util.Date;
import java.util.List;

public interface ReservationService {

    List<ReservationDto> getAllReservations() throws ServiceException;

    List<ReservationDto> findReservationsByCustomerId(Long customerId, Integer state) throws ServiceException;

    ReservationDto getReservationById(Long reservationID) throws ServiceException;

    List<ReservationDto> getReservationsByTimeSlotId(Long timeSlotId) throws ServiceException;

    ReservationDto saveReservation(ReservationDto reservationDto) throws ServiceException;

    void deleteReservationById(Long id) throws ServiceException;

    ReservationDto updateReservation(ReservationDto reservationDto) throws ServiceException;

    ReservationDto deleteFromWishlist(Long customerId, Long id) throws ServiceException;

    ReservationDto convertReservationToReservationDto(Reservation reservation) throws ServiceException;

    Reservation convertReservationDtoToReservation(ReservationDto reservationDto) throws ServiceException;

    boolean isValidReservationDtoToSave(ReservationDto reservationDto);

    boolean isValidReservationDtoToUpdate(ReservationDto reservationDto);

    List<ReservationDto> findReservationsByState(Integer state) throws ServiceException;
}
