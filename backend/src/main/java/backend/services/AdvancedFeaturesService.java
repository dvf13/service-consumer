package backend.services;

import backend.dtos.MessageDto;
import backend.exceptions.ServiceException;

import java.util.List;

public interface AdvancedFeaturesService {

    List<MessageDto> correct(String query) throws ServiceException;

    List<MessageDto> autocomplete(String query) throws ServiceException;

    List<MessageDto> search(String query) throws ServiceException;

    List<MessageDto> recommendHistory(String query) throws ServiceException;

    List<MessageDto> alsoBooked(String query) throws ServiceException;
}
