package backend.controller.impl;

import backend.controller.BusinessController;
import backend.dtos.BusinessDto;
import backend.dtos.ErrorDto;
import backend.dtos.ResponseDto;
import backend.exceptions.ServiceException;
import backend.services.BusinessService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Slf4j
@RestController
@RequestMapping("/api/business/")
public class BusinessControllerImpl implements BusinessController {

    private BusinessService businessService;


    @Autowired
    public BusinessControllerImpl(BusinessService businessService) {
        this.businessService = businessService;
    }


    @Override
    @ResponseBody
    @PreAuthorize("hasRole('ADMIN')")
    @RequestMapping(value = "", method = RequestMethod.GET)
    public ResponseDto getAllBusinesses() {
        try {
            List<BusinessDto> businessDtoListResponse = this.businessService.getAllBusinesses();
            return new ResponseDto<>(false, businessDtoListResponse);
        } catch (ServiceException e) {
            log.error(e.getMessage());
            return new ResponseDto<>(true, new ErrorDto(e.getCode(), e.getMessage()));
        }
    }

    @Override
    @ResponseBody
    @PreAuthorize("hasAnyRole('ADMIN', 'BUSINESS', 'CUSTOMER')")
    @RequestMapping(value = "{id}", method = RequestMethod.GET)
    public ResponseDto getBusinessById(@PathVariable Long id) {
        try {
            BusinessDto businessDtoResponse = this.businessService.getBusinessById(id);
            return new ResponseDto<>(false, businessDtoResponse);
        } catch (ServiceException e) {
            log.error(e.getMessage());
            return new ResponseDto<>(true, new ErrorDto(e.getCode(), e.getMessage()));
        }
    }

    @Override
    @ResponseBody
    @RequestMapping(value = "", method = RequestMethod.POST)
    public ResponseDto saveBusiness(@RequestBody BusinessDto businessDto) {
        try {
            BusinessDto businessDtoResponse = this.businessService.saveBusiness(businessDto);
            return new ResponseDto<>(false, businessDtoResponse);
        } catch(ServiceException e) {
            log.error(e.getMessage());
            return new ResponseDto<>(true, new ErrorDto(e.getCode(), e.getMessage()));
        }
    }

    @Override
    @ResponseBody
    @PreAuthorize("hasAnyRole('ADMIN', 'BUSINESS')")
    @RequestMapping(value = "{id}", method = RequestMethod.DELETE)
    public void deleteBusinessById(@PathVariable Long id) {
        try {
            this.businessService.deleteBusinessById(id);
        } catch (ServiceException e) {
            log.error(e.getMessage());
        }
    }

    @Override
    @ResponseBody
    @PreAuthorize("hasAnyRole('ADMIN', 'BUSINESS')")
    @RequestMapping(value = "{id}", method = RequestMethod.PUT)
    public ResponseDto updateBusiness(@PathVariable Long id, @RequestBody BusinessDto businessDto) {
        try {
            log.info("Called Update Method!");
            BusinessDto businessDtoResponse = this.businessService.updateBusiness(id, businessDto);
            return new ResponseDto<>(false, businessDtoResponse);
        } catch(ServiceException e) {
            log.error(e.getMessage());
            return new ResponseDto<>(true, new ErrorDto(e.getCode(), e.getMessage()));
        }
    }
}
