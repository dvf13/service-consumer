package backend.dtos;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonRootName;
import lombok.Data;

@Data
@JsonRootName("business")
public class BusinessDto {

    private Long id;

    private String username;

    private String password;

    private String email;

    private String name;

    private String street;

    private String streetNumber;

    private int zipCode;

    private String city;
}
