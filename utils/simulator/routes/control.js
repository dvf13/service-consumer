'use strict';

let _ = require('lodash'),
    striptags = require('striptags'),
    sanitizer = require('sanitizer'),
    fs = require("fs"),
    NodeRequestParser = require('node-request-parser'),
    StateService = require('../services/StateService'),
    UserService = require('../services/UserService'),
    MetricsService = require('../services/MetricsService');


const express = require('express'),
      router = express.Router();

//initialize request parser
let requestParserOptions = {disableRegex: true, sanitizeFunction: (property) => sanitizer.sanitize(striptags(property))};
let requestParser = new NodeRequestParser(requestParserOptions);
router.get('/', (req, res, next) => {
  res.json({success: true, config: StateService.getConfig()});
});

router.get('/default/', (req, res, next) => {
  StateService.setConfig(undefined);
  res.json({success: true, config: StateService.getDefaultConfig()});
});


router.post('/', (req, res, next) => {
  let data = requestParser.parseSync(req, ['B*apiEndpoint', 'BconcurrentUsers', 'BconcurrentBusinesses', 'B*registerEndpointUser', 'B*registerEndpointBusiness', 'B*requestProcessing']);
  if(!data || data.error){
    console.error(data.errors);
    return res.json({success: false, message: 'Wrong data passed.'});
  }
  let config = {};
  config.apiEndpoint = data.body.apiEndpoint;
  config.concurrentUsers = data.body.concurrentUsers;
  config.concurrentBusinesses = data.body.concurrentBusinesses;
  config.registerEndpointUser = data.body.registerEndpointUser;
  config.registerEndpointBusiness = data.body.registerEndpointBusiness;
  config.requestProcessing = data.body.requestProcessing;
  StateService.setConfig(config);
  res.json({success: true, message: 'Config saved successfully.'});
});

router.post('/start/', (req, res, next) => {
  if(!StateService.isStarted()){
    StateService.setStarted(true);
    UserService.createUsers((err, userCreationData) => {
      if(err) return res.json({success: false, message: 'Could not create users.'});
      StateService.setRunning(true);
      res.json({success: true, message: 'Started successfully.', data: userCreationData});
    });
  } else{
    res.json({success: false, message: 'Could not start, already running.'});
  }
});

router.get('/starting/', (req, res, next) => {
  if(StateService.isStarted()){
    res.json({success: true, starting: true});
  } else{
    res.json({success: true, starting: false});
  }
});

router.get('/running/', (req, res, next) => {
  if(StateService.isRunning()){
    res.json({success: true, running: true});
  } else{
    res.json({success: true, running: false});
  }
});

router.post('/reset/', (req, res, next) => {
  if(StateService.isStarted()){
    StateService.setDefault();
    MetricsService.resetComplete();
    res.json({success: true, reset: true, message: 'Reset successfully.'});
  } else{
    res.json({success: true, reset: false, message: 'Simulator was not started.'});
  }
});

router.get('/loggedInCount/', (req, res, next) => {
  let users = StateService.getUsers();
  let businessUsers = StateService.getBusinessUsers();
  let loggedInUsers = _.filter(users, function(u) { return !!u.authToken; });
  let loggedInBusinessUsers = _.filter(businessUsers, function(u) { return !!u.authToken; });
  res.json({success: true, loggedInCount: loggedInBusinessUsers.length + loggedInUsers.length});
});

router.get('/loggedInCount/customers/', (req, res, next) => {
  let users = StateService.getUsers();
  let loggedInUsers = _.filter(users, function(u) { return !!u.authToken; });
  res.json({success: true, loggedInCount: loggedInUsers.length});
});

router.get('/loggedInCount/businesses/', (req, res, next) => {
  let businessUsers = StateService.getBusinessUsers();
  let loggedInBusinessUsers = _.filter(businessUsers, function(u) { return !!u.authToken; });
  res.json({success: true, loggedInCount: loggedInBusinessUsers.length});
});

module.exports = router;
