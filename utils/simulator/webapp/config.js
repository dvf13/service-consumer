module.exports = {
  backend_port: 3030,
  api_endpoint: '/api',
  concurrentUsers: 1,
  concurrentBusinesses: 1,
  registerEndpointUser: '/registration/customer/',
  registerEndpointBusiness: '/registration/business/',

  apiEndpoint : 'https://localhost:8443/api'
};