package backend.services;

import backend.dtos.BusinessDto;
import backend.dtos.CustomerDto;
import backend.exceptions.ServiceException;
import backend.services.impl.RegistrationServiceImpl;
import org.junit.Test;

import java.util.Calendar;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class RegistrationServiceImplTest {

    private CustomerService customerService;

    private BusinessService businessService;

    private ValidationService validationService;

    private RegistrationService registrationService;

    public RegistrationServiceImplTest() {
        this.customerService = mock(CustomerService.class);
        this.businessService = mock(BusinessService.class);
        this.validationService = mock(ValidationService.class);
        this.registrationService = new RegistrationServiceImpl(customerService, businessService, validationService);
    }

    @Test
    public void registerCustomerTest_positive() throws ServiceException {
        when(this.validationService.isUniqueUsername(getCustomerDto().getId(), getCustomerDto().getUsername())).thenReturn(true);
        when(this.validationService.isValidEmail(getCustomerDto().getEmail())).thenReturn(true);
        when(this.validationService.isUniqueEmail(getCustomerDto().getId(), getCustomerDto().getEmail())).thenReturn(true);

        registrationService.registerCustomer(getCustomerDto());
        assertNotNull(customerService.getAllCustomers());
    }

    @Test(expected = ServiceException.class)
    public void registerCustomerTest_invalidObject() throws ServiceException {
        registrationService.registerCustomer(null);
    }

    @Test(expected = ServiceException.class)
    public void registerCustomerTest_usernameAlreadyExists() throws ServiceException {
        when(this.validationService.isUniqueUsername(getCustomerDto().getId(), getCustomerDto().getUsername())).thenReturn(false);

        registrationService.registerCustomer(getCustomerDto());
    }

    @Test(expected = ServiceException.class)
    public void registerCustomerTest_invalidEmail() throws ServiceException {
        when(this.validationService.isUniqueUsername(getCustomerDto().getId(), getCustomerDto().getUsername())).thenReturn(true);
        when(this.validationService.isValidEmail(getCustomerDto().getEmail())).thenReturn(false);

        registrationService.registerCustomer(getCustomerDto());
    }

    @Test(expected = ServiceException.class)
    public void registerCustomerTest_emailAlreadyExists() throws ServiceException {
        when(this.validationService.isUniqueUsername(getCustomerDto().getId(), getCustomerDto().getUsername())).thenReturn(true);
        when(this.validationService.isValidEmail(getCustomerDto().getEmail())).thenReturn(true);
        when(this.validationService.isUniqueEmail(getCustomerDto().getId(), getCustomerDto().getEmail())).thenReturn(false);

        registrationService.registerCustomer(getCustomerDto());
    }

    @Test
    public void registerBusinessTest_positive() throws ServiceException {
        when(this.validationService.isUniqueUsername(getBusinessDto().getId(), getBusinessDto().getUsername())).thenReturn(true);
        when(this.validationService.isValidEmail(getBusinessDto().getEmail())).thenReturn(true);
        when(this.validationService.isUniqueEmail(getBusinessDto().getId(), getBusinessDto().getEmail())).thenReturn(true);

        registrationService.registerBusiness(getBusinessDto());
        assertNotNull(businessService.getBusinessesByUsername(getBusinessDto().getUsername()));
    }

    @Test(expected = ServiceException.class)
    public void registerBusinessTest_invalidObject() throws ServiceException {
        registrationService.registerBusiness(null);
    }

    @Test(expected = ServiceException.class)
    public void registerBusinessTest_usernameAlreadyExists() throws ServiceException {
        when(this.validationService.isUniqueUsername(getBusinessDto().getId(), getBusinessDto().getUsername())).thenReturn(false);

        registrationService.registerBusiness(getBusinessDto());
    }

    @Test(expected = ServiceException.class)
    public void registerBusinessTest_invalidEmail() throws ServiceException {
        when(this.validationService.isUniqueUsername(getBusinessDto().getId(), getBusinessDto().getUsername())).thenReturn(true);
        when(this.validationService.isValidEmail(getBusinessDto().getEmail())).thenReturn(false);

        registrationService.registerBusiness(getBusinessDto());
    }

    @Test(expected = ServiceException.class)
    public void registerBusinessTest_emailAlreadyExists() throws ServiceException {
        when(this.validationService.isUniqueUsername(getBusinessDto().getId(), getBusinessDto().getUsername())).thenReturn(true);
        when(this.validationService.isValidEmail(getBusinessDto().getEmail())).thenReturn(true);
        when(this.validationService.isUniqueEmail(getBusinessDto().getId(), getBusinessDto().getEmail())).thenReturn(false);

        registrationService.registerBusiness(getBusinessDto());
    }

    private CustomerDto getCustomerDto() {

        CustomerDto customerDto = new CustomerDto();

        customerDto.setId(1L);

        customerDto.setBirthdate(Calendar.getInstance().getTime());
        customerDto.setGender('M');
        customerDto.setFirstName("Test");
        customerDto.setLastName("Last");
        customerDto.setUsername("Test_Username2");

        customerDto.setEmail("test@mail.com");

        return customerDto;

    }

    private BusinessDto getBusinessDto() {

        BusinessDto businessDto = new BusinessDto();

        businessDto.setId(2L);
        businessDto.setCity("cc");
        businessDto.setEmail("mm@gmail.com");
        businessDto.setStreet("ss");
        businessDto.setStreetNumber("12");
        businessDto.setUsername("test12");
        businessDto.setZipCode(1040);
        businessDto.setName("Test_Business_User5");

        return businessDto;

    }


}
