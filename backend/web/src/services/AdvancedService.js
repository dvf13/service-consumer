import HttpService from './HttpService';
import UserService from './UserService';
import config from '../config';

export default {

  async getRecommendations(id) {
    if (!UserService.isAuthenticated()) {
      return Promise.reject(new Error('Not logged in!'));
    }
    return HttpService.get(`${config.apiUrl}advanced/recommendhistory/${id}`)
      .then((response) => {
        if (!response.data.error) {
          return Promise.resolve(response.data.data);
        }
        // eslint-disable-next-line no-console
        console.error(`Response error message: ${response.data.data.message}`);
        return Promise.reject(response.data.data.message);
      }, error => Promise.reject(error.statusText))
      .catch(e => Promise.reject(e));
  },

  async getAlsoBooking(id) {
    if (!UserService.isAuthenticated()) {
      return Promise.reject(new Error('Not logged in!'));
    }
    return HttpService.get(`${config.apiUrl}advanced/alsobooked/${id}`)
      .then((response) => {
        if (!response.data.error) {
          return Promise.resolve(response.data.data);
        }
        // eslint-disable-next-line no-console
        console.error(`Response error message: ${response.data.data.message}`);
        return Promise.reject(response.data.data.message);
      }, error => Promise.reject(error.statusText))
      .catch(e => Promise.reject(e));
  },

};
