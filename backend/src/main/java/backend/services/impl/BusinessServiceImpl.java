package backend.services.impl;

import backend.dtos.BusinessDto;
import backend.exceptions.ServiceException;
import backend.models.Business;
import backend.models.UserAuthority;
import backend.repositories.AuthorityRepository;
import backend.repositories.BusinessRepository;
import backend.services.BusinessService;
import backend.services.ValidationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Service
public class BusinessServiceImpl implements BusinessService {

    private BusinessRepository businessRepository;

    private AuthorityRepository authorityRepository;

    private BCryptPasswordEncoder bCryptPasswordEncoder;

    private ValidationService validationService;


    @Autowired
    public BusinessServiceImpl(BusinessRepository businessRepository, AuthorityRepository authorityRepository, BCryptPasswordEncoder bCryptPasswordEncoder, ValidationService validationService) {
        this.businessRepository = businessRepository;
        this.authorityRepository = authorityRepository;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
        this.validationService = validationService;
    }


    @Override
    @PreAuthorize("hasRole('ADMIN')")
    public List<BusinessDto> getAllBusinesses() throws ServiceException {
        List<Business> businessList = this.businessRepository.findAll();

        if(businessList == null || businessList.isEmpty()) {
            throw new ServiceException(1013, "business doesn’t exist");
        }
        return this.convertBusinessDaoListToBusinessDtoList(businessList);
    }

    @Override
    @PreAuthorize("hasAnyRole('ADMIN', 'BUSINESS', 'CUSTOMER')")
    public List<BusinessDto> getBusinessesByUsername(String username) throws ServiceException {
        List<Business> businessList = this.businessRepository.findBusinessesByUsername(username);

        if(businessList == null || businessList.isEmpty()) {
            throw new ServiceException(1013, "business doesn’t exist");
        }
        return this.convertBusinessDaoListToBusinessDtoList(businessList);
    }

    @Override
    @PreAuthorize("hasAnyRole('ADMIN', 'BUSINESS', 'CUSTOMER')")
    public BusinessDto getBusinessById(Long id) throws ServiceException {
        if(id == null || id <= 0) {
            throw new ServiceException(1010, "invalid business id");
        }
        Business business = this.businessRepository.findOne(id);

        if(business == null) {
            throw new ServiceException(1013, "business doesn’t exist");
        }
        return this.convertBusinessToBusinessDto(business);
    }

    @Override
    public BusinessDto saveBusiness(BusinessDto businessDto) throws ServiceException  {
        if (businessDto == null || !this.isValidBusinessDtoToSave(businessDto)) {
            throw new ServiceException(1012, "invalid business object");
        }
        Business business = this.convertBusinessDtoToBusiness(businessDto);

        UserAuthority userAuthority = new UserAuthority();
        userAuthority.setAuthority(this.authorityRepository.findOne(1L));
        userAuthority.setBusiness(business);

        business.setUserAuthorities(new ArrayList<>(Collections.singletonList(userAuthority)));

        Business savedBusiness = this.businessRepository.save(business);
        return this.convertBusinessToBusinessDto(savedBusiness);
    }

    @Override
    @PreAuthorize("hasAnyRole('ADMIN', 'BUSINESS')")
    public void deleteBusinessById(Long id) throws ServiceException {
        if(id == null || id <= 0) {
            throw new ServiceException(1010, "invalid business id");
        }
        this.businessRepository.delete(id);
    }

    @Override
    @PreAuthorize("hasAnyRole('ADMIN', 'BUSINESS')")
    public BusinessDto updateBusiness(Long id, BusinessDto businessDto) throws ServiceException {
        if(id == null || id <= 0) {
            throw new ServiceException(1010, "invalid business id");
        }
        if (businessDto == null || !this.isValidBusinessDtoToUpdate(businessDto)) {
            throw new ServiceException(1012, "invalid business object");
        }
        if(!id.equals(businessDto.getId())) {
            throw new ServiceException(1011, "business ids doesn’t match");
        }
        Business storedBusiness = this.businessRepository.findOne(id);

        if(storedBusiness == null) {
            throw new ServiceException(1013, "business doesn’t exist");
        }
        Business business = this.convertBusinessDtoToBusiness(businessDto);

        business.setUserAuthorities(storedBusiness.getUserAuthorities());

        if(businessDto.getPassword().equals(storedBusiness.getPasswordHash())) {
            business.setPasswordHash(storedBusiness.getPasswordHash());
        }

        Business updatedBusiness = this.businessRepository.save(business);
        return this.convertBusinessToBusinessDto(updatedBusiness);
    }

    @Override
    public BusinessDto convertBusinessToBusinessDto(Business business) throws ServiceException {
        if(business == null) {
            throw new ServiceException(1012, "invalid business object");
        }
        BusinessDto businessDto = new BusinessDto();

        businessDto.setCity(business.getCity());
        businessDto.setEmail(business.getEmail());
        businessDto.setId(business.getId());
        businessDto.setName(business.getName());
        businessDto.setPassword(business.getPasswordHash());
        businessDto.setStreet(business.getStreet());
        businessDto.setStreetNumber(business.getStreetNumber());
        businessDto.setZipCode(business.getZipCode());
        businessDto.setUsername(business.getUsername());

        return businessDto;
    }

    @Override
    public Business convertBusinessDtoToBusiness(BusinessDto businessDto) throws ServiceException {
        if(businessDto == null) {
            throw new ServiceException(1012, "invalid business object");
        }
        Business business = new Business();

        if(businessDto.getPassword() != null && businessDto.getPassword().length() > 0) {
            business.setPasswordHash(this.bCryptPasswordEncoder.encode(businessDto.getPassword()));
        }

        business.setCity(businessDto.getCity());
        business.setEmail(businessDto.getEmail());
        business.setId(businessDto.getId());
        business.setName(businessDto.getName());
        business.setStreet(businessDto.getStreet());
        business.setStreetNumber(businessDto.getStreetNumber());
        business.setZipCode(businessDto.getZipCode());
        business.setUsername(businessDto.getUsername());

        return business;
    }

    @Override
    public boolean usernameAlreadyExists(Long businessId, String username) {
        List<Business> businessList = this.businessRepository.findBusinessesByUsername(username);
        return (businessList != null && businessList.size() != 0 && !businessList.get(0).getId().equals(businessId));
    }

    @Override
    public boolean emailAlreadyExists(Long businessId, String email) {
        List<Business> businessList = this.businessRepository.findBusinessesByEmail(email);
        return (businessList != null && businessList.size() != 0 && !businessList.get(0).getId().equals(businessId));
    }

    @Override
    public boolean isValidBusinessDtoToSave(BusinessDto businessDto) {
        return businessDto.getCity() != null && !businessDto.getCity().equals("") &&
                this.validationService.isValidEmail(businessDto.getEmail()) && this.validationService.isUniqueEmail(businessDto.getId(), businessDto.getEmail()) &&
                businessDto.getName() != null && !businessDto.getName().equals("") &&
                businessDto.getStreet() != null && !businessDto.getStreet().equals("") &&
                businessDto.getStreetNumber() != null && !businessDto.getStreetNumber().equals("") &&
                businessDto.getUsername() != null && !businessDto.getUsername().equals("") &&
                businessDto.getZipCode() > 0;
    }

    @Override
    public boolean isValidBusinessDtoToUpdate(BusinessDto businessDto) {
        return this.isValidBusinessDtoToSave(businessDto) && businessDto.getId() != null && businessDto.getId() > 0;
    }

    private List<BusinessDto> convertBusinessDaoListToBusinessDtoList(List<Business> businessList) throws ServiceException {
        List<BusinessDto> businessDtoList = new ArrayList<>();

        for (Business business : businessList) {
            businessDtoList.add(this.convertBusinessToBusinessDto(business));
        }

        return businessDtoList;
    }
}
