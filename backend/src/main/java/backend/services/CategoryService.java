package backend.services;

import backend.exceptions.ServiceException;
import backend.models.Category;
import backend.dtos.CategoryDto;

import java.util.List;

public interface CategoryService {

    List<CategoryDto> getAllCategories() throws ServiceException;

    CategoryDto getCategoryById(Long id) throws ServiceException;

    CategoryDto saveCategory(CategoryDto offerDto) throws ServiceException;

    void deleteCategoryById(Long id) throws ServiceException;

    CategoryDto updateCategory(Long id, CategoryDto offerDto) throws ServiceException;

    CategoryDto convertCategoryToCategoryDto(Category category) throws ServiceException;

    Category convertCategoryDtoToCategory(CategoryDto categoryDto) throws ServiceException;

    boolean isValidCategoryDtoToSave(CategoryDto categoryDto);

    boolean isValidCategoryDtoToUpdate(CategoryDto categoryDto);
}
