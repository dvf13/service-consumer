package backend.dtos;

import lombok.Data;

@Data
public class ResponseDto<T> {

    private boolean error;

    private T data;

    private ResponseDto() {}

    public ResponseDto(boolean error, T data) {
        this.error = error;
        this.data = data;
    }
}
