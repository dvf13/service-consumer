package backend.services.impl;

import backend.dtos.BusinessDto;
import backend.dtos.CustomerDto;
import backend.dtos.MessageDto;
import backend.exceptions.ServiceException;
import backend.services.BusinessService;
import backend.services.CustomerService;
import backend.services.RegistrationService;
import backend.services.ValidationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;

@Service
public class RegistrationServiceImpl implements RegistrationService {

    private CustomerService customerService;

    private BusinessService businessService;

    private ValidationService validationService;


    @Autowired
    public RegistrationServiceImpl(CustomerService customerService, BusinessService businessService, ValidationService validationService) {
        this.customerService = customerService;
        this.businessService = businessService;
        this.validationService = validationService;
    }


    @Override
    public MessageDto registerCustomer(CustomerDto customerDto) throws ServiceException {
        if (customerDto == null || customerDto.getUsername() == null || customerDto.getUsername().equals("") || customerDto.getEmail() == null || customerDto.getEmail().equals("")) {
            throw new ServiceException(1012, "invalid business object");
        }
        if(!this.validationService.isUniqueUsername(customerDto.getId(), customerDto.getUsername())) {
            throw new ServiceException(1060, "username already exists");
        }
        if(!this.validationService.isValidEmail(customerDto.getEmail())) {
            throw new ServiceException(1061, "invalid email");
        }
        if(!this.validationService.isUniqueEmail(customerDto.getId(), customerDto.getEmail())) {
            throw new ServiceException(1062, "email already exists");
        }
        this.customerService.saveCustomer(customerDto);

        return new MessageDto("Successfully registered");
    }

    @Override
    public MessageDto registerBusiness(BusinessDto businessDto) throws ServiceException {
        if (businessDto == null || businessDto.getUsername() == null || businessDto.getUsername().equals("") || businessDto.getEmail() == null || businessDto.getEmail().equals("")) {
            throw new ServiceException(1012, "invalid business object");
        }
        if(!this.validationService.isUniqueUsername(businessDto.getId(), businessDto.getUsername())) {
            throw new ServiceException(1060, "username already exists");
        }
        if(!this.validationService.isValidEmail(businessDto.getEmail())) {
            throw new ServiceException(1061, "invalid email");
        }
        if(!this.validationService.isUniqueEmail(businessDto.getId(), businessDto.getEmail())) {
            throw new ServiceException(1062, "email already exists");
        }
        this.businessService.saveBusiness(businessDto);

        return new MessageDto("Successfully registered");
    }
}
