package backend.services.impl;

import backend.services.BusinessService;
import backend.services.CustomerService;
import backend.services.ValidationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Service
public class ValidationServiceImpl implements ValidationService {

    private BusinessService businessService;

    private CustomerService customerService;

    private static final Pattern VALID_EMAIL_ADDRESS_REGEX = Pattern.compile("^[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?", Pattern.CASE_INSENSITIVE);


    @Autowired
    public ValidationServiceImpl(@Lazy BusinessService businessService, @Lazy CustomerService customerService) {
        this.businessService = businessService;
        this.customerService = customerService;
    }

    @Override
    public boolean isUniqueUsername(Long userId, String username) {
        return !this.businessService.usernameAlreadyExists(userId, username) && !this.customerService.usernameAlreadyExists(userId, username);
    }

    @Override
    public boolean isValidEmail(String email) {
        Matcher matcher = VALID_EMAIL_ADDRESS_REGEX.matcher(email);
        return matcher.find();
    }

    @Override
    public boolean isUniqueEmail(Long userId, String email) {
        return !this.businessService.emailAlreadyExists(userId, email) && !this.customerService.emailAlreadyExists(userId, email);
    }
}
