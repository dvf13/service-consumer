package backend.auth;

import java.util.ArrayList;

public class TokenStore {

    private static ArrayList<String> validToken = new ArrayList<>();


    private TokenStore() { }


    public static void addToken(String token) {
        if(!findToken(token)) {
            validToken.add(token);
        }
    }

    public static boolean removeToken(String token) {
        if(findToken(token)) {
            validToken.remove(token);
            return true;
        }
        return false;
    }

    public static boolean isTokenValid(String token) {
        return findToken(token);
    }

    private static boolean findToken(String token) {
        for(String tokenFromValidToken: validToken) {
            if(tokenFromValidToken.equals(token)) {
                return true;
            }
        }
        return false;
    }
}
