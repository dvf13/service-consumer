package backend.controller.impl;

import backend.auth.JwtAuthenticationRequest;
import backend.controller.AuthenticationController;
import backend.dtos.ErrorDto;
import backend.dtos.MessageDto;
import backend.dtos.ResponseDto;
import backend.dtos.UserInfoDto;
import backend.dtos.UserTokenStateDto;
import backend.exceptions.ServiceException;
import backend.services.AuthenticationService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mobile.device.Device;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.security.Principal;

@Slf4j
@RestController
@RequestMapping("/api/auth/")
public class AuthenticationControllerImpl implements AuthenticationController {

    private AuthenticationService authenticationService;


    @Autowired
    public AuthenticationControllerImpl(AuthenticationService authenticationService) {
        this.authenticationService = authenticationService;
    }

    @Override
    @ResponseBody
    @RequestMapping(value = "login/", method = RequestMethod.POST)
    public ResponseDto createAuthenticationToken(@RequestBody JwtAuthenticationRequest authenticationRequest, HttpServletResponse response, Device device) {
        try {
            UserTokenStateDto userTokenStateDto = this.authenticationService.createAuthenticationToken(authenticationRequest, device);
            return new ResponseDto<>(false, userTokenStateDto);
        } catch (ServiceException e) {
            log.error(e.getMessage());
            return new ResponseDto<>(true, new ErrorDto(e.getCode(), e.getMessage()));
        }
    }

    @Override
    @ResponseBody
    @PreAuthorize("hasAnyRole('ADMIN', 'BUSINESS', 'CUSTOMER')")
    @RequestMapping(value = "logout/", method = RequestMethod.POST)
    public ResponseDto logout(HttpServletRequest request) {
        try {
            MessageDto messageDto = this.authenticationService.logout(request);
            return new ResponseDto<>(false, messageDto);
        } catch (ServiceException e) {
            log.error(e.getMessage());
            return new ResponseDto<>(true, new ErrorDto(e.getCode(), e.getMessage()));
        }
    }


    @Override
    @ResponseBody
    @PreAuthorize("hasAnyRole('ADMIN', 'BUSINESS', 'CUSTOMER')")
    @RequestMapping(value = "refresh/", method = RequestMethod.POST)
    public ResponseDto refresh(HttpServletRequest request, Principal principal) {
        try {
            UserTokenStateDto userTokenStateDto = this.authenticationService.refresh(request, principal);
            return new ResponseDto<>(false, userTokenStateDto);
        } catch (ServiceException e) {
            log.error(e.getMessage());
            return new ResponseDto<>(true, new ErrorDto(e.getCode(), e.getMessage()));
        }
    }

    @Override
    @ResponseBody
    @PreAuthorize("hasAnyRole('ADMIN', 'BUSINESS', 'CUSTOMER')")
    @RequestMapping(value = "userinfo/", method = RequestMethod.GET)
    public ResponseDto getUserInfo(Principal user) {
        try {
            UserInfoDto userInfoDto = this.authenticationService.getUserInfo(user);
            return new ResponseDto<>(false, userInfoDto);
        } catch (ServiceException e) {
            log.error(e.getMessage());
            return new ResponseDto<>(true, new ErrorDto(e.getCode(), e.getMessage()));
        }
    }
}
