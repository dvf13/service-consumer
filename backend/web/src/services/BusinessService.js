import HttpService from './HttpService';
import config from '../config';
import UserService from './UserService';

export default {
  async getAllBusinesses() {
    return HttpService.get(`${config.apiUrl}business/`)
      .then((response) => {
        if (!response.data.error) {
          return Promise.resolve(response.data.data);
        }
        // eslint-disable-next-line no-console
        console.error(`Response error message: ${response.data.data.message}`);
        return Promise.reject(response.data.data.message);
      }, error => Promise.reject(error.statusText))
      .catch(e => Promise.reject(e));
  },
  async getOfferByBusinessId(businessId) {
    return HttpService.get(`${config.apiUrl}offer/business/${businessId}`)
      .then((response) => {
        const responseDto = response.data;

        if (responseDto.error) {
          return Promise.reject(responseDto.data.message);
        }
        return Promise.resolve(responseDto.data);
      }, error => Promise.reject(error.statusText))
      .catch(e => Promise.reject(e));
  },
  async getOfferByBusinessIdFromTo(businessId, from, to) {
    return HttpService.get(`${config.apiUrl}offer/business/${businessId}/${from}/${to}`)
      .then((response) => {
        const responseDto = response.data;

        if (responseDto.error) {
          return Promise.reject(responseDto.data.message);
        }
        return Promise.resolve(responseDto.data);
      }, error => Promise.reject(error.statusText))
      .catch(e => Promise.reject(e));
  },
  async getBusiness(id) {
    return HttpService.get(`${config.apiUrl}business/${id}`)
      .then((response) => {
        const responseDto = response.data;

        if (responseDto.error) {
          return Promise.reject(responseDto.data.message);
        }
        return Promise.resolve(responseDto.data);
      }, error => Promise.reject(error.statusText))
      .catch(e => Promise.reject(e));
  },
  async updateBusiness(business) {
    if (!UserService.isAuthenticated()) {
      return Promise.reject(new Error('Not logged in!'));
    }
    return HttpService.put(`${config.apiUrl}business/${business.id}`, business)
      .then((response) => {
        const responseDto = response.data;

        if (responseDto.error) {
          return Promise.reject(responseDto.data.message);
        }
        return Promise.resolve(responseDto.data);
      }, error => Promise.reject(error.statusText))
      .catch(e => Promise.reject(e));
  },
};

