# noinspection SqlNoDataSourceInspection
SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `business`;
CREATE TABLE `business` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `password_hash` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `street` varchar(255) NOT NULL,
  `street_number` varchar(255) NOT NULL,
  `zip_code` int(5) NOT NULL,
  `city` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `category`;
CREATE TABLE `category` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `customer`;
CREATE TABLE `customer` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `password_hash` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `birthdate` datetime NOT NULL,
  `gender` char(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `offer`;
CREATE TABLE `offer` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `business_id` bigint(20) NOT NULL,
  `category_id` bigint(20) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` varchar(2048) NOT NULL,
  `picture_url` varchar(1024) NOT NULL,
  `picture` MEDIUMBLOB,
  `latitude` double,
  `longitude` double,
  `price` int(11) NOT NULL,
  `discount` double NOT NULL,
  `amount` int(5) NOT NULL,
  `valid_until` datetime NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `business_id` (`business_id`),
  KEY `category_id` (`category_id`),
  CONSTRAINT `offer_ibfk_1` FOREIGN KEY (`business_id`) REFERENCES `business` (`id`),
  CONSTRAINT `offer_ibfk_2` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `reservation`;
CREATE TABLE `reservation` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `reserved_at` datetime NOT NULL,
  `state` int(11) DEFAULT NULL,
  `customer_id` bigint(20) NOT NULL,
  `timeslot_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK41v6ueo0hiran65w8y1cta2c2` (`customer_id`),
  KEY `FKbq8ayr5v3o8wbx483seomo9wq` (`timeslot_id`),
  CONSTRAINT `FK41v6ueo0hiran65w8y1cta2c2` FOREIGN KEY (`customer_id`) REFERENCES `customer` (`id`),
  CONSTRAINT `FKbq8ayr5v3o8wbx483seomo9wq` FOREIGN KEY (`timeslot_id`) REFERENCES `timeslot` (`id`),
  CONSTRAINT `reservation_ibfk_1` FOREIGN KEY (`timeslot_id`) REFERENCES `timeslot` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `timeslot`;
CREATE TABLE `timeslot` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `beg_date_time` datetime NOT NULL,
  `end_date_time` datetime NOT NULL,
  `quantity_booked` int(11) NOT NULL,
  `quantity_total` int(11) NOT NULL,
  `offer_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKnmqjd1lwn9yhxtmcc9ardyn9f` (`offer_id`),
  CONSTRAINT `FKnmqjd1lwn9yhxtmcc9ardyn9f` FOREIGN KEY (`offer_id`) REFERENCES `offer` (`id`),
  CONSTRAINT `timeslot_ibfk_1` FOREIGN KEY (`offer_id`) REFERENCES `offer` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `authority`;
CREATE TABLE `authority` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `user_authority`;
CREATE TABLE `user_authority` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `authority_id` bigint(20) NOT NULL,
  `business_id` bigint(20) NULL,
  `customer_id` bigint(20) NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `user_authority_ibfk_1` FOREIGN KEY (`authority_id`) REFERENCES `authority` (`id`),
  CONSTRAINT `user_authority_ibfk_2` FOREIGN KEY (`business_id`) REFERENCES `business` (`id`),
  CONSTRAINT `user_authority_ibfk_3` FOREIGN KEY (`customer_id`) REFERENCES `customer` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;