import Vue from 'vue';
import axios from 'axios';
import CONFIG from '../config';

export default {
  getSimulationData(){
    if(Vue.localStorage.get('ASE_SIM_DATA') && Vue.localStorage.get('ASE_SIM_DATA').length > 5) return JSON.parse(Vue.localStorage.get('ASE_SIM_DATA'));
    return this.getDefaultSimulationData();
  },

  setSimulationData(simData){
    Vue.localStorage.set('ASE_SIM_DATA', JSON.stringify(simData));
  },

  getDefaultSimulationData(){
    return {
      user: {
        loginEndpoint: '/auth/login/',
        requestProcessing: 'concurrently'
      },
      getOffer: {
        getOfferEndpoint: '/offer/',
        requestProcessing: 'concurrently',
        userCount: 1,
      },
      createOffer: {
        createOfferEndpoint: '/offer/',
        requestProcessing: 'concurrently',
        businessCount: 1,
      }
      // TODO: additional sim config data here
    };
  },

  resetSimulationData(){
    console.log('###');
    console.log(this.getDefaultSimulationData());
    this.setSimulationData(this.getDefaultSimulationData());
    return this.getDefaultSimulationData();
  },

  async loginUsers(loginEndpoint, requestProcessing){
    return axios.post(`http://localhost:${CONFIG.backend_port}${CONFIG.api_endpoint}/simulation/login/`, {loginEndpoint, requestProcessing}, {headers: {'Content-Type': 'application/json'}})
      .then(response => {
        if (!!response.data.success) {
          return Promise.resolve(response.data.message);
        } else {
          return Promise.reject(response.data.message);
        }
      });
  },

  async simulateGetOffers(getOfferEndpoint, userCount, requestProcessing){
    return axios.post(`http://localhost:${CONFIG.backend_port}${CONFIG.api_endpoint}/simulation/getOffers/`, {getOfferEndpoint, userCount, requestProcessing}, {headers: {'Content-Type': 'application/json'}})
      .then(response => {
        if (!!response.data.success) {
          return Promise.resolve(response.data.message);
        } else {
          return Promise.reject(response.data.message);
        }
      });
  },

  async simulateCreateOffers(createOfferEndpoint, businessCount, requestProcessing){
    return axios.post(`http://localhost:${CONFIG.backend_port}${CONFIG.api_endpoint}/simulation/createOffers/`, {createOfferEndpoint, businessCount, requestProcessing}, {headers: {'Content-Type': 'application/json'}})
      .then(response => {
        if (!!response.data.success) {
          return Promise.resolve(response.data.message);
        } else {
          return Promise.reject(response.data.message);
        }
      });
  },
}