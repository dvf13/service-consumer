package backend.controller;

import backend.dtos.CustomerDto;
import backend.dtos.ResponseDto;

import java.util.List;

public interface CustomerController {

    ResponseDto getAllCustomers();

    ResponseDto getCustomerById(Long id);

    ResponseDto saveCustomer(CustomerDto customerDto);

    void deleteCustomerById(Long id);

    ResponseDto updateCustomer(Long id, CustomerDto customerDto);
}
