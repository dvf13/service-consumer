package backend.services;

import backend.dtos.BusinessDto;
import backend.dtos.CustomerDto;
import backend.models.Business;
import backend.models.Customer;
import backend.repositories.BusinessRepository;
import backend.repositories.CustomerRepository;
import backend.services.impl.ValidationServiceImpl;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.*;
import static org.junit.Assert.*;

public class ValidationServiceImplTest {

    private CustomerRepository customerRepository;
    private BusinessRepository businessRepository;
    private ValidationService validationService;

    @Mock
    private BusinessService businessService;

    @Mock
    private CustomerService customerService;

    public ValidationServiceImplTest(){
        this.customerRepository = mock(CustomerRepository.class);
        this.businessRepository = mock(BusinessRepository.class);
        this.validationService = new ValidationServiceImpl(businessService, customerService);
    }

    // TODO: Refactor all tests
    //@Test
    public void isValidUsernameForRegistationTest_UsernameAlreadyExistsInBothRepositories(){
        when(this.customerRepository.findCustomersByUsername(Mockito.anyString())).thenReturn(getAllCustomers());
        when(this.businessRepository.findBusinessesByUsername(Mockito.anyString())).thenReturn(getBusinessList());

        assertFalse(this.validationService.isUniqueUsername(1L,"Test_Username"));
    }

    //@Test
    public void isValidUsernameForRegistationTest_UsernameAlreadyExistsInOneRepositories(){
        when(this.customerRepository.findCustomersByUsername(Mockito.anyString())).thenReturn(null);
        when(this.businessRepository.findBusinessesByUsername(Mockito.anyString())).thenReturn(getBusinessList());

        assertFalse(this.validationService.isUniqueUsername(1L,"Test_Username"));
    }

    //@Test
    public void isValidUsernameForRegistationTest_ValidCase_UsernameDoesNotExist(){
        when(this.customerRepository.findCustomersByUsername(Mockito.anyString())).thenReturn(null);
        when(this.businessRepository.findBusinessesByUsername(Mockito.anyString())).thenReturn(null);

        assertTrue(this.validationService.isUniqueUsername(1L,"Test_Username"));
    }

    //@Test
    public void isValidEmailForRegistationTest_ValidCase_EmailDoesNotExist(){
        when(this.customerRepository.findCustomersByEmail(Mockito.anyString())).thenReturn(null);
        when(this.businessRepository.findBusinessesByEmail(Mockito.anyString())).thenReturn(null);

        assertTrue(this.validationService.isUniqueEmail(1L,"test@mail.com"));
    }

    //@Test
    public void isValidEmailForRegistationTest_RmsilAlreadyExistsInBothRepositories(){
        when(this.customerRepository.findCustomersByEmail(Mockito.anyString())).thenReturn(getAllCustomers());
        when(this.businessRepository.findBusinessesByEmail(Mockito.anyString())).thenReturn(getBusinessList());

        assertFalse(this.validationService.isUniqueEmail(1L,"test@mail.com"));
    }

    //@Test
    public void isValidEmailForRegistationTest_RmsilAlreadyExistsInOneRepositories(){
        when(this.customerRepository.findCustomersByEmail(Mockito.anyString())).thenReturn(getAllCustomers());
        when(this.businessRepository.findBusinessesByEmail(Mockito.anyString())).thenReturn(null);

        assertFalse(this.validationService.isUniqueEmail(1L,"test@mail.com"));
    }

    @Test
    public void isValidEmailTest_InvalidEmail(){
        assertFalse(this.validationService.isValidEmail("test$mail.com"));
    }

    @Test
    public void isValidEmailTest_ValidEmail(){
        assertTrue(this.validationService.isValidEmail("test@mail.com"));
    }

    private Business getBusiness() {
        Business business = new Business();
        business.setId(1L);
        business.setUsername("Test_Username");
        business.setEmail("test@mail.com");
        return business;
    }

    private List<Business> getBusinessList() {
        List<Business> businessArrayList = new ArrayList<>();
        businessArrayList.add(getBusiness());
        return businessArrayList;
    }

    private BusinessDto getBusinessDto() {
        BusinessDto businessDto = new BusinessDto();
        businessDto.setId(1L);
        businessDto.setUsername("Test_Username");
        businessDto.setEmail("test@mail.com");
        return businessDto;
    }

    private List<BusinessDto> getBusinessDtoList() {
        List<BusinessDto> businessDtoArrayList = new ArrayList<>();
        businessDtoArrayList.add(getBusinessDto());
        return businessDtoArrayList;
    }

    private Customer getCustomer(){
        Customer customer = new Customer();
        customer.setId(1L);
        customer.setUsername("Test_Username");
        customer.setEmail("test@mail.com");
        return customer;
    }

    private List<Customer> getAllCustomers(){
        List<Customer> customerArrayList = new ArrayList<>();
        customerArrayList.add(getCustomer());
        return customerArrayList;
    }

    private CustomerDto getCustomerDto(){
        CustomerDto customerDto = new CustomerDto();
        customerDto.setId(1L);
        customerDto.setUsername("Test_Username");
        customerDto.setEmail("test@mail.com");
        return customerDto;
    }

    private List<CustomerDto> getAllCustomerDto(){
        List<CustomerDto> customerArrayList = new ArrayList<>();
        customerArrayList.add(getCustomerDto());
        return customerArrayList;
    }

}
