package backend.repositories;

import backend.models.Business;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Business repository for jpa
 */
@Repository
public interface BusinessRepository extends JpaRepository<Business, Long> {

    List<Business> findBusinessesByUsername(String username);

    List<Business> findBusinessesByEmail(String email);
}