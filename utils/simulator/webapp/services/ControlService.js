import Vue from 'vue';
import axios from 'axios';
import CONFIG from '../config';

let config = {apiEndpoint: `${CONFIG.apiEndpoint}/`, concurrentUsers: CONFIG.concurrentUsers, concurrentBusinesses: CONFIG.concurrentBusinesses, registerEndpointUser: CONFIG.registerEndpointUser, registerEndpointBusiness: CONFIG.registerEndpointBusiness, requestProcessing: 'concurrently'};

export default {
  async getConfig(){
    return axios.get(`http://localhost:${CONFIG.backend_port}${CONFIG.api_endpoint}/control/`, {headers: {'Content-Type': 'application/json'}})
      .then(response => {
        if (!!response.data.success) {
          config = response.data.config;
          return Promise.resolve(config);
        } else {
          return Promise.reject(response.data.message);
        }
      });
  },

  async getUserCount(){
    if(config && config.concurrentUsers) return Promise.resolve(config.concurrentUsers);
    Promise.resolve(1);
  },

  async getBusinessUserCount(){
    if(config && config.concurrentBusinesses) return Promise.resolve(config.concurrentBusinesses);
    Promise.resolve(1);
  },

  async getRegisterEndpointUser(){
    if(config && config.registerEndpointUser) return Promise.resolve(config.apiEndpoint + config.registerEndpointUser);
    Promise.resolve('');
  },


  async getRegisterEndpointBusiness(){
    if(config && config.registerEndpointBusiness) return Promise.resolve(config.apiEndpoint + config.registerEndpointBusiness);
    Promise.resolve('');
  },


  async getDefaultConfig(){
    return axios.get(`http://localhost:${CONFIG.backend_port}${CONFIG.api_endpoint}/control/default/`, {headers: {'Content-Type': 'application/json'}})
      .then(response => {
        if (!!response.data.success) {
          config = response.data.config;
          return Promise.resolve(config);
        } else {
          return Promise.reject(response.data.message);
        }
      });
  },

  async saveConfig(apiEndpoint, concurrentUsers, concurrentBusinesses, registerEndpointUser, registerEndpointBusiness, requestProcessing){
    return axios.post(`http://localhost:${CONFIG.backend_port}${CONFIG.api_endpoint}/control/`, {apiEndpoint, concurrentUsers, concurrentBusinesses, registerEndpointUser, registerEndpointBusiness, requestProcessing}, {headers: {'Content-Type': 'application/json'}})
      .then(response => {
        if (!!response.data.success) {
          config = {
            apiEndpoint: apiEndpoint,
            concurrentUsers: concurrentUsers,
            concurrentBusinesses: concurrentBusinesses,
            registerEndpointUser: registerEndpointUser,
            registerEndpointBusiness: registerEndpointBusiness,
            requestProcessing: requestProcessing
          };
          return Promise.resolve(response.data.message);
        } else {
          return Promise.reject(response.data.message);
        }
      });
  },

  async start(){
    Vue.localStorage.set('ASE_SIM_STARTED', 'true');
    return axios.post(`http://localhost:${CONFIG.backend_port}${CONFIG.api_endpoint}/control/start/`, {}, {headers: {'Content-Type': 'application/json'}})
      .then(response => {
        if (!!response.data.success) {
          Vue.localStorage.set('ASE_SIM_RUNNING', 'true');
          return Promise.resolve(response.data.message);
        } else {
          return Promise.reject(response.data.message);
        }
      });
  },

  async reset(){
    return axios.post(`http://localhost:${CONFIG.backend_port}${CONFIG.api_endpoint}/control/reset/`, {}, {headers: {'Content-Type': 'application/json'}})
      .then(response => {
        if (!!response.data.success) {
          Vue.localStorage.set('ASE_SIM_STARTED', 'false');
          Vue.localStorage.set('ASE_SIM_RUNNING', 'false');
          return Promise.resolve(response.data.message);
        } else {
          return Promise.reject(response.data.message);
        }
      });
  },

  async isStarted(){
    if(Vue.localStorage.get('ASE_SIM_STARTED') === 'true') return Promise.resolve(true);
    return Promise.resolve(false);
  },

  async isStarting(){
    return axios.get(`http://localhost:${CONFIG.backend_port}${CONFIG.api_endpoint}/control/starting/`, {headers: {'Content-Type': 'application/json'}})
      .then(response => {
        if (!!response.data.success) {
          if(response.data.starting){
            Vue.localStorage.set('ASE_SIM_STARTED', 'true');
          } else{
            Vue.localStorage.set('ASE_SIM_STARTED', 'false');
          }
          return Promise.resolve(response.data.starting);
        } else {
          return Promise.reject(response.data.message);
        }
      });
  },

  async isRunning(){
    return axios.get(`http://localhost:${CONFIG.backend_port}${CONFIG.api_endpoint}/control/running/`, {headers: {'Content-Type': 'application/json'}})
      .then(response => {
        if (!!response.data.success) {
          if(response.data.running){
            Vue.localStorage.set('ASE_SIM_RUNNING', 'true');
          } else{
            Vue.localStorage.set('ASE_SIM_RUNNING', 'false');
          }
          return Promise.resolve(response.data.running);
        } else {
          return Promise.reject(response.data.message);
        }
      });
  },


  async getLoggedInCount(){
    return axios.get(`http://localhost:${CONFIG.backend_port}${CONFIG.api_endpoint}/control/loggedInCount/`, {headers: {'Content-Type': 'application/json'}})
      .then(response => {
        if (!!response.data.success) {
          if(response.data.loggedInCount){
            return Promise.resolve(response.data.loggedInCount);
          } else{
            return Promise.reject('No count given.');
          }
        } else {
          return Promise.reject(response.data.message);
        }
      });
  },


  async getLoggedInCustomersCount(){
    return axios.get(`http://localhost:${CONFIG.backend_port}${CONFIG.api_endpoint}/control/loggedInCount/customers/`, {headers: {'Content-Type': 'application/json'}})
      .then(response => {
        if (!!response.data.success) {
          if(response.data.loggedInCount){
            return Promise.resolve(response.data.loggedInCount);
          } else{
            return Promise.reject('No count given.');
          }
        } else {
          return Promise.reject(response.data.message);
        }
      });
  },


  async getLoggedInBusinessesCount(){
    return axios.get(`http://localhost:${CONFIG.backend_port}${CONFIG.api_endpoint}/control/loggedInCount/businesses/`, {headers: {'Content-Type': 'application/json'}})
      .then(response => {
        if (!!response.data.success) {
          if(response.data.loggedInCount){
            return Promise.resolve(response.data.loggedInCount);
          } else{
            return Promise.reject('No count given.');
          }
        } else {
          return Promise.reject(response.data.message);
        }
      });
  },
}