package backend.services.impl;

import backend.dtos.MessageDto;
import backend.exceptions.ServiceException;
import backend.services.AdvancedFeaturesService;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.search.SearchType;
import org.elasticsearch.client.Client;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.transport.InetSocketTransportAddress;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.suggest.SuggestBuilders;
import org.elasticsearch.search.suggest.term.TermSuggestionBuilder;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

@Service
public class AdvancedFeaturesServiceImpl implements AdvancedFeaturesService {

    @Value("${elk.host}")
    private String ELK_HOST;

    @Value("${elk.port}")
    private int ELK_PORT;


    @Override
    public List<MessageDto> correct(String query) throws ServiceException {
        if(query == null || query.equals("") || query.matches(".*[^a-zA-Z0-9\\s\\-'].*")) {
            throw new ServiceException(1081, "Could not correct query");
        }

        String[] terms = query.split("\\s|\\-");

        Client client;
        try {
            client = TransportClient.builder().build().addTransportAddress(new InetSocketTransportAddress(InetAddress.getByName(this.ELK_HOST), this.ELK_PORT));
        } catch (UnknownHostException e) {
            throw new ServiceException(1080, "ELK server not available");
        }

        TermSuggestionBuilder csb = SuggestBuilders.termSuggestion("mysug").text(query).field("name");

        SearchResponse rs = client.prepareSearch("ase")
                .setSize(0)
                .addSuggestion(csb)
                .setQuery(QueryBuilders.matchAllQuery())
                .execute()
                .actionGet();
        JSONObject response = new JSONObject(rs.toString());

        client.close();

        JSONArray suggestions = response.getJSONObject("suggest").getJSONArray("mysug");

        List<MessageDto> result = new ArrayList<>();

        for (int i = 0; i < suggestions.length(); i++) {
            JSONArray options = suggestions.getJSONObject(i).getJSONArray("options");

            if (options.length() == 0) {
                result.add(new MessageDto(terms[i]));
            } else {
                result.add(new MessageDto(options.getJSONObject(0).getString("text")));
            }
        }
        return result;
    }

    @Override
    public List<MessageDto> autocomplete(String query) throws ServiceException {
        if(query == null || query.equals("")) {
            throw new ServiceException(1081, "Could not correct query");
        }

        Client client;
        try {
            client = TransportClient.builder().build().addTransportAddress(new InetSocketTransportAddress(InetAddress.getByName(this.ELK_HOST), this.ELK_PORT));
        } catch (UnknownHostException e) {
            throw new ServiceException(1080, "ELK server not available");
        }

        SearchResponse rs = client.prepareSearch("ase")
                .setSearchType(SearchType.COUNT)
                .setQuery(QueryBuilders.matchAllQuery())
                .addAggregation(AggregationBuilders.terms("keyword").field("name.raw").size(10).include(".*" + query + ".*"))
                .setSize(10)
                .execute()
                .actionGet();

        JSONObject response = new JSONObject(rs.toString());

        client.close();

        JSONArray buckets = response.getJSONObject("aggregations").getJSONObject("keyword").getJSONArray("buckets");

        List<MessageDto> result = new ArrayList<>();

        for (int i = 0; i < buckets.length(); i++) {
            result.add(new MessageDto(buckets.getJSONObject(i).getString("key")));
        }

        return result;
    }

    @Override
    public List<MessageDto> search(String query) throws ServiceException {
        if(query == null || query.equals("")) {
            throw new ServiceException(1081, "Could not correct query");
        }

        Client client;
        try {
            client = TransportClient.builder().build().addTransportAddress(new InetSocketTransportAddress(InetAddress.getByName(this.ELK_HOST), this.ELK_PORT));
        } catch (UnknownHostException e) {
            throw new ServiceException(1080, "ELK server not available");
        }

        SearchResponse rs = client.prepareSearch("ase")
                .setSearchType(SearchType.COUNT)
                .setQuery(QueryBuilders.termQuery("name", query))
                .addAggregation(AggregationBuilders.terms("name").field("name.raw"))
                .setSize(10)
                .execute()
                .actionGet();

        JSONObject response = new JSONObject(rs.toString());

        client.close();

        JSONArray buckets = response.getJSONObject("aggregations").getJSONObject("name").getJSONArray("buckets");

        List<MessageDto> result = new ArrayList<>();

        for (int i = 0; i < buckets.length(); i++) {
            result.add(new MessageDto(buckets.getJSONObject(i).getString("key")));
        }

        return result;
    }

    @Override
    public List<MessageDto> recommendHistory(String uid) throws ServiceException {
        if(uid == null || uid.equals("")) {
            throw new ServiceException(1081, "Could not correct query");
        }

        Client client;
        try {
            client = TransportClient.builder().build().addTransportAddress(new InetSocketTransportAddress(InetAddress.getByName(this.ELK_HOST), this.ELK_PORT));
        } catch (UnknownHostException e) {
            throw new ServiceException(1080, "ELK server not available");
        }

        SearchResponse rs = client.prepareSearch("ase")
                .setSearchType(SearchType.COUNT)
                .setQuery(QueryBuilders.termQuery("uid", uid))
                .addAggregation(AggregationBuilders.terms("keyword").field("prov_type").size(0)
                        .subAggregation(AggregationBuilders.terms("names").field("pid").size(0)))
                .execute()
                .actionGet();

        client.close();

        JSONObject response = new JSONObject(rs.toString());

        StringBuilder provType = new StringBuilder();
        StringBuilder exclude = new StringBuilder();

        JSONArray buckets = response.getJSONObject("aggregations").getJSONObject("keyword").getJSONArray("buckets");

        for (int i = 0; i < buckets.length(); i++) {
            provType.append("prov_type:\"").append(buckets.getJSONObject(i).getString("key")).append("\" ");

            JSONArray buckets2 = buckets.getJSONObject(i).getJSONObject("names").getJSONArray("buckets");

            for (int j = 0; j < buckets2.length(); j++) {
                exclude.append(buckets2.getJSONObject(j).getString("key")).append("|");
            }
        }

        provType = new StringBuilder(provType.toString().trim().replaceAll(" ", " OR "));
        exclude = new StringBuilder(exclude.substring(0, exclude.length() - 1));

        try {
            client = TransportClient.builder().build().addTransportAddress(new InetSocketTransportAddress(InetAddress.getByName(this.ELK_HOST), this.ELK_PORT));
        } catch (UnknownHostException e) {
            throw new ServiceException(1080, "ELK server not available");
        }

        rs = client.prepareSearch("ase")
                .setSearchType(SearchType.COUNT)
                .setQuery(QueryBuilders.simpleQueryStringQuery(provType.toString()))
                .addAggregation(AggregationBuilders.terms("keyword").field("pid").size(10).exclude(exclude.toString()))
                .execute()
                .actionGet();

        client.close();

        response = new JSONObject(rs.toString());

        buckets = response.getJSONObject("aggregations").getJSONObject("keyword").getJSONArray("buckets");

        List<MessageDto> result = new ArrayList<>();

        for (int i = 0; i < buckets.length(); i++) {
            result.add(new MessageDto(buckets.getJSONObject(i).getString("key")));
        }

        return result;
    }

    @Override
    public List<MessageDto> alsoBooked(String pid) throws ServiceException {
        if(pid == null || pid.equals("")) {
            throw new ServiceException(1081, "Could not correct query");
        }

        Client client;
        try {
            client = TransportClient.builder().build().addTransportAddress(new InetSocketTransportAddress(InetAddress.getByName(this.ELK_HOST), this.ELK_PORT));
        } catch (UnknownHostException e) {
            throw new ServiceException(1080, "ELK server not available");
        }

        SearchResponse rs = client.prepareSearch("ase")
                .setSearchType(SearchType.COUNT)
                .setQuery(QueryBuilders.termQuery("pid", pid))
                .addAggregation(AggregationBuilders.terms("name").field("uid").size(10))
                .execute()
                .actionGet();

        client.close();

        JSONObject response = new JSONObject(rs.toString());

        StringBuilder uids = new StringBuilder();

        JSONArray buckets = response.getJSONObject("aggregations").getJSONObject("name").getJSONArray("buckets");

        for (int i = 0; i < buckets.length(); i++) {
            uids.append("uid:\"").append(buckets.getJSONObject(i).getString("key")).append("\" ");
        }

        uids = new StringBuilder(uids.toString().trim().replaceAll(" ", " OR "));

        try {
            client = TransportClient.builder().build().addTransportAddress(new InetSocketTransportAddress(InetAddress.getByName(this.ELK_HOST), this.ELK_PORT));
        } catch (UnknownHostException e) {
            throw new ServiceException(1080, "ELK server not available");
        }

        rs = client.prepareSearch("ase")
                .setSearchType(SearchType.COUNT)
                .setQuery(QueryBuilders.simpleQueryStringQuery(uids.toString()))
                .addAggregation(AggregationBuilders.terms("keyword").field("pid").size(10).exclude(pid))
                .execute()
                .actionGet();

        client.close();

        response = new JSONObject(rs.toString());

        buckets = response.getJSONObject("aggregations").getJSONObject("keyword").getJSONArray("buckets");

        List<MessageDto> result = new ArrayList<>();

        for (int i = 0; i < buckets.length(); i++) {
            result.add(new MessageDto(buckets.getJSONObject(i).getString("key")));
        }

        return result;
    }
}
