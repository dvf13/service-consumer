package backend.services.impl;

import backend.dtos.CustomerDto;
import backend.exceptions.ServiceException;
import backend.models.*;
import backend.dtos.ReservationDto;
import backend.repositories.CustomerRepository;
import backend.repositories.OfferRepository;
import backend.repositories.ReservationRepository;
import backend.repositories.TimeSlotRepository;
import backend.services.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;

import java.sql.Connection;
import java.sql.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;

@Service
public class ReservationServiceImpl implements ReservationService {

    private ReservationRepository reservationRepository;

    private CustomerRepository customerRepository;

    private OfferRepository offerRepository;

    private CustomerService customerService;

    private OfferService offerService;

    private TimeSlotService timeSlotService;

    private TimeSlotRepository timeSlotRepository;

    private LoggingService loggingService;


    @Autowired
    public ReservationServiceImpl(ReservationRepository reservationRepository, CustomerRepository customerRepository, TimeSlotRepository timeSlotRepository, OfferRepository offerRepository, CustomerService customerService, OfferService offerService, TimeSlotService timeSlotService, LoggingService loggingService) {
        this.reservationRepository = reservationRepository;
        this.customerRepository = customerRepository;
        this.timeSlotRepository = timeSlotRepository;
        this.offerRepository = offerRepository;
        this.customerService = customerService;
        this.offerService = offerService;
        this.timeSlotService = timeSlotService;
        this.loggingService = loggingService;
    }


    @Override
    @PreAuthorize("hasAnyRole('ADMIN')")
    public List<ReservationDto> findReservationsByState(Integer state) throws ServiceException {
        List<Reservation> reservationList = this.reservationRepository.findReservationsByState(state);

        if (reservationList == null || reservationList.isEmpty()) {
            throw new ServiceException(1053, "reservation doesn’t exist");
        }
        return this.convertReservationDaoListToReservationDtoList(reservationList);
    }

    @Override
    @PreAuthorize("hasAnyRole('ADMIN', 'BUSINESS', 'CUSTOMER')")
    public List<ReservationDto> getAllReservations() throws ServiceException {
        List<Reservation> reservationList = this.reservationRepository.findAll();

        if (reservationList == null || reservationList.isEmpty()) {
            throw new ServiceException(1053, "reservation doesn’t exist");
        }
        return this.convertReservationDaoListToReservationDtoList(reservationList);
    }

    @Override
    @PreAuthorize("hasAnyRole('ADMIN', 'CUSTOMER')")
    public List<ReservationDto> findReservationsByCustomerId(Long customerId, Integer state) throws ServiceException {
        if (customerId == null || customerId <= 0) {
            throw new ServiceException(1050, "invalid reservation ids");
        }

        List<Reservation> reservationList = this.reservationRepository.findReservationsByCustomerId(customerId, state);

        return this.convertReservationDaoListToReservationDtoList(reservationList);
    }

    @Override
    @PreAuthorize("hasAnyRole('ADMIN', 'BUSINESS', 'CUSTOMER')")
    public ReservationDto getReservationById(Long reservationID) throws ServiceException {
        if(reservationID == null || reservationID <= 0) {
            throw new ServiceException(1050, "invalid reservation ids");
        }

        Reservation reservation = this.reservationRepository.findOne(reservationID);

        if (reservation == null) {
            throw new ServiceException(1053, "reservation doesn’t exist");
        }
        return this.convertReservationToReservationDto(reservation);
    }

    @Override
    @PreAuthorize("hasAnyRole('ADMIN', 'BUSINESS', 'CUSTOMER')")
    public List<ReservationDto> getReservationsByTimeSlotId(Long timeSlotId) throws ServiceException {
        if(timeSlotId == null || timeSlotId <= 0) {
            throw new ServiceException(1050, "invalid reservation ids");
        }

        List<Reservation> reservationList = this.reservationRepository.findReservationsByTimeSlotId(timeSlotId);

        return this.convertReservationDaoListToReservationDtoList(reservationList);
    }

    @Override
    @PreAuthorize("hasAnyRole('ADMIN', 'CUSTOMER')")
    public ReservationDto saveReservation(ReservationDto reservationDto) throws ServiceException {
        reservationDto.setReservedAt(new Date());
        if (reservationDto == null || !this.isValidReservationDtoToSave(reservationDto)) {
            throw new ServiceException(1052, "invalid reservation object");
        }
        //System.out.println(reservationDto.toString());
        if(reservationDto.getState() == 0) {
            List<Reservation> list = this.reservationRepository.findReservationsByCustomerIdAndOfferIdAndState(reservationDto.getCustomerDto().getId(), reservationDto.getTimeSlotDto().getOfferDto().getId(), 0);
            if (list.size() > 0) {
                return this.convertReservationToReservationDto(list.get(0));
            } else {
                Reservation savedReservation = this.reservationRepository.save(this.convertReservationDtoToReservation(reservationDto));
                this.loggingService.logToElasticsearch(savedReservation);
                return this.convertReservationToReservationDto(savedReservation);
            }
        } else{
            Reservation savedReservation = this.reservationRepository.save(this.convertReservationDtoToReservation(reservationDto));
            this.loggingService.logToElasticsearch(savedReservation);
            return this.convertReservationToReservationDto(savedReservation);
        }
//        this.timeSlotService.saveTimeSlot(reservationDto.getTimeSlotDto());

        //TimeSlot timeSlotUpdate = savedReservation.getReservationIdentity().getTimeSlot();
        //int quantity_booked = timeSlotUpdate.getQuantity_booked();
        //quantity_booked++;
        //timeSlotUpdate.setQuantity_booked(quantity_booked);
        //this.timeSlotService.updateTimeSlot(timeSlotId, this.timeSlotService.convertTimeSlotToTimeSlotDto(timeSlotUpdate));
        //return this.convertReservationToReservationDto(savedReservation);
    }

    @Override
    @PreAuthorize("hasAnyRole('ADMIN', 'CUSTOMER')")
    public void deleteReservationById(Long id) throws ServiceException {
        if(id == null || id <= 0) {
            throw new ServiceException(1050, "invalid reservation ids");
        }

       this.reservationRepository.delete(id);
    }

    @Override
    @PreAuthorize("hasAnyRole('ADMIN', 'CUSTOMER')")
    public ReservationDto updateReservation(ReservationDto reservationDto) throws ServiceException {
        if (reservationDto == null || !this.isValidReservationDtoToUpdate(reservationDto)) {
            throw new ServiceException(1052, "invalid reservation object");
        }

        //if(this.reservationRepository.findOne(new ReservationIdentity(reservedAt, this.customerService.convertCustomerDtoToCustomer(this.customerService.getCustomerById(reservationDto.getCustomerDto().getId())), this.timeSlotService.convertTimeSlotDtoToTimeSlot(this.timeSlotService.getTimeSlotById(timeSlotId)))) == null) {
        //    throw new ServiceException(1053, "reservation doesn’t exist");
        //}
        Reservation updatedReservation = this.reservationRepository.save(this.convertReservationDtoToReservation(reservationDto));
        return this.convertReservationToReservationDto(updatedReservation);
    }

    @Override
    @PreAuthorize("hasAnyRole('ADMIN', 'CUSTOMER')")
    public ReservationDto deleteFromWishlist(Long customerId, Long id) throws ServiceException {

        List<Reservation> list = this.reservationRepository.findReservationsByCustomerIdAndOfferIdAndState(customerId, id, 0);


        if (list == null || list.size() <= 0) {
            throw new ServiceException(1052, "no reservation found");
        }
        Reservation toDelete = list.get(0);
        toDelete.setState(3);
        Reservation updatedReservation = this.reservationRepository.save(toDelete);
        return this.convertReservationToReservationDto(updatedReservation);
    }

    @Override
    public ReservationDto convertReservationToReservationDto(Reservation reservation) throws ServiceException {
        if (reservation == null) {
            throw new ServiceException(1052, "invalid reservation object");
        }
        ReservationDto reservationDto = new ReservationDto();

        reservationDto.setId(reservation.getId());
        reservationDto.setReservedAt(reservation.getReservedAt());
        reservationDto.setState(reservation.getState());
        reservationDto.setTimeSlotDto(this.timeSlotService.convertTimeSlotToTimeSlotDto(reservation.getTimeSlot()));
        reservationDto.setCustomerDto(this.customerService.convertCustomerToCustomerDto(reservation.getCustomer()));

        return reservationDto;
    }

    @Override
    public Reservation convertReservationDtoToReservation(ReservationDto reservationDto) throws ServiceException {
        if(reservationDto == null) {
            throw new ServiceException(1052, "invalid reservation object");
        }
        Reservation reservation = new Reservation();
        System.out.println(reservationDto.getReservedAt() + " " + reservationDto.getTimeSlotDto().getId());
        reservation.setId(reservationDto.getId());
        reservation.setState(reservationDto.getState());
        Customer loadedCustomer = customerRepository.findOne(reservationDto.getCustomerDto().getId());
        TimeSlot loadedTimeSlot = timeSlotRepository.findOne(reservationDto.getTimeSlotDto().getId());
        if(reservation.getId() == null && reservation.getState() == 2) { //new booking, amount of booked timeslot offers +1
            int booked = loadedTimeSlot.getQuantity_booked();
            booked++;
            loadedTimeSlot.setQuantity_booked(booked);
        }
        reservation.setReservedAt(reservationDto.getReservedAt());
        reservation.setCustomer(loadedCustomer);
        reservation.setTimeSlot(loadedTimeSlot);
        //System.out.println(reservation.getReservationIdentity().getReservedAt() + " " + reservation.getReservationIdentity().getCustomer().getId());

        return reservation;
    }

    @Override
    public boolean isValidReservationDtoToSave(ReservationDto reservationDto) {

        return reservationDto.getReservedAt() != null && reservationDto.getState() >= 0;
    }

    @Override
    public boolean isValidReservationDtoToUpdate(ReservationDto reservationDto) {
        // TODO
        //return this.isValidReservationDtoToSave(reservationDto) && this.customerService.isValidCustomerDtoToUpdate(reservationDto.getCustomerDto()) && this.offerService.isValidOfferDtoToUpdate(reservationDto.getOfferDto());
        return true;
    }

    private List<ReservationDto> convertReservationDaoListToReservationDtoList(List<Reservation> reservationList) throws ServiceException {
        List<ReservationDto> reservationDtoList = new ArrayList<>();

        for (Reservation reservation : reservationList) {
            reservationDtoList.add(this.convertReservationToReservationDto(reservation));
        }

        return reservationDtoList;
    }
}
