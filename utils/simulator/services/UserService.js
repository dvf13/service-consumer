let fs = require("fs"),
  CONFIG = JSON.parse(fs.readFileSync(process.env['CONFIG_FILE'])),
  StateService = require('./StateService'),
  MetricsService = require('./MetricsService'),
  request = require('request'),
  _ = require('lodash');

module.exports = {
  async createUsers(callback){
    let config = StateService.getConfig();
    let done = _.after(config.concurrentUsers + config.concurrentBusinesses, () => {
      console.log('Done creating users!');
      return callback(null, {usersCount: users.length, businessUsersCount: businessUsers.length});
    });

    let users = [];
    let createdUsers = [];
    let randInt = (Math.random() * 10000).toFixed(0);
    for (let i = 0; i < config.concurrentUsers; i++){
      let birthdate = new Date();
      birthdate.setFullYear(1990);

      let gender = i % 2;
      if(gender === 0){
        gender = 'f';
      } else{
        gender = 'm';
      }

      let newUser = {
        username: 'customer' + randInt + i,
        password: 'customer_password' + randInt + i,
        email: 'max' + randInt + i + '.mustermann' + randInt + i + '@email.com',
        firstName: 'Max' + randInt + i,
        lastName: 'Mustermann' + randInt + i,
        birthdate: birthdate,
        gender: gender,
      };
      users.push(newUser);
    }


    let businessUsers = [];
    let createdBusinessUsers = [];
    for (let i = 0; i < config.concurrentBusinesses; i++){
      let newBusinessUser = {
        username: 'business' + randInt + i,
        password: 'business_password' + randInt + i,
        email: 'acme' + randInt + i + '.business' + randInt + i + '@email.com',
        name: 'acmeInc' + randInt + i,
        street: 'Street' + randInt + i,
        streetNumber: '' + randInt + i,
        zipCode: (i * 1000 + i * 100 + i * 10 + 1) % 9999,
        city: 'City' + randInt + i
      };
      businessUsers.push(newBusinessUser);
    }

    let startDates = [];
    this.createNextUser(config.requestProcessing === 'concurrently', 0, users, createdUsers, config, startDates, done);

    let startDates2 = [];
    this.createNextBusiness(config.requestProcessing === 'concurrently', 0, businessUsers, createdBusinessUsers, config, startDates2, done);

    StateService.setUsers(createdUsers);
    StateService.setBusinessUsers(createdBusinessUsers);
  },

  createNextUser: (concurrently, i, users, createdUsers, config, startDates, done) => {
    if(concurrently && i < users.length - 1){
      module.exports.createNextUser(concurrently, i + 1, users, createdUsers, config, startDates, done);
    }

    let options = {
      uri: config.apiEndpoint + config.registerEndpointUser,
      method: 'POST',
      headers: {
        'Accept': 'application/json'
      },
      body: users[i],
      json: true
    };

    const c = i;
    startDates[c] = Date.now();
    request(options, function (error, response, body) {
      let endDate = Date.now();
      if(error) console.error(error);
      if(!error) createdUsers.push(users[i]);
      MetricsService.logRequestTimeKeep('POST', config.registerEndpointUser, startDates[c], endDate, error);
      if(!concurrently && i < users.length - 1){
        module.exports.createNextUser(concurrently, i + 1, users, createdUsers, config, startDates, done);
      }
      done();
    });
  },

  createNextBusiness: (concurrently, i, businessUsers, createdBusinessUsers, config, startDates, done) => {
    if(concurrently && i < businessUsers.length - 1){
      module.exports.createNextBusiness(concurrently, i + 1, businessUsers, createdBusinessUsers, config, startDates, done);
    }

    let options = {
      uri: config.apiEndpoint + config.registerEndpointBusiness,
      method: 'POST',
      headers: {
        'Accept': 'application/json'
      },
      body: businessUsers[i],
      json: true
    };

    const c = i;
    startDates[c] = Date.now();
    request(options, function (error, response, body) {
      let endDate = Date.now();
      if(error) console.error(error);
      if(!error) createdBusinessUsers.push(businessUsers[i]);
      MetricsService.logRequestTimeKeep('POST', config.registerEndpointBusiness, startDates[c], endDate, error);
      if(!concurrently && i < businessUsers.length - 1){
        module.exports.createNextBusiness(concurrently, i + 1, businessUsers, createdBusinessUsers, config, startDates, done);
      }
      done();
    });
  },

  async loginUsers(loginEndpoint, requestProcessing, callback){
    let config = StateService.getConfig();
    let done = _.after(config.concurrentUsers + config.concurrentBusinesses, () => {
      console.log('Done logging in users!');
      return callback(null);
    });

    let users = StateService.getUsers();
    let businessUsers = StateService.getBusinessUsers();

    let startDates = [];
    this.loginNextUser(requestProcessing === 'concurrently', loginEndpoint, 0, users, config, startDates, done);

    let startDates2 = [];
    this.loginNextBusiness(requestProcessing === 'concurrently', loginEndpoint, 0, businessUsers, config, startDates2, done);
  },

  loginNextUser: (concurrently, loginEndpoint, i, users, config, startDates, done) => {
    if(concurrently && i < users.length - 1){
      module.exports.loginNextUser(concurrently, loginEndpoint, i + 1, users, config, startDates, done);
    }

    const c = i;
    let u = users[i];
    let options = {
      uri: config.apiEndpoint + loginEndpoint,
      method: 'POST',
      headers: {
        'Accept': 'application/json'
      },
      body: {
        username: u.username,
        password: u.password
      },
      json: true
    };

    startDates[c] = Date.now();
    request(options, function (error, response, body) {
      let endDate = Date.now();
      if(error){
        console.error(error);
      } else{
        if(!body.data){
          error = 'Cant find a body.';
        } else{
          if(!body.data.access_token) console.error('No access token.');
          u.authToken = body.data.access_token;
        }
      }
      MetricsService.logRequestTimeKeep('POST', loginEndpoint, startDates[c], endDate, error);
      if(!concurrently && i < users.length - 1){
        module.exports.loginNextUser(concurrently, loginEndpoint, i + 1, users, config, startDates, done);
      }
      done();
    });
  },

  loginNextBusiness: (concurrently, loginEndpoint, i, businessUsers, config, startDates, done) => {
    if(concurrently && i < businessUsers.length - 1){
      module.exports.loginNextBusiness(concurrently, loginEndpoint, i + 1, businessUsers, config, startDates, done);
    }

    let b = businessUsers[i];
    const c = i;
    let options = {
      uri: config.apiEndpoint + loginEndpoint,
      method: 'POST',
      headers: {
        'Accept': 'application/json'
      },
      body: {
        username: b.username,
        password: b.password
      },
      json: true
    };

    startDates[c] = Date.now();
    request(options, function (error, response, body) {
      let endDate = Date.now();
      if(error){
        console.error(error);
      } else{
        if(!body.data){
          error = 'Cant find a body.';
        } else{
          if(!body.data.access_token) console.error('No access token.');
          b.authToken = body.data.access_token;
        }
      }
      MetricsService.logRequestTimeKeep('POST', loginEndpoint, startDates[c], endDate, error);
      if(!concurrently && i < businessUsers.length - 1){
        module.exports.loginNextBusiness(concurrently, loginEndpoint, i + 1, businessUsers, config, startDates, done);
      }
      done();
    });
  }
};