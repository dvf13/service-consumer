package backend.services;

import backend.exceptions.ServiceException;
import backend.models.Customer;
import backend.dtos.CustomerDto;

import java.util.List;

public interface CustomerService {

    List<CustomerDto> getAllCustomers() throws ServiceException;

    List<CustomerDto> getCustomersByUsername(String Username) throws ServiceException;

    CustomerDto getCustomerById(Long id) throws ServiceException;

    CustomerDto saveCustomer(CustomerDto offerDto) throws ServiceException;

    void deleteCustomerById(Long id) throws ServiceException;

    CustomerDto updateCustomer(Long id, CustomerDto offerDto) throws ServiceException;

    CustomerDto convertCustomerToCustomerDto(Customer customer) throws ServiceException;

    Customer convertCustomerDtoToCustomer(CustomerDto customerDto) throws ServiceException;

    boolean usernameAlreadyExists(Long customerId, String username);

    boolean emailAlreadyExists(Long customerId, String email);

    boolean isValidCustomerDtoToSave(CustomerDto customerDto);

    boolean isValidCustomerDtoToUpdate(CustomerDto customerDto);
}
