import HttpService from './HttpService';
import config from '../config';
import UserService from './UserService';

export default {
  async getAllCustomers() {
    return HttpService.get(`${config.apiUrl}customer/`)
      .then((response) => {
        if (!response.data.error) {
          return Promise.resolve(response.data.data);
        }
        // eslint-disable-next-line no-console
        console.error(`Response error message: ${response.data.data.message}`);
        return Promise.reject(response.data.data.message);
      }, error => Promise.reject(error.statusText))
      .catch(e => Promise.reject(e));
  },
  async getCustomer(id) {
    return HttpService.get(`${config.apiUrl}customer/${id}`)
      .then((response) => {
        const responseDto = response.data;

        if (responseDto.error) {
          return Promise.reject(responseDto.data.message);
        }
        return Promise.resolve(responseDto.data);
      }, error => Promise.reject(error.statusText))
      .catch(e => Promise.reject(e));
  },
  async updateCustomer(customer) {
    if (!UserService.isAuthenticated()) {
      return Promise.reject(new Error('Not logged in!'));
    }
    return HttpService.put(`${config.apiUrl}customer/${customer.id}`, customer)
      .then((response) => {
        const responseDto = response.data;

        if (responseDto.error) {
          return Promise.reject(responseDto.data.message);
        }
        return Promise.resolve(responseDto.data);
      }, error => Promise.reject(error.statusText))
      .catch(e => Promise.reject(e));
  },
};
