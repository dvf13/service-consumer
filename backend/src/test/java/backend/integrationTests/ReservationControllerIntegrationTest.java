package backend.integrationTests;

import backend.dtos.*;
import backend.integrationTests.util.SSLUtil;
import backend.integrationTests.util.IntegTestsUtil;
import org.json.JSONObject;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
@ActiveProfiles("test")
@Sql(executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD, scripts="/../resources/data.sql")
public class ReservationControllerIntegrationTest {

    private static final String AUTHORIZED_CUSTOMER_NAME = "user1";
    private static final String AUTHORIZED_CUSTOMER_PASS = "12345";
    private static final String UNAUTHORIZED_USER_NAME = "business1";
    private static final String UNAUTHORIZED_USER_PASS = "12345";

    private static final String RESERVED_STATE = "0";
    private static final String BOOKED_STATE = "2";
    private static final String CANCELLED_STATE = "3";

    @Autowired
    private TestRestTemplate testRestTemplate;

    static {
        javax.net.ssl.HttpsURLConnection.setDefaultHostnameVerifier(
                (hostname, sslSession) -> hostname.equals("localhost"));
    }

    @Before
    public void turnOffSslChecking() throws Exception {
        SSLUtil.deactivateSslChecking();
    }

    @After
    public void turnOnSslChecking() throws Exception {
        SSLUtil.activateSslChecking();
    }

    @Test
    public void getAllReservationsByCustomerId_AuthorizedLogin() throws Exception{
        assertTrue(IntegTestsUtil.login(AUTHORIZED_CUSTOMER_NAME, AUTHORIZED_CUSTOMER_PASS, this.testRestTemplate));

        HttpHeaders authHeader = IntegTestsUtil.getAuthHeader();
        HttpEntity<ReservationControllerIntegrationTest> entity = new HttpEntity<>(null, authHeader);

        ResponseEntity<ResponseDto> responseEntity = this.testRestTemplate.exchange("/api/reservation/customer/1/reserved/", HttpMethod.GET, entity, ResponseDto.class);
        ResponseDto responseDto = responseEntity.getBody();

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertFalse(responseDto.isError());

        ArrayList<JSONObject> result = IntegTestsUtil.convertArrayListOfLinkedHashMapsToArrayListOfJsonObjects((ArrayList<LinkedHashMap>) responseDto.getData());
        assertEquals(1, result.size());

        assertEquals(RESERVED_STATE, String.valueOf(result.get(0).getInt("state")));

        assertTrue(IntegTestsUtil.logout(this.testRestTemplate));
    }

    @Test
    public void getAllReservationsByCustomerId_UnauthorizedLogin() throws Exception{
        assertTrue(IntegTestsUtil.login(UNAUTHORIZED_USER_NAME, UNAUTHORIZED_USER_PASS, this.testRestTemplate));

        HttpHeaders authHeader = IntegTestsUtil.getAuthHeader();
        HttpEntity<ReservationControllerIntegrationTest> entity = new HttpEntity<>(null, authHeader);

        ResponseEntity<String> responseEntity = this.testRestTemplate.exchange("/api/reservation/customer/1/reserved/", HttpMethod.GET, entity, String.class);

        assertEquals(HttpStatus.FORBIDDEN, responseEntity.getStatusCode());
        assertTrue(IntegTestsUtil.logout(this.testRestTemplate));
    }

    @Test
    public void getAllBookedReservationsByCustomerId_AuthorizedLogin() throws Exception{
        assertTrue(IntegTestsUtil.login(AUTHORIZED_CUSTOMER_NAME, AUTHORIZED_CUSTOMER_PASS, this.testRestTemplate));

        HttpHeaders authHeader = IntegTestsUtil.getAuthHeader();
        HttpEntity<ReservationControllerIntegrationTest> entity = new HttpEntity<>(null, authHeader);

        ResponseEntity<ResponseDto> responseEntity = this.testRestTemplate.exchange("/api/reservation/customer/1/booked/", HttpMethod.GET, entity, ResponseDto.class);
        ResponseDto responseDto = responseEntity.getBody();

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertFalse(responseDto.isError());

        ArrayList<JSONObject> result = IntegTestsUtil.convertArrayListOfLinkedHashMapsToArrayListOfJsonObjects((ArrayList<LinkedHashMap>) responseDto.getData());
        assertEquals(0, result.size());

        assertTrue(IntegTestsUtil.logout(this.testRestTemplate));
    }

    @Test
    public void getAllBookedReservationsByCustomerId_UnauthorizedLogin() throws Exception{
        assertTrue(IntegTestsUtil.login(UNAUTHORIZED_USER_NAME, UNAUTHORIZED_USER_PASS, this.testRestTemplate));

        HttpHeaders authHeader = IntegTestsUtil.getAuthHeader();
        HttpEntity<ReservationControllerIntegrationTest> entity = new HttpEntity<>(null, authHeader);

        ResponseEntity<String> responseEntity = this.testRestTemplate.exchange("/api/reservation/customer/1/booked/", HttpMethod.GET, entity, String.class);

        assertEquals(HttpStatus.FORBIDDEN, responseEntity.getStatusCode());
        assertTrue(IntegTestsUtil.logout(this.testRestTemplate));
    }

    @Test
    public void getAllCancelledReservationsByCustomerId_AuthorizedLogin() throws Exception{
        assertTrue(IntegTestsUtil.login(AUTHORIZED_CUSTOMER_NAME, AUTHORIZED_CUSTOMER_PASS, this.testRestTemplate));

        HttpHeaders authHeader = IntegTestsUtil.getAuthHeader();
        HttpEntity<ReservationControllerIntegrationTest> entity = new HttpEntity<>(null, authHeader);

        ResponseEntity<ResponseDto> responseEntity = this.testRestTemplate.exchange("/api/reservation/customer/1/cancelled/", HttpMethod.GET, entity, ResponseDto.class);
        ResponseDto responseDto = responseEntity.getBody();

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertFalse(responseDto.isError());

        ArrayList<JSONObject> result = IntegTestsUtil.convertArrayListOfLinkedHashMapsToArrayListOfJsonObjects((ArrayList<LinkedHashMap>) responseDto.getData());
        assertEquals(0, result.size());

        assertTrue(IntegTestsUtil.logout(this.testRestTemplate));
    }

    @Test
    public void getAllCancelledReservationsByCustomerId_UnauthorizedLogin() throws Exception{
        assertTrue(IntegTestsUtil.login(UNAUTHORIZED_USER_NAME, UNAUTHORIZED_USER_PASS, this.testRestTemplate));

        HttpHeaders authHeader = IntegTestsUtil.getAuthHeader();
        HttpEntity<ReservationControllerIntegrationTest> entity = new HttpEntity<>(null, authHeader);

        ResponseEntity<String> responseEntity = this.testRestTemplate.exchange("/api/reservation/customer/1/cancelled/", HttpMethod.GET, entity, String.class);

        assertEquals(HttpStatus.FORBIDDEN, responseEntity.getStatusCode());
        assertTrue(IntegTestsUtil.logout(this.testRestTemplate));
    }

    @Test
    public void saveReservation() throws Exception{
        assertTrue(IntegTestsUtil.login(AUTHORIZED_CUSTOMER_NAME, AUTHORIZED_CUSTOMER_PASS, this.testRestTemplate));

        HttpHeaders authHeader = IntegTestsUtil.getAuthHeader();

        HttpEntity<TimeSlotControllerIntegrationTests> entity = new HttpEntity<>(null, authHeader);
        ResponseEntity<ResponseDto> responseEntity = this.testRestTemplate.exchange("/api/timeslot/1", HttpMethod.GET, entity, ResponseDto.class);
        ResponseDto responseDto = responseEntity.getBody();
        ArrayList<JSONObject> timeSlots = IntegTestsUtil.convertArrayListOfLinkedHashMapsToArrayListOfJsonObjects((ArrayList<LinkedHashMap>) responseDto.getData());


        HttpEntity<CustomerControllerIntegrationTest> entity1 = new HttpEntity<>(null, authHeader);
        ResponseEntity<ResponseDto> responseEntity1 = this.testRestTemplate.exchange("/api/customer/1", HttpMethod.GET, entity1, ResponseDto.class);
        ResponseDto responseDto1 = responseEntity1.getBody();
        JSONObject customerJSON = IntegTestsUtil.convertLinkedHashMapToJsonObject((LinkedHashMap) responseDto1.getData());

        ReservationDto reservationDto = new ReservationDto();
        reservationDto.setReservedAt(new Date(2018,1,1));
        reservationDto.setState(2);
        reservationDto.setCustomerDto(IntegTestsUtil.convertJsonObjectToCustomerDto(customerJSON));
        reservationDto.setTimeSlotDto(IntegTestsUtil.convertJsonObjectToTimeSlotDto(timeSlots.get(0)));

        HttpEntity<ReservationDto> entity2 = new HttpEntity<>(reservationDto, authHeader);
        ResponseEntity<ResponseDto> responseEntity2 = this.testRestTemplate.exchange("/api/reservation/", HttpMethod.POST, entity2, ResponseDto.class);
        ResponseDto responseDto2 = responseEntity2.getBody();

        HttpEntity<ReservationControllerIntegrationTest> entity3 = new HttpEntity<>(null, authHeader);
        ResponseEntity<ResponseDto> responseEntity3 = this.testRestTemplate.exchange("/api/reservation/", HttpMethod.GET, entity3, ResponseDto.class);
        ResponseDto responseDto3 = responseEntity3.getBody();

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertFalse(responseDto.isError());
        assertEquals(HttpStatus.OK, responseEntity1.getStatusCode());
        assertFalse(responseDto1.isError());
        assertEquals(HttpStatus.OK, responseEntity2.getStatusCode());
        assertFalse(responseDto2.isError());
        assertEquals(HttpStatus.OK, responseEntity3.getStatusCode());
        assertFalse(responseDto3.isError());

        ArrayList<JSONObject> result = IntegTestsUtil.convertArrayListOfLinkedHashMapsToArrayListOfJsonObjects((ArrayList<LinkedHashMap>) responseDto3.getData());
        assertEquals(3, result.size());

        assertTrue(IntegTestsUtil.logout(this.testRestTemplate));
    }

}
