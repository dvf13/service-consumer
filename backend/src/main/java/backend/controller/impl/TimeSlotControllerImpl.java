package backend.controller.impl;

import backend.controller.OfferController;
import backend.controller.TimeSlotController;
import backend.dtos.ErrorDto;
import backend.dtos.TimeSlotDto;
import backend.dtos.ResponseDto;
import backend.exceptions.ServiceException;
import backend.services.OfferService;
import backend.services.TimeSlotService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@RestController
@RequestMapping("/api/timeslot/")
public class TimeSlotControllerImpl implements TimeSlotController {

    private TimeSlotService timeSlotService;


    @Autowired
    public TimeSlotControllerImpl(TimeSlotService timeSlotService) {
        this.timeSlotService = timeSlotService;
    }


    @Override
    @ResponseBody
    @RequestMapping(value = "", method = RequestMethod.GET)
    public ResponseDto getAllTimeSlots() {
        try {
            List<TimeSlotDto> timeSlotDtoListResponse = this.timeSlotService.getAllTimeSlots();
            return new ResponseDto<>(false, timeSlotDtoListResponse);
        } catch (ServiceException e) {
            log.error(e.getMessage());
            return new ResponseDto<>(true, new ErrorDto(e.getCode(), e.getMessage()));
        }
    }

    @Override
    @ResponseBody
    @RequestMapping(value = "{id}", method = RequestMethod.GET)
    public ResponseDto getTimeSlotsByOfferId(@PathVariable Long id) {
        try {
            List<TimeSlotDto> timeSlotDtoListResponse = this.timeSlotService.getTimeSlotByOffer(id);
            for(TimeSlotDto d : timeSlotDtoListResponse){
                System.out.println(d.getId());
            }
            return new ResponseDto<>(false, timeSlotDtoListResponse);
        } catch (ServiceException e) {
            log.error(e.getMessage());
            return new ResponseDto<>(true, new ErrorDto(e.getCode(), e.getMessage()));
        }
    }

    @Override
    @ResponseBody
    @RequestMapping(value = "", method = RequestMethod.POST)
    public ResponseDto saveTimeSlot(@RequestBody TimeSlotDto timeSlotDto) {
        try {
            TimeSlotDto timeSlotDtoResponse = this.timeSlotService.saveTimeSlot(timeSlotDto);
            return new ResponseDto<>(false, timeSlotDtoResponse);
        } catch (ServiceException e) {
            log.error(e.getMessage());
            return new ResponseDto<>(true, new ErrorDto(e.getCode(), e.getMessage()));
        }
    }

    @Override
    @ResponseBody
    @RequestMapping(value = "{id}", method = RequestMethod.DELETE)
    public void deleteTimeSlotById(@PathVariable Long id) {
        try {
            this.timeSlotService.deleteTimeSlotById(id);
        } catch (ServiceException e) {
            log.error(e.getMessage());
        }
    }

    @Override
    @ResponseBody
    @RequestMapping(value = "{id}", method = RequestMethod.PUT)
    public ResponseDto updateTimeSlot(@PathVariable Long id, @RequestBody TimeSlotDto timeSlotDto) {
        try {
            TimeSlotDto timeSlotDtoResponse = this.timeSlotService.updateTimeSlot(id, timeSlotDto);
            return new ResponseDto<>(false, timeSlotDtoResponse);
        } catch(ServiceException e) {
            log.error(e.getMessage());
            return new ResponseDto<>(true, new ErrorDto(e.getCode(), e.getMessage()));
        }
    }
}
