package backend.services;

import backend.exceptions.ServiceException;
import backend.models.Offer;
import backend.dtos.OfferDto;

import java.util.Date;
import java.util.List;

public interface OfferService {

    List<OfferDto> findOffersByBusinessIdOrderByValidUntil(Long business_id) throws ServiceException;

    List<OfferDto> findOffersByBusinessIdFromTo(Long business_id, Date from, Date to) throws ServiceException;

    List<OfferDto> getAllOffers() throws ServiceException;

    List<OfferDto> getSomeOffers() throws ServiceException;

    OfferDto getOfferById(Long id) throws ServiceException;

    List<OfferDto> getOffersByTimeSlotIds(Long[] id) throws ServiceException;

    OfferDto saveOffer(OfferDto offerDto) throws ServiceException;

    void deleteOfferById(Long id) throws ServiceException;

    OfferDto updateOffer(Long id, OfferDto offerDto) throws ServiceException;

    OfferDto convertOfferToOfferDto(Offer offer) throws ServiceException;

    Offer convertOfferDtoToOffer(OfferDto offerDto) throws ServiceException;

    boolean isValidOfferDtoToSave(OfferDto offerDto);

    boolean isValidOfferDtoToUpdate(OfferDto offerDto);
}
