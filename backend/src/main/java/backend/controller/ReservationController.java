package backend.controller;

import backend.dtos.ReservationDto;
import backend.dtos.ResponseDto;

import java.util.Date;
import java.util.List;

public interface ReservationController {

    ResponseDto getAllReservations();

    ResponseDto findReservationsByCustomerId(Long customerId, Integer state);

    ResponseDto findBookedReservationsByCustomerId(Long customerId, Integer state);

    ResponseDto findCancelledReservationsByCustomerId(Long customerId, Integer state);

    ResponseDto getReservationById(Long id);

    ResponseDto getReservationsByTimeSlotId(Long id);

    ResponseDto saveReservation(ReservationDto reservationDto);

    void deleteReservationById(Long id);

    ResponseDto updateReservation(ReservationDto reservationDto);

    ResponseDto deleteWishlistEntry(Long customerId, Long id);

    ResponseDto findReservationsByState(Integer state);
}
