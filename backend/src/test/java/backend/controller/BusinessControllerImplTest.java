package backend.controller;

import backend.controller.impl.BusinessControllerImpl;
import backend.dtos.BusinessDto;
import backend.dtos.ErrorDto;
import backend.dtos.ResponseDto;
import backend.exceptions.ServiceException;
import backend.models.Business;
import backend.services.BusinessService;
import org.junit.Test;
import org.mockito.Mockito;


import static org.hamcrest.Matchers.instanceOf;
import static org.mockito.Mockito.*;
import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;


public class BusinessControllerImplTest {

    private BusinessService businessService;
    private BusinessController businessController;

    public BusinessControllerImplTest() {
        this.businessService = mock(BusinessService.class);
        this.businessController = new BusinessControllerImpl(businessService);
    }

    @Test
    public void getAllBusinessesTest_NOError() throws ServiceException {
        when(this.businessService.getAllBusinesses()).thenReturn(this.getBusinessDtoList());

        ResponseDto responseDto = this.businessController.getAllBusinesses();

        assertFalse(responseDto.isError());
        assertNotNull(responseDto);
        assertNotNull(responseDto.getData());
        assertThat(responseDto.getData(), instanceOf(ArrayList.class));
        assertEquals(getBusinessDtoList(), responseDto.getData());
    }

    @Test
    public void getAllBusinessesTest_Error() throws ServiceException {
        when(this.businessService.getAllBusinesses()).thenThrow(ServiceException.class);

        ResponseDto responseDto = this.businessController.getAllBusinesses();

        assertTrue(responseDto.isError());
        assertNotNull(responseDto);
        assertNotNull(responseDto.getData());
        assertThat(responseDto.getData(), instanceOf(ErrorDto.class));
    }

    @Test
    public void getBusinessByIdTest_NOError() throws ServiceException {
        when(this.businessService.getBusinessById(Mockito.anyLong())).thenReturn(getBusinessDto());

        ResponseDto responseDto = this.businessController.getBusinessById(1L);

        assertFalse(responseDto.isError());
        assertNotNull(responseDto);
        assertNotNull(responseDto.getData());
        assertThat(responseDto.getData(), instanceOf(BusinessDto.class));
        assertEquals(getBusinessDto(), responseDto.getData());
    }

    @Test
    public void getBusinessByIdTest_Error() throws ServiceException {
        when(this.businessService.getBusinessById(Mockito.anyLong())).thenThrow(ServiceException.class);

        ResponseDto responseDto = this.businessController.getBusinessById(-1L);

        assertTrue(responseDto.isError());
        assertNotNull(responseDto);
        assertNotNull(responseDto.getData());
        assertThat(responseDto.getData(), instanceOf(ErrorDto.class));
    }

    @Test
    public void saveBusinessTest_NOError() throws ServiceException {
        when(this.businessService.saveBusiness(any(BusinessDto.class))).thenReturn(getBusinessDto());

        ResponseDto responseDto = this.businessController.saveBusiness(getBusinessDto());

        assertFalse(responseDto.isError());
        assertNotNull(responseDto);
        assertNotNull(responseDto.getData());
        assertThat(responseDto.getData(), instanceOf(BusinessDto.class));
        assertEquals(getBusinessDto(), responseDto.getData());
    }

    @Test
    public void saveBusinessTest_Error() throws ServiceException {
        when(this.businessService.saveBusiness(any(BusinessDto.class))).thenThrow(ServiceException.class);

        ResponseDto responseDto = this.businessController.saveBusiness(null);

        assertTrue(responseDto.isError());
        assertNotNull(responseDto);
        assertNotNull(responseDto.getData());
        assertThat(responseDto.getData(), instanceOf(ErrorDto.class));
    }

    @Test
    public void updateBusinessTest_NOError() throws ServiceException {
        when(this.businessService.updateBusiness(Mockito.anyLong(), any(BusinessDto.class)))
                .thenReturn(getBusinessDto());

        ResponseDto responseDto = this.businessController.updateBusiness(1L, getBusinessDto());

        assertFalse(responseDto.isError());
        assertNotNull(responseDto);
        assertNotNull(responseDto.getData());
        assertThat(responseDto.getData(), instanceOf(BusinessDto.class));
        assertEquals(getBusinessDto(), responseDto.getData());
    }

    @Test
    public void updateBusinessTest_Error() throws ServiceException {
        when(this.businessService.updateBusiness(Mockito.anyLong(), any(BusinessDto.class)))
                .thenThrow(ServiceException.class);

        ResponseDto responseDto = this.businessController.updateBusiness(1L, null);

        assertTrue(responseDto.isError());
        assertNotNull(responseDto);
        assertNotNull(responseDto.getData());
        assertThat(responseDto.getData(), instanceOf(ErrorDto.class));
    }

    @Test
    public void deleteBusinessByIdTest_Error() throws ServiceException {
        doThrow(ServiceException.class).when(this.businessService).deleteBusinessById(Mockito.anyLong());

        this.businessController.deleteBusinessById(-1L);
    }

    private BusinessDto getBusinessDto() {
        BusinessDto businessDto = new BusinessDto();
        businessDto.setId(1L);
        businessDto.setName("Test_Business_User");
        return businessDto;
    }

    private List<BusinessDto> getBusinessDtoList() {
        List<BusinessDto> businessDtoArrayList = new ArrayList<>();
        businessDtoArrayList.add(getBusinessDto());
        return businessDtoArrayList;
    }
}
