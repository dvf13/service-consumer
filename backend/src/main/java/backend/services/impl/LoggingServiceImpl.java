package backend.services.impl;

import backend.models.Reservation;
import backend.services.LoggingService;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.client.Client;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.geo.GeoPoint;
import org.elasticsearch.common.transport.InetSocketTransportAddress;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.ExecutionException;

import static org.elasticsearch.common.xcontent.XContentFactory.jsonBuilder;


@Service

public class LoggingServiceImpl implements LoggingService {

    @Override

    public void logToElasticsearch(Reservation reservation) {

        DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S");

        Date time_booked = new Date();

        Date time_start = new Date();
        try {
            time_start = format.parse(reservation.timeSlot.getBeg_date_time().toString());
        } catch (ParseException e) {
            e.printStackTrace();
        }

        Date time_end = new Date();
        try {
            time_end = format.parse(reservation.timeSlot.getEnd_date_time().toString());
        } catch (ParseException e) {
            e.printStackTrace();
        }

        String gender = reservation.customer.getGenderAsString();

        int age = reservation.customer.getAge();

        String prov_type = reservation.timeSlot.getOffer().getCategory().getName();

        String pid = reservation.timeSlot.getOffer().getBusiness().getId() + "";

        String uid = reservation.customer.getId() + "";

        String prov_name = reservation.timeSlot.getOffer().getBusiness().getName();

        double lat = reservation.timeSlot.getOffer().getLatitude();

        double logt = reservation.timeSlot.getOffer().getLongitude();

        try {

            Client client = TransportClient.builder().build().addTransportAddress(new InetSocketTransportAddress(InetAddress.getByName("localhost"), 9300));

            IndexResponse response = client.prepareIndex("ase", "logs")

                    .setSource(jsonBuilder()

                            .startObject()

                            .field("time_booked", time_booked)

                            .field("time_start", time_start)

                            .field("time_end", time_end)

                            .field("age", age)

                            .field("gender", gender)

                            .field("pid", pid)

                            .field("uid", uid)

                            .field("prov_type", prov_type)

                            .field("prov_name", prov_name)

                            .field("prov_loc", new GeoPoint(lat, logt))

                            .endObject()

                    ).execute().get();

            client.close();

        } catch (UnknownHostException e) {

            e.printStackTrace();

        } catch (InterruptedException e) {

            e.printStackTrace();

        } catch (ExecutionException e) {

            e.printStackTrace();

        } catch (IOException e) {

            e.printStackTrace();

        }



    }

}
