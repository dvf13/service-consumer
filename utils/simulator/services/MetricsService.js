let fs = require("fs"),
    data = {},
    keepData = {},
    CONFIG = JSON.parse(fs.readFileSync(process.env['CONFIG_FILE']));

module.exports = {

  reset(){
    data = {};
    return true;
  },

  resetComplete(){
    data = {};
    keepData = {};
    return true;
  },

  getData(){
    let returnData = {};
    return Object.assign(returnData, data, keepData);
  },

  logRequestTime(requestType, endpoint, startTime, endTime, error){
    if(!data[endpoint]) data[endpoint] = {};
    if(error){
      if(!data.errors) data.errors = {};
      if(!data.errors[requestType]) data.errors[requestType] = {};
      if(!data.errors[requestType][endpoint]) data.errors[requestType][endpoint] = 0;
      data.errors[requestType][endpoint] ++;
    } else{
      if(!data[endpoint].loggedTimes) data[endpoint].loggedTimes = {};
      if(!data[endpoint].loggedTimes[requestType]) data[endpoint].loggedTimes[requestType] = [];
      data[endpoint].loggedTimes[requestType].push(endTime - startTime);
    }
  },

  logRequestTimeKeep(requestType, endpoint, startTime, endTime, error){
    if(!keepData[endpoint]) keepData[endpoint] = {};
    if(error){
      if(!keepData.errors) keepData.errors = {};
      if(!keepData.errors[requestType]) keepData.errors[requestType] = {};
      if(!keepData.errors[requestType][endpoint]) keepData.errors[requestType][endpoint] = 0;
      keepData.errors[requestType][endpoint] ++;
    } else{
      if(!keepData[endpoint].loggedTimes) keepData[endpoint].loggedTimes = {};
      if(!keepData[endpoint].loggedTimes[requestType]) keepData[endpoint].loggedTimes[requestType] = [];
      keepData[endpoint].loggedTimes[requestType].push(endTime - startTime);
    }
  }
};