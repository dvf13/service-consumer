package backend.controller;

import backend.auth.JwtAuthenticationRequest;
import backend.dtos.ResponseDto;
import org.springframework.mobile.device.Device;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.security.Principal;


public interface AuthenticationController {

    ResponseDto createAuthenticationToken(JwtAuthenticationRequest authenticationRequest, HttpServletResponse response, Device device);

    ResponseDto logout(HttpServletRequest request);

    ResponseDto refresh(HttpServletRequest request, Principal principal);

    ResponseDto getUserInfo(Principal user);
}
