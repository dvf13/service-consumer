import Vue from 'vue';
import axios from 'axios';
import CONFIG from '../config';

export default {
  async getData(){
    return axios.get(`http://localhost:${CONFIG.backend_port}${CONFIG.api_endpoint}/data/`, {headers: {'Content-Type': 'application/json'}})
      .then(response => {
        if (!!response.data.success) {
          return Promise.resolve(response.data.data);
        } else {
          return Promise.reject(response.data.message);
        }
      });
  },

  async resetData(){
    return axios.post(`http://localhost:${CONFIG.backend_port}${CONFIG.api_endpoint}/data/reset/`, {}, {headers: {'Content-Type': 'application/json'}})
      .then(response => {
        if (!!response.data.success) {
          return Promise.resolve(response.data.data);
        } else {
          return Promise.reject(response.data.message);
        }
      });
  },
}