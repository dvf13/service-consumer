package backend.integrationTests;

import backend.dtos.BusinessDto;
import backend.dtos.CategoryDto;
import backend.dtos.OfferDto;
import backend.dtos.ResponseDto;
import backend.integrationTests.util.SSLUtil;
import backend.integrationTests.util.IntegTestsUtil;
import org.json.JSONObject;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
@ActiveProfiles("test")
@Sql(executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD, scripts="/../resources/data.sql")
public class BusinessControllerIntegrationTest {

    @Autowired
    private TestRestTemplate testRestTemplate;

    static {
        javax.net.ssl.HttpsURLConnection.setDefaultHostnameVerifier(
                (hostname, sslSession) -> hostname.equals("localhost"));
    }

    @Before
    public void turnOffSslChecking() throws Exception {
        SSLUtil.deactivateSslChecking();
    }

    @After
    public void turnOnSslChecking() throws Exception {
        SSLUtil.activateSslChecking();
    }

    @Test
    public void getAllBusinessesAsAdminUser() throws Exception{
        assertTrue(IntegTestsUtil.login("user3", "123", this.testRestTemplate));

        HttpHeaders authHeader = IntegTestsUtil.getAuthHeader();

        HttpEntity<BusinessControllerIntegrationTest> entity = new HttpEntity<>(null, authHeader);

        ResponseEntity<ResponseDto> responseEntity = this.testRestTemplate.exchange("/api/business/", HttpMethod.GET, entity, ResponseDto.class);

        ResponseDto responseDto = responseEntity.getBody();

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertFalse(responseDto.isError());

        ArrayList<JSONObject> result = IntegTestsUtil.convertArrayListOfLinkedHashMapsToArrayListOfJsonObjects((ArrayList<LinkedHashMap>) responseDto.getData());

        assertEquals(2, result.size());

        assertEquals(1, result.get(0).getInt("id"));
        assertEquals("business1", result.get(0).getString("username"));

        assertEquals(2, result.get(1).getInt("id"));
        assertEquals("business2", result.get(1).getString("username"));

        assertTrue(IntegTestsUtil.logout(this.testRestTemplate));
    }

    @Test
    public void getAllBusinessesAsNormalUser_shouldBeForbidden() throws Exception{
        assertTrue(IntegTestsUtil.login("business1", "12345", this.testRestTemplate));

        HttpHeaders authHeader = IntegTestsUtil.getAuthHeader();

        HttpEntity<BusinessControllerIntegrationTest> entity = new HttpEntity<>(null, authHeader);

        ResponseEntity<String> responseEntity = this.testRestTemplate.exchange("/api/business/", HttpMethod.GET, entity, String.class);
        System.out.println(responseEntity.getBody());

        assertEquals(HttpStatus.FORBIDDEN, responseEntity.getStatusCode());
        assertTrue(IntegTestsUtil.logout(this.testRestTemplate));
    }

    @Test
    public void getBusinessById_loggedInUser() throws Exception{
        assertTrue(IntegTestsUtil.login("business1", "12345", this.testRestTemplate));

        HttpHeaders authHeader = IntegTestsUtil.getAuthHeader();

        HttpEntity<BusinessControllerIntegrationTest> entity = new HttpEntity<>(null, authHeader);

        ResponseEntity<ResponseDto> responseEntity = this.testRestTemplate.exchange("/api/business/1", HttpMethod.GET, entity, ResponseDto.class);
        ResponseDto responseDto = responseEntity.getBody();

        JSONObject receivedOffer = IntegTestsUtil.convertLinkedHashMapToJsonObject((LinkedHashMap) responseDto.getData());

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertFalse(responseDto.isError());
        assertEquals("1", String.valueOf(receivedOffer.getLong("id")));
        assertEquals("business1", receivedOffer.getString("username"));
        assertEquals("business1@email.com", receivedOffer.getString("email"));
        assertEquals("Company1", receivedOffer.getString("name"));

        assertTrue(IntegTestsUtil.logout(this.testRestTemplate));
    }

    @Test
    public void saveBusinessAsLoggedInUser() throws Exception{
        assertTrue(IntegTestsUtil.login("business1", "12345", this.testRestTemplate));

        HttpHeaders authHeader = IntegTestsUtil.getAuthHeader();

        BusinessDto businessDto = this.createNewBusiness();

        HttpEntity<BusinessDto> entity = new HttpEntity<>(businessDto, authHeader);

        ResponseEntity<ResponseDto> responseEntity = this.testRestTemplate.exchange("/api/business/", HttpMethod.POST, entity, ResponseDto.class);
        ResponseDto responseDto = responseEntity.getBody();
        String id = responseDto.getData().toString().split(",")[0].split("=")[1];

        HttpEntity<BusinessControllerIntegrationTest> entity1 = new HttpEntity<>(null, authHeader);

        ResponseEntity<ResponseDto> responseEntity1 = this.testRestTemplate.exchange("/api/business/" + id, HttpMethod.GET, entity1, ResponseDto.class);
        ResponseDto responseDto1 = responseEntity1.getBody();

        JSONObject receivedOffer = IntegTestsUtil.convertLinkedHashMapToJsonObject((LinkedHashMap) responseDto1.getData());

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertFalse(responseDto.isError());

        assertEquals(HttpStatus.OK, responseEntity1.getStatusCode());
        assertFalse(responseDto1.isError());


        assertEquals("Test_Business", receivedOffer.getString("username"));
        assertEquals("business@mail.com", receivedOffer.getString("email"));
        assertEquals("Test Business", receivedOffer.getString("name"));

        assertTrue(IntegTestsUtil.logout(this.testRestTemplate));
    }

    // Check this again - shouldn't get redirected?
    @Test
    public void saveNewCategoryAsAnonymUser_shouldGetRedirectTroughToNotLoggedIn() {
        BusinessDto businessDto = this.createNewBusiness();

        ResponseEntity<ResponseDto> responseEntity = this.testRestTemplate.postForEntity("/api/business/", businessDto, ResponseDto.class);

        assertEquals(HttpStatus.FOUND, responseEntity.getStatusCode());
    }

    private BusinessDto createNewBusiness(){
        BusinessDto businessDto = new BusinessDto();

        businessDto.setUsername("Test_Business");
        businessDto.setPassword("12345");
        businessDto.setEmail("business@mail.com");
        businessDto.setName("Test Business");
        businessDto.setStreet("Teststreet");
        businessDto.setStreetNumber("1");
        businessDto.setZipCode(1010);
        businessDto.setCity("Vienna");

        return businessDto;
    }
}
