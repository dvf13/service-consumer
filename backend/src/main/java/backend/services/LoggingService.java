package backend.services;

import backend.models.Reservation;

public interface LoggingService {

    void logToElasticsearch(Reservation reservation);

}
