package backend.repositories;

import backend.models.Reservation;
import backend.models.ReservationIdentity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Date;

/**
 * Reservation repository for jpa
 */
@Repository
public interface ReservationRepository extends JpaRepository<Reservation, Long> {

    @Query(value = "select * from offer inner join timeslot on offer.id = offer_id inner join reservation on timeslot.id = timeslot_id where customer_id = :customer_id and state = :state", nativeQuery = true)
    List<Reservation> findReservationsByCustomerId(@Param("customer_id") Long customerId, @Param("state") Integer state);

    @Query(value = "select * from reservation where timeslot_id = :timeslot_id", nativeQuery = true)
    List<Reservation> findReservationsByTimeSlotId(@Param("timeslot_id") Long timeSlotId);

    //@Query("SELECT r from reservation r where r.reservationIdentity.reservedAt = :reservedAt")
    //Reservation findByIDs(@Param("reservedAt") Date reservedAt);

    @Query(value = "select * from reservation inner join timeslot on reservation.timeslot_id = timeslot.id inner join offer on timeslot.offer_id = :offer_id where state = :state and customer_id = :customer_id", nativeQuery = true)
    List<Reservation> findReservationsByCustomerIdAndOfferIdAndState(@Param("customer_id") Long customerId, @Param("offer_id") Long offerId, @Param("state") Integer state);

    List<Reservation> findReservationsByState(@Param("state") Integer state);
}