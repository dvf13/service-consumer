package backend.dtos;

import lombok.Data;

@Data
public class ErrorDto {

    private int code;

    private String message;

    private ErrorDto() {}

    public ErrorDto(int code, String message) {
        this.code = code;
        this.message = message;
    }
}
