package backend.services;

import backend.dtos.CategoryDto;
import backend.exceptions.ServiceException;
import backend.models.Category;
import backend.repositories.CategoryRepository;
import backend.services.impl.CategoryServiceImpl;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;

public class CategoryServiceImplTest {

    private CategoryRepository categoryRepository;

    private CategoryService categoryService;

    public CategoryServiceImplTest(){
        this.categoryRepository = mock(CategoryRepository.class);
        this.categoryService = new CategoryServiceImpl(categoryRepository);
    }

    @Test
    public void getAllCategoriesTest_shouldFindAll() throws ServiceException{
        when(this.categoryRepository.findAll()).thenReturn(getCategoryList());

        List<CategoryDto> categoryDtoList = this.categoryService.getAllCategories();

        assertNotNull(categoryDtoList);
        assertEquals(1, categoryDtoList.size());
        assertEquals((Long) 1L, categoryDtoList.get(0).getId());
        assertEquals("Test_Category", categoryDtoList.get(0).getName());
    }

    @Test(expected = ServiceException.class)
    public void getAllCategories_NoCategoryInRepository_shouldThrowServiceException() throws ServiceException{
        when(this.categoryRepository.findAll()).thenReturn(null);

        List<CategoryDto> categoryDtoList = this.categoryService.getAllCategories();

        assertNull(categoryDtoList);
    }
    @Test
    public void getCategoryByIdTest_ValidId_shouldFindOne() throws ServiceException{
        when(this.categoryRepository.findOne(Mockito.anyLong())).thenReturn(getCategory());

        CategoryDto categoryDto = this.categoryService.getCategoryById(1L);

        assertNotNull(categoryDto);
        assertEquals((Long) 1L, categoryDto.getId());
        assertEquals("Test_Category", categoryDto.getName());
    }

    @Test(expected = ServiceException.class)
    public void getCategoryByIdTest_ValidIdButNotExisting_ShouldThrowServiceException() throws ServiceException{
        when(this.categoryRepository.findOne(Mockito.anyLong())).thenReturn(null);

        CategoryDto categoryDto = this.categoryService.getCategoryById(1L);

        assertNull(categoryDto);
    }

    @Test(expected = ServiceException.class)
    public void getCategoryByIdTest_InvalidId_ShouldThrowServiceException() throws ServiceException {
        when(this.categoryRepository.findOne(Mockito.anyLong())).thenReturn(null);

        CategoryDto categoryDto = this.categoryService.getCategoryById(-1L);

    }

    @Test(expected = ServiceException.class)
    public void saveCategoryTest_CategoryNotValid_shouldThrowServiceException() throws ServiceException{
        when(this.categoryRepository.save(any(Category.class))).thenReturn(null);

        CategoryDto categoryDto = this.categoryService.saveCategory(null);

        assertNull(categoryDto);
    }

    @Test(expected = ServiceException.class)
    public void updateCategoryTest_IdIsNull_shouldThrowServiceException() throws ServiceException{
        CategoryDto categoryDto = this.categoryService.updateCategory(null, getCategoryDto());
        assertNull(categoryDto);
    }

    @Test(expected = ServiceException.class)
    public void updateCategoryTest_IdBelowZero_shouldThrowServiceException() throws ServiceException{
        CategoryDto categoryDto = this.categoryService.updateCategory(-1L, getCategoryDto());
        assertNull(categoryDto);
    }

    @Test(expected = ServiceException.class)
    public void updateCategoryTest_CategoryDtoIsNull_shouldThrowServiceException() throws ServiceException{
        CategoryDto categoryDto = this.categoryService.updateCategory(1L, null);
        assertNull(categoryDto);
    }

    @Test(expected = ServiceException.class)
    public void updateCategoryTest_IdsDoNotMatch_shouldThrowServiceException() throws ServiceException{
        CategoryDto categoryDto = this.categoryService.updateCategory(2L, getCategoryDto());
        assertNull(categoryDto);
    }

    @Test(expected = ServiceException.class)
    public void updateCategoryTest_IdNotInRepository_shouldThrowServiceException() throws ServiceException{
        when(this.categoryRepository.findOne(Mockito.anyLong())).thenReturn(null);

        CategoryDto categoryDto = this.categoryService.updateCategory(1L, getCategoryDto());

        assertNull(categoryDto);
    }

    @Test(expected = ServiceException.class)
    public void deleteCategoryByIdTest_IdIsNull_shouldThrowException() throws ServiceException{
        this.categoryService.deleteCategoryById(null);
    }

    @Test(expected = ServiceException.class)
    public void deleteCategoryByIdTest_IdIsBelowZero_shouldThrowException() throws ServiceException{
        this.categoryService.deleteCategoryById(-1L);
    }

    @Test
    public void isValidCategoryDtoToSaveTest_InvalidCategory_ShouldBeFalse() throws ServiceException{
        CategoryDto categoryDto = new CategoryDto();
        assertFalse(this.categoryService.isValidCategoryDtoToSave(categoryDto));
    }

    @Test
    public void isValidCategoryDtoToUpdateTest_CommitedCategoryNull() throws ServiceException{
        CategoryDto categoryDto = new CategoryDto();
        assertFalse(this.categoryService.isValidCategoryDtoToUpdate(categoryDto));
    }

    @Test
    public void isValidCategoryDtoToUpdateTest_IdIsZero() throws ServiceException{
        CategoryDto categoryDto = getCategoryDto();
        categoryDto.setId(0L);
        assertFalse(this.categoryService.isValidCategoryDtoToUpdate(categoryDto));
    }

    private Category getCategory(){
        Category category = new Category();
        category.setId(1L);
        category.setName("Test_Category");
        return category;
    }

    private List<Category> getCategoryList(){
        List<Category> categoryArrayList = new ArrayList<>();
        categoryArrayList.add(getCategory());
        return categoryArrayList;
    }

    private CategoryDto getCategoryDto(){
        CategoryDto categoryDto = new CategoryDto();
        categoryDto.setId(1L);
        categoryDto.setName("Test_Category");
        return categoryDto;
    }

    private List<CategoryDto> getCategoryDtoList(){
        List<CategoryDto> categoryDtoArrayList = new ArrayList<>();
        categoryDtoArrayList.add(getCategoryDto());
        return categoryDtoArrayList;
    }
}
