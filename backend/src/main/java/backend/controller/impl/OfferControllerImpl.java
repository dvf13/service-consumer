package backend.controller.impl;

import backend.controller.OfferController;
import backend.dtos.ErrorDto;
import backend.dtos.OfferDto;
import backend.dtos.ResponseDto;
import backend.exceptions.ServiceException;
import backend.models.Business;
import backend.services.OfferService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.websocket.server.ServerEndpoint;
import java.util.Date;
import java.util.List;

@Slf4j
@RestController
@RequestMapping("/api/offer/")
public class OfferControllerImpl implements OfferController {

    private OfferService offerService;


    @Autowired
    public OfferControllerImpl(OfferService offerService) {
        this.offerService = offerService;
    }


    @Override
    @ResponseBody
    @RequestMapping(value = "/business/{business_id}", method = RequestMethod.GET)
    public ResponseDto findOffersByBusinessIdOrderByValidUntil(@PathVariable Long business_id) {
        try {
            List<OfferDto> offerDtoListResponse = this.offerService.findOffersByBusinessIdOrderByValidUntil(business_id);
            return new ResponseDto<>(false, offerDtoListResponse);
        } catch (ServiceException e) {
            log.error(e.getMessage());
            return new ResponseDto<>(true, new ErrorDto(e.getCode(), e.getMessage()));
        }
    }

    @Override
    @ResponseBody
    @RequestMapping(value = "/business/{business_id}/{from}/{to}", method = RequestMethod.GET)
    public ResponseDto findOffersByBusinessIdFromTo(@PathVariable Long business_id, @PathVariable Date from, @PathVariable Date to) {
        try {
            List<OfferDto> offerDtoListResponse = this.offerService.findOffersByBusinessIdFromTo(business_id, from, to);
            return new ResponseDto<>(false, offerDtoListResponse);
        } catch (ServiceException e) {
            log.error(e.getMessage());
            return new ResponseDto<>(true, new ErrorDto(e.getCode(), e.getMessage()));
        }
    }


    @Override
    @ResponseBody
    @PreAuthorize("hasAnyRole('ADMIN', 'BUSINESS', 'CUSTOMER')")
    @RequestMapping(value = "", method = RequestMethod.GET)
    public ResponseDto getAllOffers() {
        try {
            List<OfferDto> offerDtoListResponse = this.offerService.getAllOffers();
            return new ResponseDto<>(false, offerDtoListResponse);
        } catch (ServiceException e) {
            log.error(e.getMessage());
            return new ResponseDto<>(true, new ErrorDto(e.getCode(), e.getMessage()));
        }
    }

    @Override
    @ResponseBody
    @RequestMapping(value = "limited", method = RequestMethod.GET)
    public ResponseDto getSomeOffers() {
        try {
            List<OfferDto> offerDtoListResponse = this.offerService.getSomeOffers();
            return new ResponseDto<>(false, offerDtoListResponse);
        } catch (ServiceException e) {
            log.error(e.getMessage());
            return new ResponseDto<>(true, new ErrorDto(e.getCode(), e.getMessage()));
        }
    }

    @Override
    @ResponseBody
    @PreAuthorize("hasAnyRole('ADMIN', 'BUSINESS', 'CUSTOMER')")
    @RequestMapping(value = "{id}", method = RequestMethod.GET)
    public ResponseDto getOfferById(@PathVariable Long id) {
        try {
            OfferDto offerDtoResponse = this.offerService.getOfferById(id);
            return new ResponseDto<>(false, offerDtoResponse);
        } catch (ServiceException e) {
            log.error(e.getMessage());
            return new ResponseDto<>(true, new ErrorDto(e.getCode(), e.getMessage()));
        }
    }

    @Override
    @ResponseBody
    @PreAuthorize("hasAnyRole('ADMIN', 'BUSINESS', 'CUSTOMER')")
    @RequestMapping(value = "/timeslot/{id}", method = RequestMethod.GET)
    public ResponseDto getOffersByTimeSlotIds(@PathVariable Long[] id) {
        try {
            List<OfferDto> offerDtoResponse = this.offerService.getOffersByTimeSlotIds(id);
            return new ResponseDto<>(false, offerDtoResponse);
        } catch (ServiceException e) {
            log.error(e.getMessage());
            return new ResponseDto<>(true, new ErrorDto(e.getCode(), e.getMessage()));
        }
    }

    @Override
    @ResponseBody
    @PreAuthorize("hasAnyRole('ADMIN', 'BUSINESS')")
    @RequestMapping(value = "", method = RequestMethod.POST)
    public ResponseDto saveOffer(@RequestBody OfferDto offerDto) {
        try {
            OfferDto offerDtoResponse = this.offerService.saveOffer(offerDto);
            System.out.println("saveOfferController");
            return new ResponseDto<>(false, offerDtoResponse);
        } catch (ServiceException e) {
            log.error(e.getMessage());
            System.out.println("saveOfferControllerError");
            return new ResponseDto<>(true, new ErrorDto(e.getCode(), e.getMessage()));
        }
    }

    @Override
    @ResponseBody
    @PreAuthorize("hasAnyRole('ADMIN', 'BUSINESS')")
    @RequestMapping(value = "{id}", method = RequestMethod.DELETE)
    public void deleteOfferById(@PathVariable Long id) {
        try {
            this.offerService.deleteOfferById(id);
        } catch (ServiceException e) {
            log.error(e.getMessage());
        }
    }

    @Override
    @ResponseBody
    @PreAuthorize("hasAnyRole('ADMIN', 'BUSINESS')")
    @RequestMapping(value = "{id}", method = RequestMethod.PUT)
    public ResponseDto updateOffer(@PathVariable Long id, @RequestBody OfferDto offerDto) {
        try {
            OfferDto offerDtoResponse = this.offerService.updateOffer(id, offerDto);
            return new ResponseDto<>(false, offerDtoResponse);
        } catch(ServiceException e) {
            log.error(e.getMessage());
            return new ResponseDto<>(true, new ErrorDto(e.getCode(), e.getMessage()));
        }
    }
}
