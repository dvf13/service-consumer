# README #

## Preconditions ##
### Install ###
- npm/node
- docker
- intellij lombok plugin (only to make intellij recognize the autocompiled code; this makes writing model classes great again by automatically implicating getter and setter through @Data annotation, and also handles the logger with @Slf4j annotation)
- gradle (might be possible to use gradle wrappers?)

## Run ##

### Configuration ###
All properties for configuration are in `application.properties file `(/backend/src/main/resources)`

### Before first run ###
- execute `gradle build`

### Start project ###
- Start Elasticsearch
    - Extract archive /utils/elk/elasticsearch-2.3.5.tar.gz to your desired location
    - Open a terminal
    - Change to the elasticsearch-2.3.5/bin folder you just extracted
    - ./elasticsearch
- Enable SSL and start Kibana
    - Extract archive /utils/elk/kibana-4.5.4-linux-x64.tar.gz to your desired location
    - Open the file "kibana-4.5.4-linux-x64/config/kibana.yml"
    - Go to lines 36-37 and change the path to the certificates (no relative paths accepted!!!) and save
    - Open a terminal
    - Change to the kibana-4.5.4-linux-x64/bin folder you just extracted
    - ./kibana
- Run `startEnv` task with gradle (beware, can take longer the first time)
- Run `bootRun` gradle task of backend

This should boot up mysql, elk and backend with served frontend. If you don't have it already, docker will create a `/data` directory (excluded from git) to keep states persisted during container restarts.

### Stop ###
- Stop java tasks for `backend `

and

- Run `stopEnv` gradle task

### Ports ###
- Backend/Frontend: `8080`
- Frontend Development Server: `4200`
- others lookup in `docker-compose-env.yml`


## Frontend ##
The frontend corresponding to backend server is in `/backend/web`

### Develop ###
NPM scripts for developing frontend are defined in `/backend/web/package.json`

For easy use of those scripts with intellij just right-click on the file `package.json` and select `Show npm Scripts`. Otherwise you have to cd into the folder where the package.json resides.

- Use `npm run start` or `npm run dev` to run frontend development server on port `4200`. One can use this additionally to starting the backend to serve and watch frontend files.
- Use `npm run test` to run unit and e2e tests

### CSS/SASS ###
We use SASS for frontend styling (just look at the App.vue for an example).

If you have properties like colors, button sizes etc. you can configure variables for it in one the global sass files in `/global_styles` or create a new one.


## DB ##

### Adminer ###
The `startEnv` gradle task also starts a frontend GUI for the mysql database on port `9090`.
Password and username are `root`, no DB has to be given.