package backend.auth;

import lombok.Data;

@Data
public class JwtAuthenticationRequest {

    private String username;
    private String password;


    private JwtAuthenticationRequest() { }

    public JwtAuthenticationRequest(String username, String password) {
        this.username = username;
        this.password = password;
    }
}