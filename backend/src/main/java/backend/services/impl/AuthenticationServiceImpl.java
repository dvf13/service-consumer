package backend.services.impl;

import backend.auth.JwtAuthenticationRequest;
import backend.auth.TokenStore;
import backend.auth.TokenUtils;
import backend.dtos.BusinessDto;
import backend.dtos.CustomerDto;
import backend.dtos.MessageDto;
import backend.dtos.UserInfoDto;
import backend.dtos.UserTokenStateDto;
import backend.exceptions.ServiceException;
import backend.services.AuthenticationService;
import backend.services.BusinessService;
import backend.services.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mobile.device.Device;
import org.springframework.mobile.device.DeviceUtils;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.servletapi.SecurityContextHolderAwareRequestWrapper;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.security.Principal;
import java.util.List;

@Service
public class AuthenticationServiceImpl implements AuthenticationService {

    private AuthenticationManager authenticationManager;

    private TokenUtils tokenUtils;

    private BusinessService businessService;

    private CustomerService customerService;


    @Autowired
    public AuthenticationServiceImpl(AuthenticationManager authenticationManager, TokenUtils tokenUtils, BusinessService businessService, CustomerService customerService) {
        this.authenticationManager = authenticationManager;
        this.tokenUtils = tokenUtils;
        this.businessService = businessService;
        this.customerService = customerService;
    }

    @Override
    public UserTokenStateDto createAuthenticationToken(JwtAuthenticationRequest authenticationRequest, Device device) throws ServiceException {
        try {
            final Authentication authentication = this.authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(authenticationRequest.getUsername(), authenticationRequest.getPassword()));

            SecurityContextHolder.getContext().setAuthentication(authentication);

            UserDetails userDetails = (UserDetails) authentication.getPrincipal();

            String authToken = this.tokenUtils.generateToken(userDetails.getUsername(), device);

            TokenStore.addToken(authToken);

            int expiresIn = this.tokenUtils.getExpiredIn(device);

            return new UserTokenStateDto(authToken, expiresIn);
        } catch (AuthenticationException e) {
            throw new ServiceException(1070, "Invalid login credentials");
        }
    }

    @Override
    @PreAuthorize("hasAnyRole('ADMIN', 'BUSINESS', 'CUSTOMER')")
    public MessageDto logout(HttpServletRequest request) throws ServiceException {
        String authToken = this.tokenUtils.getToken(request);

        if(authToken != null && TokenStore.removeToken(authToken)) {
            return new MessageDto("Successfully logged out");
        } else {
            throw new ServiceException(1071, "Invalid or no authentication token");
        }
    }

    @Override
    @PreAuthorize("hasAnyRole('ADMIN', 'BUSINESS', 'CUSTOMER')")
    public UserTokenStateDto refresh(HttpServletRequest request, Principal principal) throws ServiceException {
        String authToken = this.tokenUtils.getToken(request);

        Device device = DeviceUtils.getCurrentDevice(request);

        if (authToken != null && principal != null) {

            // TODO check user password last update
            String refreshedToken = this.tokenUtils.refreshToken(authToken, device);
            int expiresIn = this.tokenUtils.getExpiredIn(device);

            return new UserTokenStateDto(refreshedToken, expiresIn);
        } else {
            throw new ServiceException(1071, "Invalid or no authentication token");
        }
    }

    @Override
    @PreAuthorize("hasAnyRole('ADMIN', 'BUSINESS', 'CUSTOMER')")
    public UserInfoDto getUserInfo(Principal principal) throws ServiceException {
        if(principal == null) {
            throw new ServiceException(1072, "No authenticated user");
        }

        List<BusinessDto> businessDtoList = null;
        List<CustomerDto> customerDtoList = null;

        try {
            businessDtoList = this.businessService.getBusinessesByUsername(principal.getName());
        } catch (ServiceException ignore) { }

        try {
            customerDtoList = this.customerService.getCustomersByUsername(principal.getName());
        } catch (ServiceException ignore) { }

        UserInfoDto userInfoDto;

        if(businessDtoList != null && businessDtoList.size() > 0) {
            userInfoDto = new UserInfoDto(businessDtoList.get(0).getId(), businessDtoList.get(0).getUsername(), UserInfoDto.UserType.BUSINESS);
        } else if(customerDtoList != null && customerDtoList.size() > 0) {
            userInfoDto = new UserInfoDto( customerDtoList.get(0).getId(), customerDtoList.get(0).getUsername(), UserInfoDto.UserType.CUSTOMER);
        } else {
            throw new ServiceException(1072, "No authenticated user");
        }

        if(this.hasRole("ROLE_ADMIN")) {
            userInfoDto.setUserType(UserInfoDto.UserType.ADMIN);
        }
        return userInfoDto;
    }

    private boolean hasRole(String role) {
        SecurityContext securityContext = SecurityContextHolder.getContext();

        if(securityContext == null) {
            return false;
        }
        Authentication authentication = securityContext.getAuthentication();

        if(authentication == null)
            return false;

        for(GrantedAuthority grantedAuthority : authentication.getAuthorities()) {
            if (role.equals(grantedAuthority.getAuthority())) {
                return true;
            }
        }
        return false;
    }
}