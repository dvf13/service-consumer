package backend.services;

import backend.auth.JwtAuthenticationRequest;
import backend.dtos.MessageDto;
import backend.dtos.UserInfoDto;
import backend.dtos.UserTokenStateDto;
import backend.exceptions.ServiceException;
import org.springframework.mobile.device.Device;

import javax.servlet.http.HttpServletRequest;
import java.security.Principal;

public interface AuthenticationService {

    UserTokenStateDto createAuthenticationToken(JwtAuthenticationRequest authenticationRequest, Device device) throws ServiceException;

    MessageDto logout(HttpServletRequest request) throws ServiceException;

    UserTokenStateDto refresh(HttpServletRequest request, Principal principal) throws ServiceException;

    UserInfoDto getUserInfo(Principal principal) throws ServiceException;
}
