package backend.integrationTests.util;

import backend.dtos.*;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;

public class IntegTestsUtil {

    private static final String AUTH_HEADER = "Authorization";
    private static final String AUTH_PREFIX = "Bearer ";
    private static String authToken = "";

    public static HttpHeaders getAuthHeader() {
        HttpHeaders headers = new HttpHeaders();
        headers.set(AUTH_HEADER, AUTH_PREFIX+authToken);
        headers.set("Content-Type", "application/json");
        return headers;
    }

    public static boolean login(String username, String password, TestRestTemplate testRestTemplate) {
        HttpHeaders headers = new HttpHeaders();
        headers.set("Content-Type", "application/json");

        String loginData = "{ \"username\": \""+username+"\", \"password\": \""+password+"\" }";

        HttpEntity<String> entity = new HttpEntity<>(loginData, headers);

        ResponseEntity<ResponseDto> responseEntity = testRestTemplate.exchange("/api/auth/login/", HttpMethod.POST, entity, ResponseDto.class);
        ResponseDto responseDto = responseEntity.getBody();

        if(responseDto.isError()) {
            return false;
        } else {
            JSONObject data = convertLinkedHashMapToJsonObject((LinkedHashMap) responseDto.getData());
            try {
                authToken = (String) data.get("access_token");
            } catch (JSONException e) {
                e.printStackTrace();
                return false;
            }
        }
        return true;
    }

    public static boolean logout(TestRestTemplate testRestTemplate) {
        HttpHeaders authHeader = IntegTestsUtil.getAuthHeader();

        HttpEntity entity = new HttpEntity(authHeader);

        ResponseEntity<ResponseDto> responseEntity = testRestTemplate.exchange("/api/auth/logout/", HttpMethod.POST, entity, ResponseDto.class);
        ResponseDto responseDto = responseEntity.getBody();

        if(responseDto.isError()) {
            return false;
        } else {
            authToken = "";
        }
        return true;
    }

    public static ArrayList<JSONObject> convertArrayListOfLinkedHashMapsToArrayListOfJsonObjects(ArrayList<LinkedHashMap> linkedHashMaps) {
        ArrayList<JSONObject> jsonObjects = new ArrayList<>();

        for(LinkedHashMap linkedHashMap: linkedHashMaps) {
            jsonObjects.add(convertLinkedHashMapToJsonObject(linkedHashMap));
        }

        return jsonObjects;
    }

    public static JSONObject convertLinkedHashMapToJsonObject(LinkedHashMap linkedHashMap) {
        return new JSONObject(linkedHashMap);
    }

    public static BusinessDto convertJsonObjectToBusinessDto(JSONObject jsonObject) throws JSONException {

        BusinessDto businessDto = new BusinessDto();
        if (jsonObject != null) {
            businessDto.setId(jsonObject.getLong("id"));
            businessDto.setUsername(jsonObject.getString("username"));
            businessDto.setPassword(jsonObject.getString("password"));
            businessDto.setEmail(jsonObject.getString("email"));
            businessDto.setName(jsonObject.getString("name"));
            businessDto.setStreet(jsonObject.getString("street"));
            businessDto.setStreetNumber(jsonObject.getString("streetNumber"));
            businessDto.setZipCode(jsonObject.getInt("zipCode"));
            businessDto.setCity(jsonObject.getString("city"));
        }
        return businessDto;
    }

    public static CategoryDto convertJsonObjectToCategoryDto(JSONObject jsonObject) throws JSONException {

        CategoryDto categoryDto = new CategoryDto();
        if (jsonObject != null) {
            categoryDto.setId(jsonObject.getLong("id"));
            categoryDto.setName(jsonObject.getString("name"));
        }
        return categoryDto;
    }

    public static OfferDto convertJsonObjectToOfferDto(JSONObject jsonObject) throws JSONException {

        OfferDto offerDto = new OfferDto();
        if (jsonObject != null) {
            offerDto.setId(jsonObject.getLong("id"));
            offerDto.setTitle(jsonObject.getString("title"));
            offerDto.setDescription(jsonObject.getString("description"));
            offerDto.setPictureUrl(jsonObject.getString("pictureUrl"));
            offerDto.setPrice(jsonObject.getInt("price"));
            offerDto.setDiscount(jsonObject.getDouble("discount"));
            offerDto.setAmount(jsonObject.getInt("amount"));
        }
        return offerDto;
    }

    public static CustomerDto convertJsonObjectToCustomerDto(JSONObject jsonObject) throws JSONException, ParseException {

        CustomerDto customerDto = new CustomerDto();
        if (jsonObject != null) {
            customerDto.setId(jsonObject.getLong("id"));
            customerDto.setUsername(jsonObject.getString("username"));
            customerDto.setFirstName(jsonObject.getString("firstName"));
            customerDto.setLastName(jsonObject.getString("lastName"));
            customerDto.setEmail(jsonObject.getString("email"));
            customerDto.setBirthdate(parseDate(String.valueOf(jsonObject.get("birthdate"))));
            customerDto.setPassword(jsonObject.getString("password"));
            customerDto.setGender(jsonObject.getString("gender").toCharArray()[0]);
        }
        return customerDto;
    }

    public static TimeSlotDto convertJsonObjectToTimeSlotDto(JSONObject jsonObject) throws JSONException, ParseException {
        System.out.println(jsonObject);
        TimeSlotDto timeSlotDto = new TimeSlotDto();
        if (jsonObject != null) {
            timeSlotDto.setId(jsonObject.getLong("id"));
            timeSlotDto.setBeg_date_time(parseDate(String.valueOf(jsonObject.get("beg_date_time"))));
            timeSlotDto.setEnd_date_time(parseDate(String.valueOf(jsonObject.get("end_date_time"))));
            timeSlotDto.setQuantity_booked(jsonObject.getInt("quantity_booked"));
            timeSlotDto.setQuantity_total(jsonObject.getInt("quantity_total"));
            //timeSlotDto.setOfferDto(convertJsonObjectToOfferDto(jsonObject.getJSONObject("offer")));
        }
        return timeSlotDto;
    }

    public static ArrayList<TimeSlotDto> convertArrayListOfJsonObjectsToArrayListOfTimeSlotDto(ArrayList<JSONObject> jsonObjects) throws ParseException {
        ArrayList<TimeSlotDto> timeSlotDtos = new ArrayList<>();

        for(JSONObject jsonObject: jsonObjects) {
            timeSlotDtos.add(convertJsonObjectToTimeSlotDto(jsonObject));
        }

        return timeSlotDtos;
    }

    /**
     * Converts milliseconds in Date object
     *
     * @param dateString - date in milliseconds
     * @return
     */
    private static Date parseDate(String dateString) throws ParseException {
        Date date = new Date(Long.parseLong(dateString));
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.parse(sdf.format(date));
    }
}
