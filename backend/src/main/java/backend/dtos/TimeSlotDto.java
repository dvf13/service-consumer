package backend.dtos;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

@Data
@JsonRootName("timeslot")
public class TimeSlotDto {

    private Long id;

    @JsonProperty("offer")
    @JsonBackReference
    private OfferDto offerDto;

    private Date beg_date_time;

    private Date end_date_time;

    private int quantity_total;

    private int quantity_booked;

    @Override
    public String toString() {
        return "TimeSlotDto{" +
                "id=" + id +
                ", offerDto=" + offerDto.getId() +
                ", beg_date_time=" + beg_date_time +
                ", end_date_time=" + end_date_time +
                ", quantity_total=" + quantity_total +
                ", quantity_booked=" + quantity_booked +
                '}';
    }
}
