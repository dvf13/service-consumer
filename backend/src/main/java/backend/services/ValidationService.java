package backend.services;

public interface ValidationService {

    boolean isUniqueUsername(Long userId, String username);

    boolean isValidEmail(String email);

    boolean isUniqueEmail(Long userId, String email);
}
