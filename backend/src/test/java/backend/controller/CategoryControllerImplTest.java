package backend.controller;

import backend.controller.impl.CategoryControllerImpl;
import backend.dtos.CategoryDto;
import backend.dtos.ErrorDto;
import backend.dtos.ResponseDto;
import backend.exceptions.ServiceException;
import backend.services.CategoryService;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.instanceOf;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

public class CategoryControllerImplTest {

    private CategoryService categoryService;
    private CategoryController categoryController;

    public CategoryControllerImplTest() {
        this.categoryService = mock(CategoryService.class);
        this.categoryController = new CategoryControllerImpl(categoryService);
    }

    @Test
    public void getAllCategoriesTest_NOError() throws ServiceException {
        when(this.categoryService.getAllCategories()).thenReturn(getCategoryDtoList());

        ResponseDto responseDto = this.categoryController.getAllCategories();

        assertFalse(responseDto.isError());
        assertNotNull(responseDto);
        assertNotNull(responseDto.getData());
        assertThat(responseDto.getData(), instanceOf(ArrayList.class));
        assertEquals(getCategoryDtoList(), responseDto.getData());
    }

    @Test
    public void getAllCategoriesTest_Error() throws ServiceException {
        when(this.categoryService.getAllCategories()).thenThrow(ServiceException.class);

        ResponseDto responseDto = this.categoryController.getAllCategories();

        assertTrue(responseDto.isError());
        assertNotNull(responseDto);
        assertNotNull(responseDto.getData());
        assertThat(responseDto.getData(), instanceOf(ErrorDto.class));
    }

    @Test
    public void getCategoryByIdTest_NOError() throws ServiceException {
        when(this.categoryService.getCategoryById(Mockito.anyLong())).thenReturn(getCategoryDto());

        ResponseDto responseDto = this.categoryController.getCategoryById(1L);

        assertFalse(responseDto.isError());
        assertNotNull(responseDto);
        assertNotNull(responseDto.getData());
        assertThat(responseDto.getData(), instanceOf(CategoryDto.class));
        assertEquals(getCategoryDto(), responseDto.getData());
    }

    public void getCategoryByIdTest_Error() throws ServiceException {
        when(this.categoryService.getCategoryById(Mockito.anyLong())).thenThrow(ServiceException.class);

        ResponseDto responseDto = this.categoryController.getCategoryById(-1L);

        assertTrue(responseDto.isError());
        assertNotNull(responseDto);
        assertNotNull(responseDto.getData());
        assertThat(responseDto.getData(), instanceOf(ErrorDto.class));
    }

    public void saveCategoryTest_NOError() throws ServiceException {
        when(this.categoryService.saveCategory(any(CategoryDto.class))).thenReturn(getCategoryDto());

        ResponseDto responseDto = this.categoryController.saveCategory(getCategoryDto());

        assertFalse(responseDto.isError());
        assertNotNull(responseDto);
        assertNotNull(responseDto.getData());
        assertThat(responseDto.getData(), instanceOf(CategoryDto.class));
        assertEquals(getCategoryDto(), responseDto.getData());
    }

    public void saveCategoryTest_Error() throws ServiceException {
        when(this.categoryService.saveCategory(any(CategoryDto.class))).thenThrow(ServiceException.class);

        ResponseDto responseDto = this.categoryController.saveCategory(null);

        assertTrue(responseDto.isError());
        assertNotNull(responseDto);
        assertNotNull(responseDto.getData());
        assertThat(responseDto.getData(), instanceOf(ErrorDto.class));
    }

    @Test
    public void updateCategoryTest_NOError() throws ServiceException {
        when(this.categoryService.updateCategory(
                Mockito.anyLong(), any(CategoryDto.class)))
                .thenReturn(getCategoryDto());

        ResponseDto responseDto = this.categoryController.updateCategory(1L, getCategoryDto());

        assertFalse(responseDto.isError());
        assertNotNull(responseDto);
        assertNotNull(responseDto.getData());
        assertThat(responseDto.getData(), instanceOf(CategoryDto.class));
        assertEquals(getCategoryDto(), responseDto.getData());
    }

    @Test
    public void updateCategoryTest_Error() throws ServiceException {
        when(this.categoryService.updateCategory(
                Mockito.anyLong(), any(CategoryDto.class)))
                .thenThrow(ServiceException.class);

        ResponseDto responseDto = this.categoryController.updateCategory(1L, null);

        assertTrue(responseDto.isError());
        assertNotNull(responseDto);
        assertNotNull(responseDto.getData());
        assertThat(responseDto.getData(), instanceOf(ErrorDto.class));
    }

    @Test
    public void deleteCategoryByIdTest_Error() throws ServiceException {
        doThrow(ServiceException.class).when(this.categoryService).deleteCategoryById(Mockito.anyLong());

        this.categoryController.deleteCategoryById(-1L);
    }

    private CategoryDto getCategoryDto() {
        CategoryDto categoryDto = new CategoryDto();
        categoryDto.setId(1L);
        categoryDto.setName("Test_Category");
        return categoryDto;
    }

    private List<CategoryDto> getCategoryDtoList() {
        List<CategoryDto> categoryDtoArrayList = new ArrayList<>();
        categoryDtoArrayList.add(getCategoryDto());
        return categoryDtoArrayList;
    }
}
