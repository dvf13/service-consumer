package backend;

import backend.exceptions.ServiceException;
import backend.services.AdvancedFeaturesService;
import backend.services.impl.AdvancedFeaturesServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.annotation.ComponentScan;

import javax.annotation.PostConstruct;

@Slf4j
@SpringBootApplication
@ComponentScan
public class Application {

    @Value("${server.port}")
    private int port;

    public static void main(String[] args) {
        new SpringApplicationBuilder().sources(Application.class).run(args);
    }

    @PostConstruct
    public void init() {
        log.info("#### Backend/Frontend started on port " + port + " ####");
    }

}