package backend.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.UpdateTimestamp;
import org.hibernate.validator.constraints.NotBlank;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * Offer model for jpa
 */
@Data
@Entity(name = "offer")
public class Offer implements Serializable {

    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    // TODO: @NotNull enable again after implementation
    @ManyToOne
    @JoinColumn(name = "business_id")
    private Business business;

    // TODO: @NotNull enable again after implementation
    @ManyToOne
    @JoinColumn(name = "category_id")
    private Category category;

    @Column
    @NotBlank
    private String title;

    @Column
    @NotBlank
    private String description;

    @Column
    private String pictureUrl;

    @Column(columnDefinition="mediumblob")
    private String picture;

    @Column
    private double latitude;

    @Column
    private double longitude;

    @Column
    private int price;

    @Column
    @NotNull
    private Double discount;

    @Column
    private int amount;

    @Column
    @NotNull
    private Date validUntil;

    @Column(nullable = false, updatable = false)
    @Temporal(TemporalType.TIMESTAMP)
    @CreationTimestamp
    private Date createdAt;

    @Column(nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    @UpdateTimestamp
    private Date updatedAt;

    @OneToMany(mappedBy="offer", cascade=CascadeType.ALL, fetch = FetchType.LAZY)
    @Column(nullable=true)
    @JsonManagedReference
    private List<TimeSlot> timeSlotList;
}