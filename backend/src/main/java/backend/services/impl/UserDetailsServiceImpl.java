package backend.services.impl;

import backend.models.Business;
import backend.models.Customer;
import backend.repositories.BusinessRepository;
import backend.repositories.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    private CustomerRepository customerRepository;

    private BusinessRepository businessRepository;


    @Autowired
    public UserDetailsServiceImpl(CustomerRepository customerRepository, BusinessRepository businessRepository) {
        this.customerRepository = customerRepository;
        this.businessRepository = businessRepository;
    }


    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        List<Customer> customerList = this.customerRepository.findCustomersByUsername(username);
        List<Business> businessList = this.businessRepository.findBusinessesByUsername(username);

        if((customerList == null || customerList.size() == 0) && (businessList == null || businessList.size() == 0)) {
            throw new UsernameNotFoundException("Username does not exist");
        }

        if(customerList != null && customerList.size() != 0) {
            return customerList.get(0);
        } else {
            return businessList.get(0);
        }
    }
}