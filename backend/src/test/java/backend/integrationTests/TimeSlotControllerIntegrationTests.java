package backend.integrationTests;

import backend.dtos.*;
import backend.integrationTests.util.SSLUtil;
import backend.integrationTests.util.IntegTestsUtil;
import backend.models.Offer;
import org.json.JSONObject;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
@ActiveProfiles("test")
@Sql(executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD, scripts="/../resources/data.sql")
public class TimeSlotControllerIntegrationTests {

    private static final String ADMIN_NAME = "user3";
    private static final String ADMIN_PASS = "123";
    private static final String AUTHORIZED_CUSTOMER_NAME = "user1";
    private static final String AUTHORIZED_CUSTOMER_PASS = "12345";

    @Autowired
    private TestRestTemplate testRestTemplate;

    static {
        javax.net.ssl.HttpsURLConnection.setDefaultHostnameVerifier(
                (hostname, sslSession) -> hostname.equals("localhost"));
    }

    @Before
    public void turnOffSslChecking() throws Exception {
        SSLUtil.deactivateSslChecking();
    }

    @After
    public void turnOnSslChecking() throws Exception {
        SSLUtil.activateSslChecking();
    }

    @Test
    public void getAllTimeSlots() throws Exception{
        assertTrue(IntegTestsUtil.login(AUTHORIZED_CUSTOMER_NAME, AUTHORIZED_CUSTOMER_PASS, this.testRestTemplate));
        HttpHeaders authHeader = IntegTestsUtil.getAuthHeader();
        HttpEntity<TimeSlotControllerIntegrationTests> entity = new HttpEntity<>(null, authHeader);

        ResponseEntity<ResponseDto> responseEntity = this.testRestTemplate.exchange("/api/timeslot/", HttpMethod.GET, entity, ResponseDto.class);
        ResponseDto responseDto = responseEntity.getBody();

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertFalse(responseDto.isError());

        ArrayList<JSONObject> result = IntegTestsUtil.convertArrayListOfLinkedHashMapsToArrayListOfJsonObjects((ArrayList<LinkedHashMap>) responseDto.getData());
        assertEquals(2, result.size());

        // verifications for timeslot 1
        assertEquals("1", String.valueOf(result.get(0).getLong("id")));
        assertEquals(0, result.get(0).getInt("quantity_booked"));
        assertEquals(5, result.get(0).getInt("quantity_total"));

        // verifications for timeslot 2
        assertEquals("2", String.valueOf(result.get(1).getLong("id")));
        assertEquals(2, result.get(1).getInt("quantity_booked"));
        assertEquals(5, result.get(1).getInt("quantity_total"));

        assertTrue(IntegTestsUtil.logout(this.testRestTemplate));
    }

    @Test
    public void getTimeSlotsByOfferId() throws Exception{
        assertTrue(IntegTestsUtil.login(AUTHORIZED_CUSTOMER_NAME, AUTHORIZED_CUSTOMER_PASS, this.testRestTemplate));

        HttpHeaders authHeader = IntegTestsUtil.getAuthHeader();
        HttpEntity<TimeSlotControllerIntegrationTests> entity = new HttpEntity<>(null, authHeader);

        ResponseEntity<ResponseDto> responseEntity = this.testRestTemplate.exchange("/api/timeslot/1", HttpMethod.GET, entity, ResponseDto.class);
        ResponseDto responseDto = responseEntity.getBody();

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertFalse(responseDto.isError());

        ArrayList<JSONObject> result = IntegTestsUtil.convertArrayListOfLinkedHashMapsToArrayListOfJsonObjects((ArrayList<LinkedHashMap>) responseDto.getData());
        assertEquals(2, result.size());

        assertEquals("1", String.valueOf(result.get(0).getLong("id")));
        assertEquals("2", String.valueOf(result.get(1).getLong("id")));

        assertTrue(IntegTestsUtil.logout(this.testRestTemplate));
    }

    @Test
    public void saveTimeSlot() throws Exception{
        assertTrue(IntegTestsUtil.login(AUTHORIZED_CUSTOMER_NAME, AUTHORIZED_CUSTOMER_PASS, this.testRestTemplate));

        HttpHeaders authHeader = IntegTestsUtil.getAuthHeader();

        // offer
        HttpEntity<OfferControllerIntegrationTest> offerEntity = new HttpEntity<>(null, authHeader);
        ResponseEntity<ResponseDto> rEntity = this.testRestTemplate.exchange("/api/offer/1", HttpMethod.GET, offerEntity, ResponseDto.class);
        ResponseDto rDto = rEntity.getBody();
        JSONObject offerJson = IntegTestsUtil.convertLinkedHashMapToJsonObject((LinkedHashMap) rDto.getData());
        OfferDto offerDto = IntegTestsUtil.convertJsonObjectToOfferDto(offerJson);

        TimeSlotDto timeSlotDto = new TimeSlotDto();
        timeSlotDto.setEnd_date_time(new Date(2019,1,1));
        timeSlotDto.setBeg_date_time(new Date(2018,1,1));
        timeSlotDto.setQuantity_total(5);
        timeSlotDto.setQuantity_booked(1);
        timeSlotDto.setOfferDto(offerDto);

        HttpEntity<TimeSlotDto> entity = new HttpEntity<>(timeSlotDto, authHeader);

        ResponseEntity<ResponseDto> responseEntity = this.testRestTemplate.exchange("/api/timeslot/", HttpMethod.POST, entity, ResponseDto.class);
        ResponseDto responseDto = responseEntity.getBody();
        String id = responseDto.getData().toString().split(",")[0].split("=")[1];

        HttpEntity<TimeSlotControllerIntegrationTests> entity1 = new HttpEntity<>(null, authHeader);

        ResponseEntity<ResponseDto> responseEntity1 = this.testRestTemplate.exchange("/api/timeslot/" + id, HttpMethod.GET, entity1, ResponseDto.class);
        ResponseDto responseDto1 = responseEntity1.getBody();

        // checking status codes
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertFalse(responseDto.isError());

        assertEquals(HttpStatus.OK, responseEntity1.getStatusCode());
        assertFalse(responseDto1.isError());
        assertNotNull(responseDto1.getData());

        assertTrue(IntegTestsUtil.logout(this.testRestTemplate));
    }

}
