package backend.services;

import backend.dtos.OfferDto;
import backend.exceptions.ServiceException;
import backend.models.Business;
import backend.models.Category;
import backend.models.Offer;
import backend.repositories.OfferRepository;
import backend.services.impl.OfferServiceImpl;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.*;

public class OfferServiceImplTest {

    private OfferRepository offerRepository;

    private OfferService offerService;

    private BusinessService businessService;

    private CategoryService categoryService;

    private TimeSlotService timeSlotService;

    public OfferServiceImplTest() {
        this.offerRepository = mock(OfferRepository.class);
        this.businessService = mock(BusinessService.class);
        this.categoryService = mock(CategoryService.class);
        this.timeSlotService = mock(TimeSlotService.class);
        this.offerService = new OfferServiceImpl(this.offerRepository, this.businessService, this.categoryService, this.timeSlotService);
    }

    @Test
    public void getAllOffersTest() throws ServiceException {
        when(this.offerRepository.findAll()).thenReturn(this.getOfferList());

        List<OfferDto> offerDtoList = this.offerService.getAllOffers();

        assertNotNull(offerDtoList);
        assertEquals(1, offerDtoList.size());
        assertEquals((Long) 1L, offerDtoList.get(0).getId());
    }

    @Test (expected = ServiceException.class)
    public void getAllOffersTest_NoOffers() throws ServiceException {
        when(this.offerRepository.findAll()).thenReturn(null);

        List<OfferDto> offerDtoList = this.offerService.getAllOffers();
    }

    @Test
    public void getOfferByIdTest_positive() throws ServiceException {
        when(this.offerRepository.findOne(Mockito.anyLong())).thenReturn(this.getOffer());

        OfferDto offerDto = this.offerService.getOfferById(1L);

        assertNotNull(offerDto);;
    }

    @Test (expected = ServiceException.class)
    public void getOfferByIdTest_InvalidId() throws ServiceException {
        when(this.offerRepository.findAll()).thenReturn(this.getOfferList());

        OfferDto offerDto = this.offerService.getOfferById(2L);
    }

    @Test (expected = ServiceException.class)
    public void saveOffer_invalidOffer() throws ServiceException {
        when(this.offerRepository.save(any(Offer.class))).thenReturn(this.getOffer());

        Offer offer = this.getOffer();

        OfferDto offerDto = this.offerService.saveOffer(this.offerService.convertOfferToOfferDto(offer));
    }

    @Test (expected = ServiceException.class)
    public void updateOfferTest_invalidOfferId() throws ServiceException {
        Offer offer = this.getOffer();

        OfferDto offerDto = this.offerService.updateOffer(2L, offerService.convertOfferToOfferDto(offer));
    }

    @Test (expected = ServiceException.class)
    public void updateOfferTest_invalidOffer() throws ServiceException {
        Offer offer = this.getOffer();
        offer.setAmount(20);

        OfferDto offerDto = this.offerService.updateOffer(1L, offerService.convertOfferToOfferDto(offer));
    }

    @Test
    public void deleteOfferTest_positive() throws ServiceException {
        this.offerService.deleteOfferById(1L);
    }

    @Test (expected = ServiceException.class)
    public void deleteOfferTest_invalidId() throws ServiceException {
        this.offerService.deleteOfferById(null);
    }

    private Business getBusiness() {
        Business business = new Business();
        business.setId(1L);
        business.setName("Test_Business_User");
        return business;
    }

    private Category getCategory() {
        Category category = new Category();
        category.setId(1L);
        category.setName("Test_Category");
        return category;
    }

    private Offer getOffer() {
        Offer offer = new Offer();
        offer.setId(1L);
        offer.setAmount(10);
        offer.setBusiness(getBusiness());
        offer.setCategory(getCategory());
        return offer;
    }

    private List<Offer> getOfferList() {
        List<Offer> offerList = new ArrayList<>();
        offerList.add(getOffer());
        return offerList;
    }

}
