package backend.services.impl;


import backend.dtos.OfferDto;
import backend.dtos.TimeSlotDto;
import backend.exceptions.ServiceException;
import backend.models.Offer;
import backend.models.TimeSlot;
import backend.repositories.TimeSlotRepository;
import backend.services.BusinessService;
import backend.services.CategoryService;
import backend.services.OfferService;
import backend.services.TimeSlotService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class TimeSlotServiceImpl implements TimeSlotService {

    private TimeSlotRepository timeSlotRepository;

    private BusinessService businessService;

    private CategoryService categoryService;

    private OfferService offerService;


    @Autowired
    public TimeSlotServiceImpl(TimeSlotRepository timeSlotRepository, BusinessService businessService, CategoryService categoryService, @Lazy OfferService offerService) {
        this.timeSlotRepository = timeSlotRepository;
        this.businessService = businessService;
        this.categoryService = categoryService;
        this.offerService = offerService;
    }


    @Override
    public List<TimeSlotDto> getAllTimeSlots() throws ServiceException {
        List<TimeSlot> timeSlotList = this.timeSlotRepository.findAll();

        if(timeSlotList == null || timeSlotList.isEmpty()) {
            throw new ServiceException(1043, "Timeslot doesn’t exist");
        }
        return this.convertTimeSlotDaoListToTimeSlotDtoList(null, timeSlotList);
    }

    @Override
    public TimeSlotDto getTimeSlotById(Long id) throws ServiceException {
        if(id == null || id <= 0) {
            throw new ServiceException(1040, "invalid timeslot id");
        }
        TimeSlot timeSlot = this.timeSlotRepository.findOne(id);

        if(timeSlot == null) {
            throw new ServiceException(1043, "offer doesn’t exist");
        }
        return this.convertTimeSlotToTimeSlotDto(timeSlot);
    }


    @Override
    public List<TimeSlotDto> getTimeSlotByOffer(Long offer) throws ServiceException {
        if(offer == null || offer <= 0) {
            throw new ServiceException(1040, "invalid offer id");
        }
        List<TimeSlot> timeSlot = this.timeSlotRepository.findByOfferID(offer);

        if(timeSlot == null) {
            throw new ServiceException(1043, "timeslot by offer id doesn’t exist");
        }

        return this.convertTimeSlotDaoListToTimeSlotDtoList(null, timeSlot);

        //return null;
    }

    @Override
    public TimeSlotDto saveTimeSlot(TimeSlotDto timeSlotDto) throws ServiceException {
        if (timeSlotDto == null || !this.isValidTimeSlotDtoToSave(timeSlotDto)) {
            throw new ServiceException(1042, "invalid timeSlotDot object");
        }
        TimeSlot savedTimeSlot = this.timeSlotRepository.save(this.convertTimeSlotDtoToTimeSlot(timeSlotDto));
        return this.convertTimeSlotToTimeSlotDto(savedTimeSlot);
    }

    @Override
    public void deleteTimeSlotById(Long id) throws ServiceException {
        if(id == null || id <= 0) {
            throw new ServiceException(1040, "invalid timeslot id");
        }
        this.timeSlotRepository.delete(id);
    }

    @Override
    public TimeSlotDto updateTimeSlot(Long id, TimeSlotDto timeSlotDto) throws ServiceException {
        System.out.println("updateTimeSlot");
        if(id == null || id <= 0) {
            throw new ServiceException(1040, "invalid timeslot id");
        }
        if (timeSlotDto == null || !this.isValidTimeSlotDtoToUpdate(timeSlotDto)) {
            throw new ServiceException(1042, "invalid timeslot object");
        }
        if(id != timeSlotDto.getId()) {
            System.out.println(id + " " + timeSlotDto.getId());
            throw new ServiceException(1041, "timeslot ids doesn’t match");
        }
        if(this.timeSlotRepository.findOne(id) == null) {
            throw new ServiceException(1043, "timeslot doesn’t exist");
        }


        TimeSlot updatedTimeSlot = this.timeSlotRepository.save(this.convertTimeSlotDtoToTimeSlot(timeSlotDto));
        return this.convertTimeSlotToTimeSlotDto(updatedTimeSlot);
    }

    @Override
    public TimeSlotDto convertTimeSlotToTimeSlotDto(TimeSlot timeSlot) throws ServiceException {
        if(timeSlot == null) {
            throw new ServiceException(1042, "invalid timeslot object");
        }
        TimeSlotDto timeSlotDto = new TimeSlotDto();

        timeSlotDto.setId(timeSlot.getId());
    //    timeSlotDto.setOfferDto(this.offerService.convertOfferToOfferDto(timeSlot.getOffer()));
        timeSlotDto.setBeg_date_time(timeSlot.getBeg_date_time());
        timeSlotDto.setEnd_date_time(timeSlot.getEnd_date_time());
        timeSlotDto.setQuantity_total(timeSlot.getQuantity_total());
        timeSlotDto.setQuantity_booked(timeSlot.getQuantity_booked());

        // offerDto.setBusinessDto(this.businessService.convertBusinessToBusinessDto(offer.getBusiness())); // TODO: enable again
        // offerDto.setCategoryDto(this.categoryService.convertCategoryToCategoryDto(offer.getCategory())); // TODO: enable again

        return timeSlotDto;
    }

    @Override
    public TimeSlot convertTimeSlotDtoToTimeSlot(TimeSlotDto timeSlotDto) throws ServiceException {
        if(timeSlotDto == null) {
            throw new ServiceException(1042, "invalid timeslot object");
        }
        //System.out.println(timeSlotDto);
        TimeSlot timeSlot = new TimeSlot();

        timeSlot.setId(timeSlotDto.getId());
        if(timeSlotDto.getOfferDto()!=null) {
            //timeSlot.setOffer(this.offerService.convertOfferDtoToOffer(timeSlotDto.getOfferDto()));
        }else{
            //timeSlot.setOffer(null);
        }
        timeSlot.setBeg_date_time(timeSlotDto.getBeg_date_time());
        timeSlot.setEnd_date_time(timeSlotDto.getEnd_date_time());
        timeSlot.setQuantity_total(timeSlotDto.getQuantity_total());
        timeSlot.setQuantity_booked(timeSlotDto.getQuantity_booked());


        // offer.setBusiness(this.businessService.convertBusinessDtoToBusiness(offerDto.getBusinessDto())); // TODO: enable again
        // offer.setCategory(this.categoryService.convertCategoryDtoToCategory(offerDto.getCategoryDto())); // TODO: enable again

        return timeSlot;
    }

    @Override
    public boolean isValidTimeSlotDtoToSave(TimeSlotDto timeSlotDto) {
        return timeSlotDto.getQuantity_booked() >= 0 &&
                //offerDto.getBusinessDto() != null && this.businessService.isValidBusinessDtoToSave(offerDto.getBusinessDto()) && // TODO: this should be enabled again
                //offerDto.getCategoryDto() != null && this.categoryService.isValidCategoryDtoToSave(offerDto.getCategoryDto()) && // TODO: this should be enabled again
                timeSlotDto.getQuantity_total() > 0 &&
                timeSlotDto.getBeg_date_time() != null &&
                timeSlotDto.getEnd_date_time() != null;
    }

    @Override
    public boolean isValidTimeSlotDtoToUpdate(TimeSlotDto timeSlotDto) {
        return this.isValidTimeSlotDtoToSave(timeSlotDto) && timeSlotDto.getId() != null && timeSlotDto.getId() > 0;
    }

    public List<TimeSlotDto> convertTimeSlotDaoListToTimeSlotDtoList(OfferDto offerDto, List<TimeSlot> timeSlotList) throws ServiceException {
        List<TimeSlotDto> timeSlotDtoList = new ArrayList<>();

        for (TimeSlot timeSlot : timeSlotList) {
            TimeSlotDto saveSlot = this.convertTimeSlotToTimeSlotDto(timeSlot);
            saveSlot.setOfferDto(offerDto);
            timeSlotDtoList.add(saveSlot);
        }

        return timeSlotDtoList;
    }

    public List<TimeSlot> convertTimeSlotDtoListToTimeSlotDaoList(Offer offer, List<TimeSlotDto> timeSlotDtoList) throws ServiceException {
        List<TimeSlot> timeSlotList = new ArrayList<>();

        for (TimeSlotDto timeSlot : timeSlotDtoList) {
            TimeSlot saveSlot = this.convertTimeSlotDtoToTimeSlot(timeSlot);
            saveSlot.setOffer(offer);
            timeSlotList.add(saveSlot);
        }

        return timeSlotList;
    }
}
