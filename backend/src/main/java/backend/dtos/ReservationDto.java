package backend.dtos;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;
import lombok.Data;

import java.util.Date;

@Data
@JsonRootName("reservation")
public class ReservationDto {

    private Long id;

    @JsonProperty("customer")
    private CustomerDto customerDto;

    @JsonProperty("timeSlot")
    private TimeSlotDto timeSlotDto;

    private int state;

    private Date reservedAt;
}
