'use strict';
// set timezone
process.env.TZ = 'Europe/Vienna';
process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";

// get dependencies
let   express = require('express'),
      path = require('path'),
      http = require('http'),
      bodyParser = require('body-parser'),
      fs = require("fs"),
      morgan = require('morgan'),
      cron = require('node-cron'),
      _ = require('lodash'),
      optionsParser = require('options-parser'),
      cors = require('cors'),
      os = require('os'),
      confToParse;

// parse command line options
let options = optionsParser.parse({
  prod: { short: 'p', flag: true },
  dev: { short: 'd', flag: true },
  conf: { short: 'c', varName: '<CONFIG-FILE>' },
  help: {
    short: 'h',
    showHelp: {
      banner: 'node server.js [options]'
    }
  }
});

// parse config file
if(options.opt.conf){
  // load config file from command line
  confToParse = fs.readFileSync(options.opt.conf);
  process.env['CONFIG_FILE'] = options.opt.conf;
} else{
  // default config file
  if(options.opt.prod){
    confToParse = fs.readFileSync('./config/config_prod.json');
    process.env['CONFIG_FILE'] = './config/config_prod.json';
  } else{
    confToParse = fs.readFileSync('./config/config_dev.json');
    process.env['CONFIG_FILE'] = './config/config_dev.json';
  }
}
global.CONFIG = JSON.parse(confToParse);

// create express app
const app = express();

// create global log object
global.log = {
  // send mqtt message?
  debug: function (msg) {
    console.log(msg);
  },
  debugMorgan: function (msg) {
    console.log(msg);
  },
  error: function (msg) {
    console.error(msg);
  }
};

// add morgan logger and point it to log object
app.use(morgan('[REST: :method] [url: :url] - [res-status: :status] [res-length: :res[content-length]] [res-time: :response-time ms]',
  {stream: {write: function (msg) {
    log.debugMorgan(msg.substring(0, msg.lastIndexOf('\n')));
  }}}));

// use cors
app.use(cors());

// check if required options are set
if(!options.opt.prod && !options.opt.dev){
  log.error('Please use one of the following flags: --prod(-p), --dev(-d)');
  process.exit(1);
}

// set env variables from options and start/connect run-mode specific services
if(options.opt.prod){
  log.debug('#### simulator started in production mode ####');
  process.env['RUN_MODE'] = 'prod';
} else if(options.opt.dev){
  log.debug('#### simulator started in development mode ####');
  process.env['RUN_MODE'] = 'dev';
}

// do some task once every minute
cron.schedule('* * * * *', function(){
  log.debug('Some task done every minute fired.');
});

// parsers for POST data
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

// point static path to dist
app.use(express.static(path.join(__dirname, './webapp')));

// get our API routes
const users = require('./routes/control');
const data = require('./routes/data');
const simulation = require('./routes/simulation');

// apply API routes
app.use('/api/control', users);
app.use('/api/data', data);
app.use('/api/simulation', simulation);

// catch all other routes and return the index file (?!change for production?!)
app.get('*', function(req, res) {
  res.sendFile(path.join(__dirname, './webapp/index.html'));
});

// start servers
http.createServer(app).listen(CONFIG.insecure_port);

log.debug('#### simulator API running on localhost:' + CONFIG.insecure_port +'/api/ ####');
