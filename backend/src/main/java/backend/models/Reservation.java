package backend.models;

import lombok.Data;
import lombok.experimental.Delegate;
import org.hibernate.validator.constraints.NotBlank;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

@Data
@Entity(name = "reservation")
public class Reservation implements Serializable {

    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private int state;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "customer_id")
    public Customer customer;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "timeslot_id")
    public TimeSlot timeSlot;

    @Column
    @NotNull
    public Date reservedAt;

}

