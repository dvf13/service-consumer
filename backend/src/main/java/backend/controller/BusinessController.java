package backend.controller;

import backend.dtos.BusinessDto;
import backend.dtos.ResponseDto;

import java.util.List;

public interface BusinessController {

    ResponseDto getAllBusinesses();

    ResponseDto getBusinessById(Long id);

    ResponseDto saveBusiness(BusinessDto businessDto);

    void deleteBusinessById(Long id);

    ResponseDto updateBusiness(Long id, BusinessDto businessDto);
}
