import axios from 'axios';
import UserService from './UserService';

/* axios.defaults.headers.common = {
  'X-Requested-With': 'XMLHttpRequest',
  'X-CSRF-TOKEN': document.querySelector('meta[name="_csrf"]').getAttribute('content'),
}; */

export default {
  async get(url, options) {
    let o = options;
    o = this.getExtendedOptionsObject(o);
    return axios.get(url, o);
  },
  async post(url, body, options) {
    let o = options;
    o = this.getExtendedOptionsObject(o);
    return axios.post(url, body, o);
  },
  async put(url, body, options) {
    let o = options;
    o = this.getExtendedOptionsObject(o);
    return axios.put(url, body, o);
  },
  async delete(url, options) {
    let o = options;
    o = this.getExtendedOptionsObject(o);
    return axios.delete(url, o);
  },

  getExtendedOptionsObject(options) {
    let o = options;

    if (o === undefined) {
      o = { headers: {} };
    }

    if (UserService.isAuthenticated()) {
      const token = UserService.getAuthToken();

      if (o) {
        o.headers.Authorization = `Bearer ${token.access_token}`;
      } else {
        o = { headers: { Authorization: `Bearer ${token.access_token}` } };
      }
    }
    // o.headers['Access-Control-Allow-Origin'] = 'http://localhost:4200';
    o.headers['Content-Type'] = 'application/json';
    return o;
  },

  // TODO: implement further REST method wrappers if needed
};
