let fs = require("fs"),
  CONFIG = JSON.parse(fs.readFileSync(process.env['CONFIG_FILE'])),
  StateService = require('./StateService'),
  MetricsService = require('./MetricsService'),
  request = require('request'),
  _ = require('lodash');

module.exports = {
  async simulateGetOffer(getOfferEndpoint, userCount, requestProcessing, callback){
    let config = StateService.getConfig();
    let done = _.after(userCount, () => {
      console.log('Done simulating get offers!');
      return callback(null);
    });

    let users = StateService.getUsers();
    let businessUsers = StateService.getBusinessUsers();

    let simulationUsers = [];
    simulationUsers = simulationUsers.concat(users);
    simulationUsers = simulationUsers.concat(businessUsers);
    simulationUsers = _.take(simulationUsers, userCount);

    let startDates = [];
    this.simulateNextGetOffer(requestProcessing === 'concurrently', getOfferEndpoint, 0, simulationUsers, config, startDates, done);
  },

  simulateNextGetOffer: (concurrently, endpoint, i, users, config, startDates, done) => {
    if(concurrently && i < users.length - 1){
      module.exports.simulateNextGetOffer(concurrently, endpoint, i + 1, users, config, startDates, done);
    }

    const c = i;
    let u = users[i];
    let options = {
      uri: config.apiEndpoint + endpoint,
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Authorization': `Bearer ${u.authToken}`
      },
      json: true
    };

    startDates[c] = Date.now();
    request(options, function (error, response, body) {
      let endDate = Date.now();
      if(error){
        console.error(error);
      } else if(!body || !body.data){
        error = 'No data received.';
      }
      MetricsService.logRequestTime('GET', endpoint, startDates[c], endDate, error);
      if(!concurrently && i < users.length - 1){
        module.exports.simulateNextGetOffer(concurrently, endpoint, i + 1, users, config, startDates, done);
      }
      done();
    });
  },

  async simulateCreateOffer(createOfferEndpoint, businessCount, requestProcessing, callback){
    let config = StateService.getConfig();
    let done = _.after(businessCount, () => {
      console.log('Done simulating creating offers!');
      return callback(null);
    });

    let businessUsers = StateService.getBusinessUsers();

    let simulationUsers = [];
    simulationUsers = simulationUsers.concat(businessUsers);
    simulationUsers = _.take(simulationUsers, businessCount);

    let offers = [];
    let randInt = (Math.random() * 1000000).toFixed(0);
    for (let i = 0; i < simulationUsers.length; i++){
      // TODO: change model as soon as categories are implemented
      let newOffer = {
        title: `TestOffer_${randInt}${i}`,
        description: `This is a test offer TestOffer_${randInt}${i} created by the simulator`,
        pictureUrl: 'www.picture.com/1',
        price: randInt + i,
        discount: ((randInt + i) % 99) + 0.1,
        amount: (randInt + i) % 105,
        validUntil: new Date()
      };
      offers.push(newOffer);
    }

    let startDates = [];
    this.simulateNextCreateOffer(requestProcessing === 'concurrently', createOfferEndpoint, 0, simulationUsers, offers, config, startDates, done);
  },

  simulateNextCreateOffer: (concurrently, endpoint, i, users, offers, config, startDates, done) => {
    if(concurrently && i < users.length - 1){
      module.exports.simulateNextCreateOffer(concurrently, endpoint, i + 1, users, offers, config, startDates, done);
    }

    const c = i;
    let u = users[i];
    let options = {
      uri: config.apiEndpoint + endpoint,
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Authorization': `Bearer ${u.authToken}`
      },
      body: offers[i],
      json: true
    };

    startDates[c] = Date.now();
    request(options, function (error, response, body) {
      let endDate = Date.now();
      if(error){
        console.error(error);
      } else if(!body || !body.data){
        error = 'No data received.';
      } else if(body.error){
        error = body.error;
      }
      MetricsService.logRequestTime('POST', endpoint, startDates[c], endDate, error);
      if(!concurrently && i < users.length - 1){
        module.exports.simulateNextCreateOffer(concurrently, endpoint, i + 1, users, offers, config, startDates, done);
      }
      done();
    });
  },
};