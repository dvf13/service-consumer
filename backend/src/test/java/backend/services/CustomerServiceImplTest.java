package backend.services;

import backend.dtos.CustomerDto;
import backend.exceptions.ServiceException;
import backend.models.Customer;
import backend.repositories.AuthorityRepository;
import backend.repositories.CustomerRepository;
import backend.services.impl.CustomerServiceImpl;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class CustomerServiceImplTest {

    private CustomerService customerService;

    private CustomerRepository customerRepository;

    private AuthorityRepository authorityRepository;

    private BCryptPasswordEncoder bCryptPasswordEncoder;

    private ValidationService validationService;

    public CustomerServiceImplTest(){
        this.customerRepository = mock(CustomerRepository.class);
        this.authorityRepository = mock(AuthorityRepository.class);
        this.bCryptPasswordEncoder = mock(BCryptPasswordEncoder.class);
        this.validationService = mock(ValidationService.class);
        this.customerService = new CustomerServiceImpl(customerRepository, authorityRepository, bCryptPasswordEncoder, validationService);
    }

    @Test
    public void getAllCustomers_shouldFindAll() throws ServiceException{
        when(this.customerRepository.findAll()).thenReturn(getAllCustomers());

        List<CustomerDto> customerDtoList = this.customerService.getAllCustomers();

        assertNotNull(customerDtoList);
        assertEquals(1, customerDtoList.size());
        assertEquals((Long) 1L, customerDtoList.get(0).getId());
        assertEquals("Test_Username", customerDtoList.get(0).getUsername());
        assertEquals("Test_PasswordHash", customerDtoList.get(0).getPassword());
        assertEquals("mail@test.com", customerDtoList.get(0).getEmail());
        assertEquals("Test_First_Name", customerDtoList.get(0).getFirstName());
        assertEquals("Test_Last_Name", customerDtoList.get(0).getLastName());
        assertEquals(new Date(1991,1,1), customerDtoList.get(0).getBirthdate());
        assertEquals('M', customerDtoList.get(0).getGender());
    }

    @Test(expected = ServiceException.class)
    public void getAllCustomerTest_NoCustomerInRepository_shouldThrowServiceException() throws ServiceException{
        when(this.customerRepository.findAll()).thenReturn(null);

        List<CustomerDto> customerDtoList = this.customerService.getAllCustomers();

        assertNull(customerDtoList);
    }

    @Test
    public void getCustomerById_ValidId_shouldFindOne() throws ServiceException{
        when(this.customerRepository.findOne(Mockito.anyLong())).thenReturn(getCustomer());

        CustomerDto customerDto = this.customerService.getCustomerById(1L);

        assertNotNull(customerDto);
        assertEquals((Long) 1L, customerDto.getId());
        assertEquals("Test_Username", customerDto.getUsername());
        assertEquals("Test_PasswordHash", customerDto.getPassword());
        assertEquals("mail@test.com", customerDto.getEmail());
        assertEquals("Test_First_Name", customerDto.getFirstName());
        assertEquals("Test_Last_Name", customerDto.getLastName());
        assertEquals(new Date(1991,1,1), customerDto.getBirthdate());
        assertEquals('M', customerDto.getGender());
    }

    @Test(expected = ServiceException.class)
    public void getCustomerById_ValidIdButNotExisting_ShouldThrowServiceExecption() throws ServiceException{
        when(this.customerRepository.findOne(Mockito.anyLong())).thenReturn(null);

        CustomerDto customerDto = this.customerService.getCustomerById(1L);

        assertNull(customerDto);
    }

    @Test(expected = ServiceException.class)
    public void getCustomerById_InvalidId_ShouldThrowServiceExecption() throws ServiceException{
        when(this.customerRepository.findOne(Mockito.anyLong())).thenReturn(null);

        CustomerDto customerDto = this.customerService.getCustomerById(-1L);

        assertNull(customerDto);
    }

    @Test(expected = ServiceException.class)
    public void saveCustomerTest_CustomerNotValid_shouldThrowServiceException() throws ServiceException{
        when(this.customerRepository.save(any(Customer.class))).thenReturn(null);

        CustomerDto customerDto = this.customerService.saveCustomer(null);

        assertNull(customerDto);
    }

    @Test(expected = ServiceException.class)
    public void updateCustomerTest_IdIsNull_shouldThrowServiceException() throws ServiceException{
        CustomerDto customerDto = this.customerService.updateCustomer(null, getCustomerDto());
        assertNull(customerDto);
    }

    @Test(expected = ServiceException.class)
    public void updateCustomerTest_IdBelowZero_shouldThrowServiceException() throws ServiceException{
        CustomerDto customerDto = this.customerService.updateCustomer(-1L, getCustomerDto());
        assertNull(customerDto);
    }

    @Test(expected = ServiceException.class)
    public void updateCustomerTest_CustomerDtoIsNull_shouldThrowServiceException() throws ServiceException{
        CustomerDto customerDto = this.customerService.updateCustomer(1L, null);
        assertNull(customerDto);
    }

    @Test(expected = ServiceException.class)
    public void updateCustomerTest_IdsDoNotMatch_shouldThrowServiceException() throws ServiceException{
        CustomerDto customerDto = this.customerService.updateCustomer(2L, getCustomerDto());
        assertNull(customerDto);
    }

    @Test(expected = ServiceException.class)
    public void updateCustomerTest_IdNotInRepository_shouldThrowServiceException() throws ServiceException{
        when(this.customerRepository.findOne(Mockito.anyLong())).thenReturn(null);

        CustomerDto customerDto = this.customerService.updateCustomer(1L, getCustomerDto());

        assertNull(customerDto);
    }

    @Test(expected = ServiceException.class)
    public void deleteCustomerByIdTest_IdIsNull_shouldThrowException() throws ServiceException{
        this.customerService.deleteCustomerById(null);
    }

    @Test(expected = ServiceException.class)
    public void deleteCustomerByIdTest_IdIsBelowZero_shouldThrowException() throws ServiceException{
        this.customerService.deleteCustomerById(-1L);
    }

    @Test
    public void isValidCustomerDtoToSaveTest_InvalidCustomer_ShouldBeFalse() throws ServiceException{
        CustomerDto customerDto = new CustomerDto();
        assertFalse(this.customerService.isValidCustomerDtoToSave(customerDto));
    }

    @Test
    public void isValidCustomerDtoToUpdateTest_CommitedCustomerNull() throws ServiceException{
        CustomerDto customerDto = new CustomerDto();
        assertFalse(this.customerService.isValidCustomerDtoToUpdate(customerDto));
    }

    @Test
    public void isValidCustomerDtoToUpdateTest_IdIsZero() throws ServiceException{
        CustomerDto customerDto = getCustomerDto();
        customerDto.setId(0L);
        assertFalse(this.customerService.isValidCustomerDtoToUpdate(customerDto));
    }

    private Customer getCustomer(){
        Customer customer = new Customer();
        customer.setId(1L);
        customer.setUsername("Test_Username");
        customer.setPasswordHash("Test_PasswordHash");
        customer.setEmail("mail@test.com");
        customer.setFirstName("Test_First_Name");
        customer.setLastName("Test_Last_Name");
        customer.setBirthdate(new Date(1991, 1,1));
        customer.setGender('M');
        return customer;
    }

    private List<Customer> getAllCustomers(){
        List<Customer> customerArrayList = new ArrayList<>();
        customerArrayList.add(getCustomer());
        return customerArrayList;
    }

    private CustomerDto getCustomerDto(){
        CustomerDto customerDto = new CustomerDto();
        customerDto.setId(1L);
        customerDto.setUsername("Test_Username");
        customerDto.setPassword("Test_PasswordHash");
        customerDto.setEmail("mail@test.com");
        customerDto.setFirstName("Test_First_Name");
        customerDto.setLastName("Test_Last_Name");
        customerDto.setBirthdate(new Date(1991, 1,1));
        customerDto.setGender('M');
        return customerDto;
    }

    private List<CustomerDto> getAllCustomerDto(){
        List<CustomerDto> customerArrayList = new ArrayList<>();
        customerArrayList.add(getCustomerDto());
        return customerArrayList;
    }
}
