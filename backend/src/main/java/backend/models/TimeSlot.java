package backend.models;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.validator.constraints.NotBlank;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.sql.Time;
import java.util.Date;

@Data
@Entity(name = "timeslot")
public class TimeSlot implements Serializable {

    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    // TODO: @NotNull enable again after implementation
    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name = "offer_id")
    @JsonBackReference
    private Offer offer;

    @Column
    @NotNull
    private Date beg_date_time;

    @Column
    @NotNull
    private Date end_date_time;

    @Column
    @NotNull
    private int quantity_total;

    @Column
    @NotNull
    private int quantity_booked;
}