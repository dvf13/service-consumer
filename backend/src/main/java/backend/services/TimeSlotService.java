package backend.services;


import backend.dtos.OfferDto;
import backend.dtos.TimeSlotDto;
import backend.exceptions.ServiceException;
import backend.models.Offer;
import backend.models.TimeSlot;

import java.util.List;

public interface TimeSlotService {

    List<TimeSlotDto> getAllTimeSlots() throws ServiceException;

    TimeSlotDto getTimeSlotById(Long id) throws ServiceException;

    List<TimeSlotDto> getTimeSlotByOffer(Long offer) throws ServiceException;

    TimeSlotDto saveTimeSlot(TimeSlotDto timeSlotDto) throws ServiceException;

    void deleteTimeSlotById(Long id) throws ServiceException;

    TimeSlotDto updateTimeSlot(Long id, TimeSlotDto timeSlotDtot) throws ServiceException;

    TimeSlotDto convertTimeSlotToTimeSlotDto(TimeSlot timeSlot) throws ServiceException;

    TimeSlot convertTimeSlotDtoToTimeSlot(TimeSlotDto timeSlotDto) throws ServiceException;

    boolean isValidTimeSlotDtoToSave(TimeSlotDto timeSlotDto);

    boolean isValidTimeSlotDtoToUpdate(TimeSlotDto timeSlotDto);

    List<TimeSlot> convertTimeSlotDtoListToTimeSlotDaoList(Offer offer, List<TimeSlotDto> timeSlotDtoList) throws ServiceException;

    public List<TimeSlotDto> convertTimeSlotDaoListToTimeSlotDtoList(OfferDto offerDto, List<TimeSlot> timeSlotList) throws ServiceException;
}
