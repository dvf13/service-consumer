package backend.dtos;

import com.fasterxml.jackson.annotation.JsonRootName;
import lombok.Data;

@Data
@JsonRootName("message")
public class MessageDto {

    private String message;


    public MessageDto() { }

    public MessageDto(String message) {
        this.message = message;
    }
}
