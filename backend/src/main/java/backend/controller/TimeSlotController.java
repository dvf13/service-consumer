package backend.controller;

import backend.dtos.OfferDto;
import backend.dtos.ResponseDto;
import backend.dtos.TimeSlotDto;

public interface TimeSlotController {

    ResponseDto getAllTimeSlots();

    ResponseDto getTimeSlotsByOfferId(Long Id);

    ResponseDto saveTimeSlot(TimeSlotDto timeSlotDto);

    void deleteTimeSlotById(Long id);

    ResponseDto updateTimeSlot(Long id, TimeSlotDto timeslotDto);
}
