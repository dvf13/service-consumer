import HttpService from './HttpService';
import UserService from './UserService';
import config from '../config';
import router from '../router';

export default {
  async getAllOffers() {
    if (!UserService.isAuthenticated()) {
      // return Promise.reject(new Error('Not logged in!'));
      // if not logged in only get a few offers
      return HttpService.get(`${config.apiUrl}offer/limited`)
        .then((response) => {
          if (!response.data.error) {
            return Promise.resolve(response.data.data);
          }
          // eslint-disable-next-line no-console
          console.error(`Response error message: ${response.data.data.message}`);
          return Promise.reject(response.data.data.message);
        }, error => Promise.reject(error.statusText))
        .catch(e => Promise.reject(e));
    }
    return HttpService.get(`${config.apiUrl}offer/`)
      .then((response) => {
        if (!response.data.error) {
          return Promise.resolve(response.data.data);
        }
        // eslint-disable-next-line no-console
        console.error(`Response error message: ${response.data.data.message}`);
        return Promise.reject(response.data.data.message);
      }, error => Promise.reject(error.statusText))
      .catch(e => Promise.reject(e));
  },
  async getOffer(id) {
    if (!UserService.isAuthenticated()) {
      return Promise.reject(new Error('Not logged in!'));
    }
    return HttpService.get(`${config.apiUrl}offer/${id}`)
      .then((response) => {
        if (!response.data.error) {
          return Promise.resolve(response.data.data);
        }
        // eslint-disable-next-line no-console
        console.error(`Response error message: ${response.data.data.message}`);
        return Promise.reject(response.data.data.message);
      }, error => Promise.reject(error.statusText))
      .catch(e => Promise.reject(e));
  },
  async getOffersByTimeSlotIds(id) {
    if (!UserService.isAuthenticated()) {
      return Promise.reject(new Error('Not logged in!'));
    }
    return HttpService.get(`${config.apiUrl}offer/timeslot/${id}`)
      .then((response) => {
        if (!response.data.error) {
          return Promise.resolve(response.data.data);
        }
        // eslint-disable-next-line no-console
        console.error(`Response error message: ${response.data.data.message}`);
        return Promise.reject(response.data.data.message);
      }, error => Promise.reject(error.statusText))
      .catch(e => Promise.reject(e));
  },
  async saveOffer(offer) {
    if (!UserService.isAuthenticated()) {
      return Promise.reject(new Error('Not logged in!'));
    }
    return HttpService.post(`${config.apiUrl}offer/`, offer)
      .then((response) => {
        if (!response.data.error) {
          // eslint-disable-next-line no-console
          console.log('Offer creation successful!');
          router.push({ path: `/offer/${response.data.data.id}` });
          return Promise.resolve();
        }
        // eslint-disable-next-line no-console
        console.error(`Response error message: ${response.data.data.message}`);
        return Promise.reject(response.data.data.message);
      }, error => Promise.reject(error.statusText))
      .catch(e => Promise.reject(e));
  },
  async getTimeSlotsByOfferId(id) {
    if (!UserService.isAuthenticated()) {
      return Promise.reject(new Error('Not logged in!'));
    }
    return HttpService.get(`${config.apiUrl}timeslot/${id}`)
      .then((response) => {
        if (!response.data.error) {
          return Promise.resolve(response.data.data);
        }
        // eslint-disable-next-line no-console
        console.error(`Response error message: ${response.data.data.message}`);
        return Promise.reject(response.data.data.message);
      }, error => Promise.reject(error.statusText))
      .catch(e => Promise.reject(e));
  },
  async saveReservation(reservation) {
    if (!UserService.isAuthenticated()) {
      return Promise.reject(new Error('Not logged in!'));
    }

    // eslint-disable-next-line no-console
    console.log('saveReservationcalled');
    return HttpService.post(`${config.apiUrl}reservation/`, reservation)
      .then((response) => {
        if (!response.data.error) {
          // eslint-disable-next-line no-console
          console.log('Reservation booking successful!');
          // router.push({ path: `/timeslot/${response.data.data.id}` });
          return Promise.resolve();
        }
        // eslint-disable-next-line no-console
        console.error(`Response error message: ${response.data.data.message}`);
        return Promise.reject(response.data.data.message);
      }, error => Promise.reject(error.statusText))
      .catch(e => Promise.reject(e));
  },
  async updateReservation(reservation) {
    if (!UserService.isAuthenticated()) {
      return Promise.reject(new Error('Not logged in!'));
    }

    // eslint-disable-next-line no-console
    console.log('updateReservationcalled');
    return HttpService.put(`${config.apiUrl}reservation/`, reservation)
      .then((response) => {
        if (!response.data.error) {
          // eslint-disable-next-line no-console
          console.log('Reservation update successful!');
          return Promise.resolve();
        }
        // eslint-disable-next-line no-console
        console.error(`Response error message: ${response.data.data.message}`);
        return Promise.reject(response.data.data.message);
      }, error => Promise.reject(error.statusText))
      .catch(e => Promise.reject(e));
  },
  async getAllCategories() {
    if (!UserService.isAuthenticated()) {
      return Promise.reject(new Error('Not logged in!'));
    }
    return HttpService.get(`${config.apiUrl}category/`)
      .then((response) => {
        if (!response.data.error) {
          return Promise.resolve(response.data.data);
        }
        // eslint-disable-next-line no-console
        console.error(`Response error message: ${response.data.data.message}`);
        return Promise.reject(response.data.data.message);
      }, error => Promise.reject(error.statusText))
      .catch(e => Promise.reject(e));
  },
};
