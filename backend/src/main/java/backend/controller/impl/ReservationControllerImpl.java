package backend.controller.impl;

import backend.controller.ReservationController;
import backend.dtos.ErrorDto;
import backend.dtos.ReservationDto;
import backend.dtos.ResponseDto;
import backend.exceptions.ServiceException;
import backend.services.ReservationService;
import backend.services.TimeSlotService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.websocket.server.ServerEndpoint;

import java.util.Date;
import java.util.List;

@Slf4j
@RestController
@RequestMapping("/api/reservation/")
public class ReservationControllerImpl implements ReservationController {

    private ReservationService reservationService;


    @Autowired
    public ReservationControllerImpl(ReservationService reservationService) {
        this.reservationService = reservationService;
    }

    @Override
    @ResponseBody
    @PreAuthorize("hasAnyRole('ADMIN')")
    @RequestMapping(value = "/booked", method = RequestMethod.GET)
    public ResponseDto findReservationsByState(Integer state) {
        try {
            List<ReservationDto> reservationDtoListResponse = this.reservationService.findReservationsByState(2);
            return new ResponseDto<>(false, reservationDtoListResponse);
        } catch (ServiceException e) {
            log.error(e.getMessage());
            return new ResponseDto<>(true, new ErrorDto(e.getCode(), e.getMessage()));
        }
    }

    @Override
    @ResponseBody
    @PreAuthorize("hasAnyRole('ADMIN', 'BUSINESS', 'CUSTOMER')")
    @RequestMapping(value = "", method = RequestMethod.GET)
    public ResponseDto getAllReservations() {
        try {
            List<ReservationDto> reservationDtoListResponse = this.reservationService.getAllReservations();
            return new ResponseDto<>(false, reservationDtoListResponse);
        } catch (ServiceException e) {
            log.error(e.getMessage());
            return new ResponseDto<>(true, new ErrorDto(e.getCode(), e.getMessage()));
        }
    }

    @Override
    @ResponseBody
    @PreAuthorize("hasAnyRole('ADMIN', 'CUSTOMER')")
    @RequestMapping(value = "/customer/{customer_id}/reserved", method = RequestMethod.GET)
    public ResponseDto findReservationsByCustomerId(@PathVariable Long customer_id, Integer state) {
        try {
            List<ReservationDto> reservationDtoListResponse = this.reservationService.findReservationsByCustomerId(customer_id, 0);
            return new ResponseDto<>(false, reservationDtoListResponse);
        } catch (ServiceException e) {
            log.error(e.getMessage());
            return new ResponseDto<>(true, new ErrorDto(e.getCode(), e.getMessage()));
        }
    }

    @Override
    @ResponseBody
    @PreAuthorize("hasAnyRole('ADMIN', 'CUSTOMER')")
    @RequestMapping(value = "/customer/{customer_id}/booked", method = RequestMethod.GET)
    public ResponseDto findBookedReservationsByCustomerId(@PathVariable Long customer_id, Integer state) {
        try {
            List<ReservationDto> reservationDtoListResponse = this.reservationService.findReservationsByCustomerId(customer_id, 2);
            return new ResponseDto<>(false, reservationDtoListResponse);
        } catch (ServiceException e) {
            log.error(e.getMessage());
            return new ResponseDto<>(true, new ErrorDto(e.getCode(), e.getMessage()));
        }
    }

    @Override
    @ResponseBody
    @PreAuthorize("hasAnyRole('ADMIN', 'CUSTOMER')")
    @RequestMapping(value = "/customer/{customer_id}/cancelled", method = RequestMethod.GET)
    public ResponseDto findCancelledReservationsByCustomerId(@PathVariable Long customer_id, Integer state) {
        try {
            List<ReservationDto> reservationDtoListResponse = this.reservationService.findReservationsByCustomerId(customer_id, 3);
            return new ResponseDto<>(false, reservationDtoListResponse);
        } catch (ServiceException e) {
            log.error(e.getMessage());
            return new ResponseDto<>(true, new ErrorDto(e.getCode(), e.getMessage()));
        }
    }

    @Override
    @ResponseBody
    @PreAuthorize("hasAnyRole('ADMIN', 'BUSINESS', 'CUSTOMER')")
    @RequestMapping(value = "/timeslot/{id}", method = RequestMethod.GET)
    public ResponseDto getReservationsByTimeSlotId(@PathVariable Long id) {
        try {
            List<ReservationDto> reservationDtoListResponse = this.reservationService.getReservationsByTimeSlotId(id);
            for(ReservationDto dto : reservationDtoListResponse){
                dto.setTimeSlotDto(null);
            }
            System.out.println(new ResponseDto<>(false, reservationDtoListResponse));
            return new ResponseDto<>(false, reservationDtoListResponse);
        } catch (ServiceException e) {
            log.error(e.getMessage());
            return new ResponseDto<>(true, new ErrorDto(e.getCode(), e.getMessage()));
        }

    }

    @Override
    @ResponseBody
    @PreAuthorize("hasAnyRole('ADMIN', 'BUSINESS', 'CUSTOMER')")
    @RequestMapping(value = "{id}", method = RequestMethod.GET)
    public ResponseDto getReservationById(@PathVariable Long id) {
        try {
            ReservationDto reservationDtoResponse = this.reservationService.getReservationById(id);
            return new ResponseDto<>(false, reservationDtoResponse);
        } catch (ServiceException e) {
            log.error(e.getMessage());
            return new ResponseDto<>(true, new ErrorDto(e.getCode(), e.getMessage()));
        }

    }

    @Override
    @ResponseBody
    @PreAuthorize("hasAnyRole('ADMIN', 'CUSTOMER')")
    @RequestMapping(value = "", method = RequestMethod.POST)
    public ResponseDto saveReservation(@RequestBody ReservationDto reservationDto) {
        try {
            //System.out.println("SaveReservation \n" + reservationDto);
            ReservationDto reservationDtoResponse = this.reservationService.saveReservation(reservationDto);
            return new ResponseDto<>(false, reservationDtoResponse);
        } catch (ServiceException e) {
            log.error(e.getMessage());
            return new ResponseDto<>(true, new ErrorDto(e.getCode(), e.getMessage()));
        }
    }

    @Override
    @ResponseBody
    @PreAuthorize("hasAnyRole('ADMIN', 'CUSTOMER')")
    @RequestMapping(value = "{id}", method = RequestMethod.DELETE)
    public void deleteReservationById(@PathVariable Long id) {
        try {
            this.reservationService.deleteReservationById(id);
        } catch (ServiceException e) {
            log.error(e.getMessage());
        }
    }

    @Override
    @ResponseBody
    @PreAuthorize("hasAnyRole('ADMIN', 'CUSTOMER')")
    @RequestMapping(value = "", method = RequestMethod.PUT)
    public ResponseDto updateReservation(@RequestBody ReservationDto reservationDto) {
        try {
            ReservationDto reservationDtoResponse = this.reservationService.updateReservation(reservationDto);
            return new ResponseDto<>(false, reservationDtoResponse);
        } catch(ServiceException e) {
            log.error(e.getMessage());
            return new ResponseDto<>(true, new ErrorDto(e.getCode(), e.getMessage()));
        }
    }


    @Override
    @ResponseBody
    @PreAuthorize("hasAnyRole('ADMIN', 'CUSTOMER')")
    @RequestMapping(value = "/wishlist/{customerId}/{id}", method = RequestMethod.DELETE)
    public ResponseDto deleteWishlistEntry(@PathVariable Long customerId, @PathVariable Long id) {
        try {
            ReservationDto reservationDtoResponse = this.reservationService.deleteFromWishlist(customerId, id);
            return new ResponseDto<>(false, reservationDtoResponse);
        } catch(ServiceException e) {
            log.error(e.getMessage());
            return new ResponseDto<>(true, new ErrorDto(e.getCode(), e.getMessage()));
        }
    }
}
