# Last Minute Deal Simulator

Starting on ``http://localhost:3030``

### Usage

* For using the simulator run ``npm run prod-server``
* For developing the simulator run ``npm run nodemon-dev-server`` and then ``npm run build-watch`` (last one for autobuilding the sources)
* For using the simulator run ``npm run prod-server``