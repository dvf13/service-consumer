package backend.repositories;

import backend.models.Offer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

/**
 * Offer repository for jpa
 */
@Repository
public interface OfferRepository extends JpaRepository<Offer, Long> {

    List<Offer> findOffersByBusinessIdOrderByValidUntil(Long business_id);

    @Query("select o from offer o order by discount desc, amount asc")
    List<Offer> findOffersOrderByDiscountDescAndAmountAsc();

    @Query(value = "SELECT * from offer inner join timeslot on timeslot.offer_id = offer.id where timeslot.id = :timeslot_id", nativeQuery = true)
    Offer findOfferByTimeSlotId(@Param("timeslot_id") Long timeSlot_id);

    @Query(value = "SELECT COUNT(reservation.id) from timeslot join reservation on reservation.timeslot_id = timeslot.id where timeslot_id = :timeslot_id and reservation.reserved_at between :fromDate AND :toDate", nativeQuery = true)
    int findReservationCount(@Param("timeslot_id") Long timeslot_id, @Param("fromDate") Date fromDate, @Param("toDate") Date toDate);
}