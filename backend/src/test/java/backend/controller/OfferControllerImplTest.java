package backend.controller;

import backend.controller.impl.OfferControllerImpl;
import backend.dtos.ErrorDto;
import backend.dtos.OfferDto;
import backend.dtos.ResponseDto;
import backend.exceptions.ServiceException;
import backend.services.OfferService;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.*;
import static org.junit.Assert.*;
import static org.hamcrest.Matchers.instanceOf;

public class OfferControllerImplTest {

    private OfferService offerService;

    private OfferController offerController;

    public OfferControllerImplTest() {
        this.offerService = mock(OfferService.class);
        this.offerController = new OfferControllerImpl(offerService);
    }

    @Test
    public void getAllOffersTest_positive() throws ServiceException {
        when(this.offerService.getAllOffers()).thenReturn(this.getOfferDtoList());

        ResponseDto allOffers = this.offerController.getAllOffers();

        assertFalse(allOffers.isError());
        assertNotNull(allOffers);
        assertEquals(this.getOfferDtoList(), allOffers.getData());
    }

    @Test
    public void getAllOffersTest_throwException() throws ServiceException {
        when(this.offerService.getAllOffers()).thenThrow(ServiceException.class);

        ResponseDto allOffers = this.offerController.getAllOffers();

        assertTrue(allOffers.isError());
        assertNotNull(allOffers);
        assertThat(allOffers.getData(), instanceOf(ErrorDto.class));
    }

    @Test
    public void getOfferbyIdTest_positive() throws ServiceException {
        when(this.offerService.getOfferById(Mockito.anyLong())).thenReturn(this.getOfferDto());

        ResponseDto offerById = this.offerController.getOfferById(1L);

        assertFalse(offerById.isError());
        assertNotNull(offerById);
        assertEquals(this.getOfferDto(), offerById.getData());
    }

    @Test
    public void getOfferbyIdTest_throwException() throws ServiceException {
        when(this.offerService.getOfferById(Mockito.anyLong())).thenThrow(ServiceException.class);

        ResponseDto offerById = this.offerController.getOfferById(-1L);

        assertTrue(offerById.isError());
        assertNotNull(offerById);
        assertThat(offerById.getData(), instanceOf(ErrorDto.class));
    }

    @Test
    public void saveOfferTest_positive() throws ServiceException {
        when(this.offerService.saveOffer(any(OfferDto.class))).thenReturn(this.getOfferDto());

        ResponseDto saveOffer = this.offerController.saveOffer(getOfferDto());

        assertFalse(saveOffer.isError());
        assertNotNull(saveOffer);
        assertEquals(this.getOfferDto(), saveOffer.getData());
    }

    @Test
    public void saveOfferTest_throwException() throws ServiceException {
        when(this.offerService.saveOffer(any(OfferDto.class))).thenThrow(ServiceException.class);

        ResponseDto saveOffer = this.offerController.saveOffer(null);

        assertTrue(saveOffer.isError());
        assertNotNull(saveOffer);
        assertThat(saveOffer.getData(), instanceOf(ErrorDto.class));
    }

    @Test
    public void updateOfferTest_positive() throws ServiceException {
        when(this.offerService.updateOffer(anyLong(), any(OfferDto.class))).thenReturn(getOfferDto());

        ResponseDto responseDto = this.offerController.updateOffer(1L, getOfferDto());

        assertFalse(responseDto.isError());
        assertNotNull(responseDto);
        assertEquals(responseDto.getData(), getOfferDto());
    }

    @Test
    public void updateOfferTest_throwException() throws ServiceException {
        when(this.offerService.updateOffer(anyLong(), any(OfferDto.class))).thenThrow(ServiceException.class);

        ResponseDto responseDto = this.offerController.updateOffer(1L, null);

        assertTrue(responseDto.isError());
        assertNotNull(responseDto);
        assertThat(responseDto.getData(), instanceOf(ErrorDto.class));
    }

    @Test
    public void deleteOfferByIdTest_throwException() throws ServiceException {
        doThrow(ServiceException.class).when(this.offerService).deleteOfferById(Mockito.anyLong());

        this.offerController.deleteOfferById(-1L);
    }

    private OfferDto getOfferDto() {
        OfferDto offerDto = new OfferDto();
        offerDto.setId(1L);
        offerDto.setAmount(10);
        return offerDto;
    }

    private List<OfferDto> getOfferDtoList() {
        List<OfferDto> offerDtoList = new ArrayList<>();
        offerDtoList.add(getOfferDto());
        return offerDtoList;
    }
}
