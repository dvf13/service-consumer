import Vue from 'vue';
import VueRouter from 'vue-router';
import VueAxios from 'vue-axios';
import axios from 'axios';
import VueLocalStorage from 'vue-localstorage';

Vue.use(VueRouter);
Vue.use(VueAxios, axios);
Vue.use(VueLocalStorage);

import App from './App.vue';
import HomeComponent from './components/HomeComponent.vue';
import ControlComponent from './components/ControlComponent.vue';
import StartControlComponent from './components/StartControlComponent.vue';
import SimulationComponent from './components/SimulationComponent.vue';
import DataComponent from './components/DataComponent.vue';

const routes = [
  {
      path: '/',
      redirect: '/home'
  },
  {
      name: 'HomeComponent',
      path: '/home',
      component: HomeComponent
  },
  {
      name: 'ControlComponent',
      path: '/control',
      component: ControlComponent
  },
  {
    name: 'StartControlComponent',
    path: '/startControl',
    component: StartControlComponent
  },
  {
      name: 'SimulationComponent',
      path: '/simulate',
      component: SimulationComponent
  },
  {
    name: 'DataComponent',
    path: '/data',
    component: DataComponent
  }
];

const router = new VueRouter({ mode: 'history', routes: routes });
new Vue(Vue.util.extend({ router }, App)).$mount('#app');
