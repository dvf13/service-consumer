package backend.services;

import backend.dtos.BusinessDto;
import backend.dtos.CustomerDto;
import backend.dtos.MessageDto;
import backend.exceptions.ServiceException;

public interface RegistrationService {

    MessageDto registerCustomer(CustomerDto customerDto) throws ServiceException;

    MessageDto registerBusiness(BusinessDto businessDto) throws ServiceException;
}
