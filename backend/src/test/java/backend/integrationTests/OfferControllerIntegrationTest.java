package backend.integrationTests;

import backend.dtos.BusinessDto;
import backend.dtos.CategoryDto;
import backend.dtos.OfferDto;
import backend.dtos.ResponseDto;
import backend.integrationTests.util.SSLUtil;
import backend.integrationTests.util.IntegTestsUtil;
import org.json.JSONObject;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedHashMap;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
@ActiveProfiles("test")
@Sql(executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD, scripts="/../resources/data.sql")
public class OfferControllerIntegrationTest {

    private final String AUTH_HEADER = "Authorization";
    private final String AUTH_PREFIX = "Bearer ";
    private String authToken = "";

    @Autowired
    private TestRestTemplate testRestTemplate;

    static {
        javax.net.ssl.HttpsURLConnection.setDefaultHostnameVerifier(
                (hostname, sslSession) -> hostname.equals("localhost"));
    }

    @Before
    public void turnOffSslChecking() throws Exception {
        SSLUtil.deactivateSslChecking();
    }

    @After
    public void turnOnSslChecking() throws Exception {
        SSLUtil.activateSslChecking();
    }


    @Test
    public void getAllOffers() throws Exception {
        assertTrue(IntegTestsUtil.login("user1", "12345", this.testRestTemplate));

        HttpHeaders authHeader = IntegTestsUtil.getAuthHeader();

        HttpEntity<OfferDto> entity = new HttpEntity<>(null, authHeader);
        ResponseEntity<ResponseDto> responseEntity = this.testRestTemplate.exchange("/api/offer/", HttpMethod.GET, entity, ResponseDto.class);
        ResponseDto responseDto = responseEntity.getBody();

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertFalse(responseDto.isError());

        ArrayList<JSONObject> result = IntegTestsUtil.convertArrayListOfLinkedHashMapsToArrayListOfJsonObjects((ArrayList<LinkedHashMap>) responseDto.getData());

        assertEquals(2, result.size());

        assertEquals(1, result.get(0).getInt("id"));
        assertEquals("Test offer", result.get(0).getString("title"));

        assertEquals(2, result.get(1).getInt("id"));
        assertEquals("Another test offer", result.get(1).getString("title"));
    }

    @Test
    public void getOfferById_loggedInUser() throws Exception{
        assertTrue(IntegTestsUtil.login("user1", "12345", this.testRestTemplate));

        HttpHeaders authHeader = IntegTestsUtil.getAuthHeader();

        HttpEntity<OfferDto> entity = new HttpEntity<>(null, authHeader);

        ResponseEntity<ResponseDto> responseEntity = this.testRestTemplate.exchange("/api/offer/1", HttpMethod.GET, entity, ResponseDto.class);
        ResponseDto responseDto = responseEntity.getBody();

        JSONObject receivedOffer = IntegTestsUtil.convertLinkedHashMapToJsonObject((LinkedHashMap) responseDto.getData());

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertFalse(responseDto.isError());
        assertEquals(1L, receivedOffer.getLong("id"));
        assertEquals("Test offer", receivedOffer.getString("title"));
        assertEquals("Test offer description", receivedOffer.getString("description"));
        assertEquals(500, receivedOffer.getInt("price"));

        assertTrue(IntegTestsUtil.logout(this.testRestTemplate));
    }

    @Test
    public void findOffersByBusinessIdOrderByValidUntil_loggedInUser() throws Exception{
        assertTrue(IntegTestsUtil.login("user1", "12345", this.testRestTemplate));

        HttpHeaders authHeader = IntegTestsUtil.getAuthHeader();

        HttpEntity<OfferDto> entity = new HttpEntity<>(null, authHeader);

        ResponseEntity<ResponseDto> responseEntity = this.testRestTemplate.exchange("/api/offer/business/2", HttpMethod.GET, entity, ResponseDto.class);
        ResponseDto responseDto = responseEntity.getBody();

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertFalse(responseDto.isError());

        ArrayList<JSONObject> result = IntegTestsUtil.convertArrayListOfLinkedHashMapsToArrayListOfJsonObjects((ArrayList<LinkedHashMap>) responseDto.getData());

        assertEquals(1, result.size());

        assertEquals(2, result.get(0).getInt("id"));
        assertEquals("Another test offer", result.get(0).getString("title"));

        assertTrue(IntegTestsUtil.logout(this.testRestTemplate));
    }

    @Test
    public void saveNewOfferAsAnonymUser_shouldGetRedirectTroughToNotLoggedIn() {
        OfferDto offerDtoToSave = this.createNewOfferDtoToSave();

        ResponseEntity<ResponseDto> responseEntity = this.testRestTemplate.postForEntity("/api/offer/", offerDtoToSave, ResponseDto.class);

        assertEquals(HttpStatus.FOUND, responseEntity.getStatusCode());
    }

    // Persistance error
    //@Test
    public void saveNewOfferAsLoggedInBusinessUser() throws Exception {
        assertTrue(IntegTestsUtil.login("business1", "12345", this.testRestTemplate));

        HttpHeaders authHeader = IntegTestsUtil.getAuthHeader();

        HttpEntity<BusinessControllerIntegrationTest> entity1 = new HttpEntity<>(null, authHeader);
        ResponseEntity<ResponseDto> responseEntity1 = this.testRestTemplate.exchange("/api/business/1", HttpMethod.GET, entity1, ResponseDto.class);
        ResponseDto responseDto1 = responseEntity1.getBody();
        JSONObject businessJson = IntegTestsUtil.convertLinkedHashMapToJsonObject((LinkedHashMap) responseDto1.getData());

        HttpEntity<CategoryControllerIntegrationTest> entity2 = new HttpEntity<>(null, authHeader);
        ResponseEntity<ResponseDto> responseEntity2 = this.testRestTemplate.exchange("/api/category/1", HttpMethod.GET, entity2, ResponseDto.class);
        ResponseDto responseDto2 = responseEntity2.getBody();
        JSONObject categoryJson = IntegTestsUtil.convertLinkedHashMapToJsonObject((LinkedHashMap) responseDto2.getData());

        HttpEntity<TimeSlotControllerIntegrationTests> entity4 = new HttpEntity<>(null, authHeader);
        ResponseEntity<ResponseDto> responseEntity4 = this.testRestTemplate.exchange("/api/timeslot/1", HttpMethod.GET, entity4, ResponseDto.class);
        ResponseDto responseDto4 = responseEntity4.getBody();
        ArrayList<JSONObject> timeSlots = IntegTestsUtil.convertArrayListOfLinkedHashMapsToArrayListOfJsonObjects((ArrayList<LinkedHashMap>) responseDto4.getData());

        OfferDto offerDto = new OfferDto();

        offerDto.setAmount(1);
        offerDto.setBusinessDto(IntegTestsUtil.convertJsonObjectToBusinessDto(businessJson));
        offerDto.setTitle("IntegTest");
        offerDto.setCreatedAt(Calendar.getInstance().getTime());
        offerDto.setCategoryDto(IntegTestsUtil.convertJsonObjectToCategoryDto(categoryJson));
        offerDto.setDescription("Offer from IntegTest");
        offerDto.setPictureUrl("http://url.comes.here");
        offerDto.setPrice(500);
        offerDto.setDiscount(20.0);
        offerDto.setValidUntil(Calendar.getInstance().getTime());
        offerDto.setLatitude(16.0);
        offerDto.setLongitude(18.0);
        offerDto.setTimeSlotDtoList(IntegTestsUtil.convertArrayListOfJsonObjectsToArrayListOfTimeSlotDto(timeSlots));

        HttpEntity<OfferDto> entity = new HttpEntity<>(offerDto, authHeader);
        ResponseEntity<ResponseDto> responseEntity = this.testRestTemplate.exchange("/api/offer/", HttpMethod.POST, entity, ResponseDto.class);
        ResponseDto responseDto = responseEntity.getBody();

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertFalse(responseDto.isError());

        JSONObject savedOffer = IntegTestsUtil.convertLinkedHashMapToJsonObject((LinkedHashMap) responseDto.getData());

        assertEquals(offerDto.getTitle(), savedOffer.getString("title"));

        HttpEntity entity3 = new HttpEntity(authHeader);

        responseEntity = this.testRestTemplate.exchange("/api/offer/", HttpMethod.GET, entity3, ResponseDto.class);
        responseDto = responseEntity.getBody();

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertFalse(responseDto.isError());

        ArrayList<JSONObject> result = IntegTestsUtil.convertArrayListOfLinkedHashMapsToArrayListOfJsonObjects((ArrayList<LinkedHashMap>) responseDto.getData());

        assertEquals(3, result.size());

        assertTrue(IntegTestsUtil.logout(this.testRestTemplate));
    }



    private OfferDto createNewOfferDtoToSave() {
        OfferDto offerDto = new OfferDto();

        BusinessDto businessDto = new BusinessDto();
        businessDto.setId(1L);

        CategoryDto categoryDto = new CategoryDto();
        categoryDto.setId(1L);

        offerDto.setAmount(1);
        offerDto.setBusinessDto(businessDto);
        offerDto.setTitle("IntegTest");
        offerDto.setCreatedAt(new Date());
        offerDto.setCategoryDto(categoryDto);
        offerDto.setDescription("Offer from IntegTest");
        offerDto.setPictureUrl("http://url.comes.here");
        offerDto.setPrice(500);
        offerDto.setDiscount(20.0);
        offerDto.setValidUntil(new Date());

        return offerDto;
    }
}







