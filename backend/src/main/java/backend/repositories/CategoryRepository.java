package backend.repositories;

import backend.models.Category;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Category repository for jpa
 */
@Repository
public interface CategoryRepository extends JpaRepository<Category, Long> {

}