import HttpService from './HttpService';
import config from '../config';
import UserService from './UserService';

export default {
  async getBookedReservations() {
    return HttpService.get(`${config.apiUrl}reservation/booked`)
      .then((response) => {
        if (!response.data.error) {
          return Promise.resolve(response.data.data);
        }
        // eslint-disable-next-line no-console
        console.error(`Response error message: ${response.data.data.message}`);
        return Promise.reject(response.data.data.message);
      }, error => Promise.reject(error.statusText))
      .catch(e => Promise.reject(e));
  },
  async getTotalReservations() {
    return HttpService.get(`${config.apiUrl}reservation/`)
      .then((response) => {
        if (!response.data.error) {
          return Promise.resolve(response.data.data);
        }
        // eslint-disable-next-line no-console
        console.error(`Response error message: ${response.data.data.message}`);
        return Promise.reject(response.data.data.message);
      }, error => Promise.reject(error.statusText))
      .catch(e => Promise.reject(e));
  },
  async getAllReservations(customerId) {
    return HttpService.get(`${config.apiUrl}reservation/customer/${customerId}/reserved`)
      .then((response) => {
        const responseDto = response.data;

        if (!responseDto.error) {
          return Promise.resolve(responseDto.data);
        }
        // eslint-disable-next-line no-console
        console.error(`Response error message: ${responseDto.data.message}`);
        return Promise.reject(responseDto.data.message);
      })
      .catch(e => Promise.reject(e));
  },
  async getAllBookedReservations(customerId) {
    return HttpService.get(`${config.apiUrl}reservation/customer/${customerId}/booked`)
      .then((response) => {
        if (!response.data.error) {
          return Promise.resolve(response.data.data);
        }
        // eslint-disable-next-line no-console
        console.error(`Response error message: ${response.data.data.message}`);
        return Promise.reject(response.data.data.message);
      })
      .catch(e => Promise.reject(e));
  },
  async getAllCancelledReservations(customerId) {
    return HttpService.get(`${config.apiUrl}reservation/customer/${customerId}/cancelled`)
      .then((response) => {
        if (!response.data.error) {
          return Promise.resolve(response.data.data);
        }
        // eslint-disable-next-line no-console
        console.error(`Response error message: ${response.data.data.message}`);
        return Promise.reject(response.data.data.message);
      })
      .catch(e => Promise.reject(e));
  },
  async getReservation(id) {
    return HttpService.get(`${config.apiUrl}reservation/${id}`)
      .then((response) => {
        if (!response.data.error) {
          return Promise.resolve(response.data.data);
        }
        // eslint-disable-next-line no-console
        console.error(`Response error message: ${response.data.message}`);
        return Promise.reject(response.data.message);
      })
      .catch(e => Promise.reject(e));
  },
  async getReservationsFromTimeSlotId(id) {
    return HttpService.get(`${config.apiUrl}reservation/timeslot/${id}`)
      .then((response) => {
        const responseDto = response.data;

        if (responseDto.error) {
          return Promise.reject(responseDto.data.message);
        }
        return Promise.resolve(responseDto.data);
      }, error => Promise.reject(error.statusText))
      .catch(e => Promise.reject(e));
  },
  async deleteFromWishlist(customerId, offerId) {
    if (!UserService.isAuthenticated()) {
      return Promise.reject(new Error('Not logged in!'));
    }

    // eslint-disable-next-line no-console
    console.log('deleteFromWishlist');
    return HttpService.delete(`${config.apiUrl}reservation/wishlist/${customerId}/${offerId}`)
      .then((response) => {
        if (!response.data.error) {
          // eslint-disable-next-line no-console
          console.log('Wishlist entry deleted successfully!');
          return Promise.resolve();
        }
        // eslint-disable-next-line no-console
        console.error(`Response error message: ${response.data.data.message}`);
        return Promise.reject(response.data.data.message);
      }, error => Promise.reject(error.statusText))
      .catch(e => Promise.reject(e));
  },
};
