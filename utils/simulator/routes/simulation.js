'use strict';

let _ = require('lodash'),
    striptags = require('striptags'),
    sanitizer = require('sanitizer'),
    fs = require("fs"),
    NodeRequestParser = require('node-request-parser'),
    UserService = require('../services/UserService'),
    SimulationService = require('../services/SimulationService');

const express = require('express'),
      router = express.Router();

//initialize request parser
let requestParserOptions = {disableRegex: true, sanitizeFunction: (property) => sanitizer.sanitize(striptags(property))};
let requestParser = new NodeRequestParser(requestParserOptions);
let data;

router.post('/login/', (req, res, next) => {
  let data = requestParser.parseSync(req, ['B*loginEndpoint', 'B*requestProcessing']);
  if(!data || data.error){
    console.error(data.errors);
    return res.json({success: false, message: 'Wrong data passed.'});
  }
  UserService.loginUsers(data.body.loginEndpoint, data.body.requestProcessing, (err) => {
    if(err) return res.json({success: false, message: 'Could not login users.'});
    res.json({success: true, message: 'Users were logged in successfully.'});
  });
});

router.post('/getOffers/', (req, res, next) => {
  let data = requestParser.parseSync(req, ['B*getOfferEndpoint', 'B*requestProcessing', 'BuserCount']);
  if(!data || data.error){
    console.error(data.errors);
    return res.json({success: false, message: 'Wrong data passed.'});
  }
  SimulationService.simulateGetOffer(data.body.getOfferEndpoint, data.body.userCount, data.body.requestProcessing, (err) => {
    if(err) return res.json({success: false, message: 'Could not simulate getting offers.'});
    res.json({success: true, message: 'Getting offers was successfully simulated.'});
  });
});

router.post('/createOffers/', (req, res, next) => {
  let data = requestParser.parseSync(req, ['B*createOfferEndpoint', 'B*requestProcessing', 'BbusinessCount']);
  if(!data || data.error){
    console.error(data.errors);
    return res.json({success: false, message: 'Wrong data passed.'});
  }
  SimulationService.simulateCreateOffer(data.body.createOfferEndpoint, data.body.businessCount, data.body.requestProcessing, (err) => {
    if(err) return res.json({success: false, message: 'Could not simulate getting offers.'});
    res.json({success: true, message: 'Creating offers was successfully simulated.'});
  });
});

module.exports = router;
