package backend.integrationTests;

import backend.dtos.*;
import backend.integrationTests.util.SSLUtil;
import backend.integrationTests.util.IntegTestsUtil;
import org.json.JSONObject;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
@ActiveProfiles("test")
@Sql(executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD, scripts="/../resources/data.sql")
public class CustomerControllerIntegrationTest {

    private static final String ADMIN_NAME = "user3";
    private static final String ADMIN_PASS = "123";
    private static final String NON_ADMIN_NAME = "user1";
    private static final String NON_ADMIN_PASS = "12345";
    private static final String UNAUTHORIZED_USER_NAME = "business1";
    private static final String UNAUTHORIZED_USER_PASS = "12345";

    @Autowired
    private TestRestTemplate testRestTemplate;

    static {
        javax.net.ssl.HttpsURLConnection.setDefaultHostnameVerifier(
                (hostname, sslSession) -> hostname.equals("localhost"));
    }

    @Before
    public void turnOffSslChecking() throws Exception {
        SSLUtil.deactivateSslChecking();
    }

    @After
    public void turnOnSslChecking() throws Exception {
        SSLUtil.activateSslChecking();
    }

    @Test
    public void getAllCustomers_AdminLogin() throws Exception{
        assertTrue(IntegTestsUtil.login(ADMIN_NAME, ADMIN_PASS, this.testRestTemplate));
        HttpHeaders authHeader = IntegTestsUtil.getAuthHeader();
        HttpEntity<CustomerControllerIntegrationTest> entity = new HttpEntity<>(null, authHeader);

        ResponseEntity<ResponseDto> responseEntity = this.testRestTemplate.exchange("/api/customer/", HttpMethod.GET, entity, ResponseDto.class);
        ResponseDto responseDto = responseEntity.getBody();

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertFalse(responseDto.isError());

        ArrayList<JSONObject> result = IntegTestsUtil.convertArrayListOfLinkedHashMapsToArrayListOfJsonObjects((ArrayList<LinkedHashMap>) responseDto.getData());
        assertEquals(3, result.size());

        // ids start from 1
        for (int i = 0; i < result.size(); i++) {
            assertEquals(i + 1, result.get(i).getInt("id"));
            assertEquals("user" + (i + 1), result.get(i).getString("username"));
        }

        assertTrue(IntegTestsUtil.logout(this.testRestTemplate));
    }

    @Test
    public void getAllCustomers_NonAdminLogin() {
        assertTrue(IntegTestsUtil.login(NON_ADMIN_NAME, NON_ADMIN_PASS, this.testRestTemplate));

        HttpHeaders authHeader = IntegTestsUtil.getAuthHeader();
        HttpEntity<CustomerControllerIntegrationTest> entity = new HttpEntity<>(null, authHeader);

        ResponseEntity<String> responseEntity = this.testRestTemplate.exchange("/api/customer/", HttpMethod.GET, entity, String.class);

        assertEquals(HttpStatus.FORBIDDEN, responseEntity.getStatusCode());
        assertTrue(IntegTestsUtil.logout(this.testRestTemplate));
    }

    @Test
    public void getCustomerById_AuthorizedNonAdminLogin() throws Exception{
        assertTrue(IntegTestsUtil.login(NON_ADMIN_NAME, NON_ADMIN_PASS, this.testRestTemplate));

        HttpHeaders authHeader = IntegTestsUtil.getAuthHeader();
        HttpEntity<CustomerControllerIntegrationTest> entity = new HttpEntity<>(null, authHeader);

        ResponseEntity<ResponseDto> responseEntity = this.testRestTemplate.exchange("/api/customer/1", HttpMethod.GET, entity, ResponseDto.class);
        ResponseDto responseDto = responseEntity.getBody();

        JSONObject result = IntegTestsUtil.convertLinkedHashMapToJsonObject((LinkedHashMap) responseDto.getData());

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertFalse(responseDto.isError());
        assertEquals("1", String.valueOf(result.getLong("id")));
        assertEquals("user1", result.getString("username"));
        assertEquals("user1@email.com", result.getString("email"));
        assertEquals("Max", result.getString("firstName"));
        assertEquals("Mustermann", result.getString("lastName"));
        assertEquals("m", result.getString("gender"));

        assertTrue(IntegTestsUtil.logout(this.testRestTemplate));
    }

    @Test
    public void getCustomerById_UnauthorizedLogin() {
        assertTrue(IntegTestsUtil.login(UNAUTHORIZED_USER_NAME, UNAUTHORIZED_USER_PASS, this.testRestTemplate));

        HttpHeaders authHeader = IntegTestsUtil.getAuthHeader();
        HttpEntity<CustomerControllerIntegrationTest> entity = new HttpEntity<>(null, authHeader);

        ResponseEntity<String> responseEntity = this.testRestTemplate.exchange("/api/customer/1", HttpMethod.GET, entity, String.class);

        assertEquals(HttpStatus.FORBIDDEN, responseEntity.getStatusCode());

        assertTrue(IntegTestsUtil.logout(this.testRestTemplate));
    }

    @Test
    public void saveCustomer() throws Exception{
        assertTrue(IntegTestsUtil.login(NON_ADMIN_NAME, NON_ADMIN_PASS, this.testRestTemplate));

        HttpHeaders authHeader = IntegTestsUtil.getAuthHeader();

        CustomerDto customerDto = new CustomerDto();
        customerDto.setUsername("Test");
        customerDto.setFirstName("FirstName");
        customerDto.setLastName("LastName");
        customerDto.setPassword("12345");
        customerDto.setBirthdate(new Date(1993,1,1));
        customerDto.setEmail("test_mail1@email.com");
        customerDto.setGender('m');

        HttpEntity<CustomerDto> entity = new HttpEntity<>(customerDto, authHeader);

        ResponseEntity<ResponseDto> responseEntity = this.testRestTemplate.exchange("/api/customer/", HttpMethod.POST, entity, ResponseDto.class);
        ResponseDto responseDto = responseEntity.getBody();
        String id = responseDto.getData().toString().split(",")[0].split("=")[1];

        HttpEntity<CustomerControllerIntegrationTest> entity1 = new HttpEntity<>(null, authHeader);

        ResponseEntity<ResponseDto> responseEntity1 = this.testRestTemplate.exchange("/api/customer/" + id, HttpMethod.GET, entity1, ResponseDto.class);
        ResponseDto responseDto1 = responseEntity1.getBody();

        JSONObject customer = IntegTestsUtil.convertLinkedHashMapToJsonObject((LinkedHashMap) responseDto1.getData());

        // checking status codes
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertFalse(responseDto.isError());

        assertEquals(HttpStatus.OK, responseEntity1.getStatusCode());
        assertFalse(responseDto1.isError());

        // verification
        assertEquals(customerDto.getUsername(), customer.getString("username"));
        assertEquals(customerDto.getFirstName(), customer.getString("firstName"));
        assertEquals(customerDto.getLastName(), customer.getString("lastName"));
        assertEquals(customerDto.getEmail(), customer.getString("email"));
        assertEquals(String.valueOf(customerDto.getGender()), customer.getString("gender"));

        assertTrue(IntegTestsUtil.logout(this.testRestTemplate));
    }

    @Test
    public void updateCustomer() throws Exception{
        assertTrue(IntegTestsUtil.login(NON_ADMIN_NAME, NON_ADMIN_PASS, this.testRestTemplate));

        HttpHeaders authHeader = IntegTestsUtil.getAuthHeader();
        HttpEntity<CustomerControllerIntegrationTest> entity = new HttpEntity<>(null, authHeader);

        ResponseEntity<ResponseDto> responseEntity = this.testRestTemplate.exchange("/api/customer/1", HttpMethod.GET, entity, ResponseDto.class);
        ResponseDto responseDto = responseEntity.getBody();

        JSONObject customer = IntegTestsUtil.convertLinkedHashMapToJsonObject((LinkedHashMap) responseDto.getData());
        CustomerDto customerDto = IntegTestsUtil.convertJsonObjectToCustomerDto(customer);
        customerDto.setFirstName("Maxim");

        HttpEntity<CustomerDto> entity1 = new HttpEntity<>(customerDto, authHeader);

        ResponseEntity<ResponseDto> responseEntity1 = this.testRestTemplate.exchange("/api/customer/1", HttpMethod.PUT, entity1, ResponseDto.class);
        ResponseDto responseDto1 = responseEntity1.getBody();

        HttpEntity<CustomerControllerIntegrationTest> entity2 = new HttpEntity<>(null, authHeader);

        ResponseEntity<ResponseDto> responseEntity2 = this.testRestTemplate.exchange("/api/customer/1", HttpMethod.GET, entity2, ResponseDto.class);
        ResponseDto responseDto2 = responseEntity2.getBody();

        JSONObject customer1 = IntegTestsUtil.convertLinkedHashMapToJsonObject((LinkedHashMap) responseDto2.getData());

        // checking status codes
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertFalse(responseDto.isError());

        assertEquals(HttpStatus.OK, responseEntity1.getStatusCode());
        assertFalse(responseDto1.isError());

        // verification
        assertEquals(customerDto.getUsername(), customer1.getString("username"));
        assertEquals("Maxim", customer1.getString("firstName"));
        assertEquals(customerDto.getLastName(), customer1.getString("lastName"));
        assertEquals(customerDto.getEmail(), customer1.getString("email"));
        assertEquals(String.valueOf(customerDto.getGender()), customer1.getString("gender"));

        assertTrue(IntegTestsUtil.logout(this.testRestTemplate));
    }

    // TODO: Bug #28743 - error code 500 when trying to delete customer
    //@Test
    public void deleteCustomer() throws Exception{
        assertTrue(IntegTestsUtil.login(ADMIN_NAME, ADMIN_PASS, this.testRestTemplate));

        HttpHeaders authHeader = IntegTestsUtil.getAuthHeader();
        HttpEntity<CustomerControllerIntegrationTest> entity = new HttpEntity<>(null, authHeader);

        ResponseEntity<String> responseEntity = this.testRestTemplate.exchange("/api/customer/2", HttpMethod.DELETE, entity, String.class);

        HttpEntity<CustomerControllerIntegrationTest> entity2 = new HttpEntity<>(null, authHeader);

        ResponseEntity<String> responseEntity2 = this.testRestTemplate.exchange("/api/customer/2", HttpMethod.GET, entity2, String.class);

        // checking status codes
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertEquals(HttpStatus.NOT_FOUND, responseEntity2.getStatusCode());


        assertTrue(IntegTestsUtil.logout(this.testRestTemplate));
    }

    /**
     * Converts milliseconds in Date object
     *
     * @param dateString - date in milliseconds
     * @return
     */
    private Date parseDate(String dateString) throws ParseException {
        Date date = new Date(Long.parseLong(dateString));
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.parse(sdf.format(date));
    }

}
